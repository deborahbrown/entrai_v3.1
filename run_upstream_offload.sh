#!/bin/sh
#
cd /data/speechsoft/apps/entrAI
if [ -e ./Applications/Logs/upstreamdebug.log ]
then
  mv ./Applications/Logs/upstreamdebug.log ./Applications/Logs/upstreamdebug.log-old
fi
/usr/local/perlbrew/perls/perl-5.28.0/bin/perl upstreaminterface.pl ./entrAI.config 2>./Applications/Logs/upstreamdebug.log
#

