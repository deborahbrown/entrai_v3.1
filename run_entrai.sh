#!/bin/sh
#
cd /data/speechsoft/apps/entrAI
if [ -e ./Applications/Logs/entrAIdebug.log ]
then
  mv ./Applications/Logs/entrAIdebug.log ./Applications/Logs/entrAIdebug.log-old
fi
/usr/local/perlbrew/perls/perl-5.28.0/bin/perl ./entrAI.pl ./entrAI.config 2>./Applications/Logs/entrAIdebug.log
#
