#!C:/Strawberry/perl/bin/perl
use lib "C:/Users/7DQVN32/Documents/Projects/entrAIProject/InProgress/entrAI/lib";
use XML::Parser;
use Data::Dumper;

my @RULES;
my @VARIABLES;

my $oldfh=select(STDERR);
$|=1;
select($oldfh);

$inputDir = shift @ARGV;
$outputDir = shift @ARGV;
opendir(INPUTDIR, "$inputDir") || die "Can't open directory $inputDir";

while ($grammarname = readdir(INPUTDIR)) {
  next if $grammarname !~ /\.grxml$/ and $grammarname !~ /\.gram$/ ;
  printf STDERR "Begin processing $grammarname\n"; 
  my $fh;
  my $grammar = do {
   local $/ = undef;
   open $fh, "<", "$grammarname" or die "Can't open $grammarname"; 
   <$fh>;
  };
  close $fh;
 
  my ($rootrule, $tag_format) = getRoot("$grammarname");

  $grammar =~ s#<grammar[^>]*>#<grammar version="1.0" xml:lang="en-us" xmlns="http://www.w3.org/2001/06/grammar" tag-format="ss-entrai/1.0" root="$rootrule">\n#;
  #$grammar = insert_external_grammars($grammar);

  if ($tag_format =~ /swi-semantics/) {
   $grammar = process_swi($grammar, "$grammarname", 1);
  }
  $grammar =~ s#([^\;\s])\s*</tag>#$1 \; </tag>#gm;

  open(OUTPUTFILE, ">$outputDir/$grammarname") || die "Can't open $grammarname";
  print OUTPUTFILE $grammar;
  close(OUTPUTFILE);
  printf STDERR "End processing $grammarname\n";
}
closedir(INPUTDIR);
unlink "foo$$.out";
exit();


sub getRoot {
  my ($grammarname) = @_;

  open(SAVEOUT, ">&STDOUT");
  open(SAVEERR, ">&STDERR");
  open(STDOUT, ">foo$$.out");
  open(STDERR, ">&STDOUT");
  select(STDERR); $|=1;
  select(STDOUT); $|=1;
  my $xmlDoc = new XML::Parser(Style => 'Debug'); 
  my $xmlHash = $xmlDoc->parsefile($grammarname);
  close(STDOUT);
  close(STDERR);
  open(STDOUT, ">&SAVEOUT");
  open(STDERR, ">&SAVEERR");

  open(DUMPED, "foo$$.out");
  my $firstline = <DUMPED>;
  my ($root) = ($firstline =~ / root\s+(\w+)/ );
  my ($tag_format) = ($firstline =~ /tag-format\s+([^\s\)]+)/);
  if (!defined $tag_format || $tag_format eq '') {
      $tag_format = 'semantics/1.0';
  }
  
  if ($root ne '') {
     close DUMPED;
     return ($root, $tag_format);
  }
  while (<DUMPED>) {
     next if $_ !~ /\(id\s+/;
     ($root) = ($_ =~/\(id\s+(\w+)/);
     close DUMPED;
     return ($root, $tag_format);
  }
  die "Couldn't find root rule in $grammarname";


}

sub getVariables {
  my ($grammarname) = @_;


  open(SAVEOUT, ">&STDOUT");
  open(SAVEERR, ">&STDERR");

  open(STDOUT, ">foo$$.out") || die "couldn't open foo$$.out";
  open(STDERR, ">&STDOUT");
  select(STDERR); $|=1;
  select(STDOUT); $|=1;

  my $xmlDoc = new XML::Parser(Style => 'Debug'); 
  my $xmlHash = $xmlDoc->parsefile($grammarname);
  close(STDOUT);
  close(STDERR);
  open(STDOUT, ">&SAVEOUT");
  open(STDERR, ">&SAVEERR");


  open(DUMPED, "foo$$.out");
  my @assignments;
  while (<DUMPED>) {
     next if ! /tag \|\|/;
     push(@assignments, /(\b[A-Za-z_]\w*)\s*=\s*['"]?\w*['"]?/g );
  }
  close(DUMPED);
  return @assignments;
}

sub getRuleNames {
  my ($grammarname) = @_;

  open(SAVEOUT, ">&STDOUT");
  open(SAVEERR, ">&STDERR");
  open(STDOUT, ">foo$$.out");
  open(STDERR, ">&STDOUT");
  select(STDERR); $|=1;
  select(STDOUT); $|=1;
  my $xmlDoc = new XML::Parser(Style => 'Debug'); 
  my $xmlHash = $xmlDoc->parsefile($grammarname);
  close(STDOUT);
  close(STDERR);
  open(STDOUT, ">&SAVEOUT");
  open(STDERR, ">&SAVEERR");

  open(DUMPED, "foo$$.out");

  while (<DUMPED>) {
     next if ! /\\\\\s*\(id\s/;
     push(@RULES, /\(id (\w+)/g );
  }
  close(DUMPED);
  return @RULES;
}

###########################

sub insert_external_grammars {
  my ($grammar) = @_;

  $grammar =~ s%</grammar>%%;

  my @externalList = ($grammar =~ /<ruleref\s[\w\s='"]*uri\s*=\s*['"]([\w\.\/\\]+)['"]/g);
  foreach my $e (@externalList) {
     my ($substituterule, $subroot) = insert1_external_grammar($e);
     $e =~ s/\\/\\\\/g;
     $grammar =~ s%$%\n$substituterule\n%; 
     $grammar =~ s%(<ruleref\s[\w\s='"]*uri\s*=\s*['"])$e(['"])%$1#$subroot$2%g; 
  }
  
  $grammar =~ s%$%\n</grammar>\n%;
  return $grammar;
}


sub insert1_external_grammar {
   my ( $externalfile, $tagformat) = @_;
 print "begin insert external grammar $externalfile\n";
   my $fh;
   my $newrules = do {
    local $/ = undef;
    open $fh, "<", "$externalfile" or die "Can't open $externalfile"; 
    <$fh>;
  };
  close $fh;


  #Get rid of initial <?xml... line

  #$newrules = process_swi($newrules, $externalfile, 0 );


  my ($rootrule) = ($newrules =~ /<grammar\s+\broot=['"]([^'"]+)['"]/);
  if ($rootrule eq '' ) {
     ($rootrule) = ($newrules =~ /<rule\s+\bid\s*=\s*['"](\w+)['"]/);
  }
 

  $externalgramid++;
  $rootrule = sprintf "__ext%d%s__", $externalgramid, $rootrule;
  push(@RULES, $rootrule);

  push(@VARIABLES, getVariables($externalfile));
 

  $newrules =~ s#<grammar\s[^>]+>##;
  $newrules =~ s#</grammar>##;
  $newrules =~ s#(<rule\s+[^>]*\bid\s*=\s*['"])(\w+)(['"])#$1__ext$externalgramid$2__$3#g;

  $newrules =~ s%(<ruleref\s+[^>]*\buri\s*=\s*['"]#)(\w+)(['"])%$1__ext$externalgramid$2__$3%g;
  my @parts = split(/>\s+/, $newrules, 2);
  $newrules = $parts[1];
 print "finished insert external grammar $externalfile\n";
  return ($newrules, $rootrule);


}



##########################

sub process_swi {
  my($grammar,  $gramfile, $toplevel) = @_;

 my ($rootrule, $tag_format) = getRoot($gramfile);

  #Get list of assignments for variable names
  push( @VARIABLES, getVariables($gramfile));

  my %Variables;
  foreach my $variable (@VARIABLES) {
     $Variables{$variable}++;
  }

  my $initial_declaration = "  <item>\n    <tag>";
  my $endruleassign = "    <tag>\n    ";
  
  my $initial_declaration = sprintf " <item>\n    <tag>var %s;\n      ", join(',', keys(%Variables));
  foreach $variable (keys %Variables) {
     next if $variable eq "SWI_disallow"; #will declare these in grammar interpretation routine
     next if $variable eq "SWI_decoy";
      next if $variable eq "SWI_scoreDelta";
     $endruleassign = sprintf "%s out.%s=%s;", $endruleassign, $variable, $variable;
  }

  #Get list of rules
  my @Rulenames = getRuleNames($gramfile);

  foreach $rulename (@Rulenames) {
     $initial_declaration = sprintf "%s var %s = {};", $initial_declaration, $rulename;

     if ($rulename ne $rootrule) {
        $grammar =~ s#(<rule\s[^>]*\bid\s*=\s*['"]$rulename['"][^>]*>)#$1\n  <item>\n#;

     }
     elsif (! $toplevel) {

        $grammar =~ s#(<rule\s[^>]*\bid\s*=\s*['"]$rulename['"][^>]*>)#$1\n  <item>\n#;
     }
  }


  $initial_declaration = sprintf "%s</tag>\n", $initial_declaration;
  $endruleassign = sprintf "%s\n    </tag>\n</item>\n</rule>\n", $endruleassign;

  $grammar =~ s#</rule>#$endruleassign#g;
  if ($toplevel) {
   $grammar =~ s#<rule\s[^>]*id\s*=\s*['"]$rootrule['"][^>]*>#<rule id='$rootrule' scope='public'>\n$initial_declaration\n#;
  }
  
  return $grammar;
}




