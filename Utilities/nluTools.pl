use lib 'C:/Users/7DQVN32/Documents/Projects/entraiproject/production/entrai/lib';
use NLU::NLU;
use NLU::wordstemmer;
use Data::Dumper;

if (@ARGV < 1) {
die <<ENDUSAGE;
Usage: perl nluTools.pl <command> [args]
     commands: train|test|stems|confusions
ENDUSAGE
}



my $command = shift @ARGV;
if ($command eq 'train') {
   doTrain(@ARGV);
}
elsif ($command eq 'test') {
   doTest(@ARGV);
}
elsif ($command eq 'stems') {
   doStems(@ARGV);
}
elsif ($command eq 'confusions') {
   doConfusions(@ARGV);
}
else {
  die "Unknown option: $command";
}


sub doTrain {
  if (@_ < 2) {
    die <<ENDUSAGE;
Usage: perl nluTools.pl train <nlu|gram|list> <package> [model]
ENDUSAGE
  }

  my ($modeltype, $package, $model) = @_;
  if (!defined $model) {
    $model = 'model.out';
  }
  if ($modeltype eq 'nlu') {
     NLU::NLU::trainModel("./Packages/$package", "./$model","nlu");
  }
  else {
     die "Only nlu supported so far";
  }
}

sub doTest {
  if (@_ < 2) {
    die <<ENDUSAGE;
Usage: perl nluTools.pl test [model] [testset]
ENDUSAGE
  }

  my ($modelfile, $testset) = @ARGV;
  if (! defined $modelfile or ! defined $testset) {
     die "Usage: perl testNLU.pl  <model file> <testset>";
  }
  my $model= NLU::NLU::readModel("./$modelfile");

  open(TESTSET, $testset)  || die "Can't open $testset";
  <TESTSET>;
  $|=1;

  my $nsentences=0;
  my $ncorrect=0;
  print "Phrase,Real Tag,Real Tag Score, Guessed Tag,Raw Score,2nd Best,Raw Score, Correct\n";
  while (<TESTSET>) {
   s/\s+$//;
   my ($sentence, $realtag) = split(/,/);
   next if !defined $sentence;
   next if $sentence =~ /^\s*$/;

   my $best = NLU::NLU::getBest($model,$sentence);
   my $correctflag=0;
   $nsentences++;
   if ($realtag eq $best->{tag}) {
    $correctflag=1;
    $ncorrect++;
   } 

  printf "%s,%s,%s,%s,%s,%d\n", $sentence, $realtag, $best->{'tag'},  $best->{score}, $best->{'nbest'}->[1], $correctflag;
  }
  close TESTSET;
  print "\n\n\n";
  print Dumper $Params;
  printf  "Total correct: %d, %6.2f pct of %d sentences\n\n", $ncorrect, 100 * $ncorrect/$nsentences, $nsentences;
  printf STDERR "Total correct: %d, %6.2f pct of %d sentences\n\n", $ncorrect, 100 * $ncorrect/$nsentences, $nsentences;

}

my $COUNT;

sub doConfusions {
  if (@_ <1 ) {
    die <<ENDUSAGE;
Usage: perl nluTools.pl confusions <test results file>
ENDUSAGE
  }
  my $results = shift @ARGV;
  open(RESULTS, $results) || die "Can't open $results";
  my %TagConfusions = ();
  my %WordConfusions = ();
  while (<RESULTS>) {
    s/\s+$//;
    my($sentence, $realtag, $recogtag, $t1, $t2,  $correctflag) = split(/,/);
    next if $correctflag == 1;
    $TagConfusions{$realtag . ',' .$recogtag}++;
    foreach my $word (split(/\s+/, $sentence)) {
      $WordConfusions{$word}++;
    }
  }
  close RESULTS;

     $COUNT=\%TagConfusions;
  my  @tagConfusions = sort bycount (keys %TagConfusions);
  $COUNT=\%WordConfusions;
  my  @wordConfusions = sort bycount (keys %WordConfusions);
  print "Top Tag Confusion\n\n\n";
  for (my $i=0; $i<10; $i++) {
    printf "%s,%d\n", $tagConfusions[$i], $TagConfusions{$tagConfusions[$i]};
  }
  print "\n\n\nTop Word Confusions";
  for (my $i=0; $i<10; $i++) {
    printf "%s,%d\n", $wordConfusions[$i], $WordConfusions{$wordConfusions[$i]};
  }
}


sub doStems {
  if (@_ <1 ) {
    die <<ENDUSAGE;
Usage: perl nluTools.pl stems <model>
ENDUSAGE
  }

  my ($modelfile) = @_;
  my $model= NLU::NLU::readModel("./$modelfile");
  $|=1;
  printf "Enter sentence:\n";
  while (<STDIN>) {
    my $sentence = $_;
    foreach my $word (split(/\s+/, $sentence) ){
      my $stems = NLU::wordstemmer::getStems($word, $model);
      my @stems = %$stems;
      printf "%s,%s\n", $word, ($stems[0] or '');
    }
  }
}
  
sub bycount {
    return $COUNT->{$b} <=> $COUNT->{$a};
}