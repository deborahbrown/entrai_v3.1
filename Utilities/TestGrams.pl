
use lib '/data/speechsoft/apps/entrAI/lib';
use Grammar::GrammarInterpreter;
use Misc::Utilities;
use Data::Dumper;
use strict;

our $sessionid='randomsessionid';
my $connectstr='DBI:MariaDB:database=entrAIDB;host=127.0.0.1';
my $DBconnectinfo = {
connstr => $connectstr,
user => 'entrai',
pwd => 'YES'
} ;
my $dbh = Misc::Utilities::ConnectToDB($DBconnectinfo);
my $testfile = shift;

print "Grammar,Input,RecoStatus,Interpretation\n";
open(TESTFILE, $testfile) or "Can't open $testfile";
while (<TESTFILE>) {
   s/\s+$//;
   next if /^\s*$/;
   my($grammar, $userinput) = split(/,/);

   my $recoObj = Grammar::GrammarInterpreter->GrammarFileInterpreter($grammar,$userinput);
   my $interpretation = '';
   my $status = $recoObj->{'status'};
   if ($status eq 'MATCH') {
     my %slotpairs = %{$recoObj->{'interpretation'}};
     foreach my $slot (sort keys %slotpairs) {
       $interpretation = sprintf "%s+%s=%s", $interpretation, $slot, $slotpairs{$slot};
     }
   }
   printf "%s,%s,%s,%s\n", $grammar, $userinput, $status, $interpretation;
}
