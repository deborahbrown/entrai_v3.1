use lib 'C:/Users/7DQVN32/Documents/Projects/entraiproject/production/entrai/lib';
use NLU::NLU;
use Data::Dumper;

  my ($modelfile, $testset) = @ARGV;
  if (! defined $modelfile or ! defined $testset) {
     die "Usage: perl testNLU.pl  <model file> <testset>";
  }
  my $model= NLU::NLU::readModel($modelfile);

  open(TESTSET, $testset)  || die "Can't open $testset";
  <TESTSET>;
  $|=1;

  my $nsentences=0;
  my $ncorrect=0;
  print "Phrase,Real Tag,Real Tag Score, Guessed Tag,Raw Score,2nd Best,Raw Score, Correct\n";
  while (<TESTSET>) {
   s/\s+$//;
   my ($sentence, $realtag) = split(/,/);
   next if !defined $sentence;
   next if $sentence =~ /^\s*$/;

   my $best = NLU::NLU::getBest($model,$sentence);
   my $correctflag=0;
   $nsentences++;
   if ($realtag eq $best->{tag}) {
    $correctflag=1;
    $ncorrect++;
   } 

  printf "%s,%s,%s,%s,%s,%s,%s,%d\n", $sentence, $realtag, $Score{$realtag},  $best->{'tag'},  $best->{score}, $best->{'nbest'}->[1], 
           $Score{$best->{'nbest'}->[1]}, $correctflag;
  }
  close TESTSET;
  print "\n\n\n";
  print Dumper $Params;
  printf  "Total correct: %d, %6.2f pct of %d sentences\n\n", $ncorrect, 100 * $ncorrect/$nsentences, $nsentences;
  printf STDERR "Total correct: %d, %6.2f pct of %d sentences\n\n", $ncorrect, 100 * $ncorrect/$nsentences, $nsentences;


