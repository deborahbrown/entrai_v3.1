use lib 'C:/Users/7DQVN32/Documents/Projects/entraiproject/production/entrai/lib';
use NLU::NLU;

if (@ARGV < 1) {
die 'Usage: perl trainModel.pl <package> [model]';
}

my ($package, $model) = @ARGV;
if (!defined $model) {
  $model = 'model.out';
}
NLU::NLU::trainModel($package, $model);
