use lib "/home/administrator/entrAI/lib";
use lib "/data/speechsoft/apps/entrAI/lib";
use DBI;
use Misc::Utilities;


if (@ARGV < 1) {
   die <<STR
Usage: perl $0 <dbname> -create 
       perl $0 <dbname> -load [prompts|grammars] <input>
       perl $0 <dbname> -update [prompts|grammars] <input>
       perl $0 <dbname> -dump prompts
STR
}

my $dbname = shift @ARGV;

my $function=shift @ARGV;
my $ListOfTags;

if ($function ne '-update' && $function ne '-create' && $function ne '-load' && $function ne '-dump') {
   die <<STR
Usage: perl $0 <database name>  -create 
       perl $0 <database name> -load [prompts|grammars] <input>
       perl $0 <database name> -update [prompts|grammars] <input>
       perl $0 <database name> -dump prompts
STR
}

&main($function );

sub main {

  if ($function eq '-create') { 
       &createDatabase($dbname) ; 
  }
  elsif ($function eq '-load') {

      my $table = shift @ARGV;
      if ($table eq 'grammars' ) {
          my $inputfile = shift @ARGV;
          &loadGrammars( $dbname, $inputfile,1);
      }
      elsif ($table eq 'prompts') {
          my $inputfile = shift @ARGV;
          &loadPrompts( $dbname, $inputfile);
      }
  }
  elsif ($function eq '-update') { 
      my $table = shift @ARGV;
      if ($table eq 'grammars') {
          my $inputfile = shift @ARGV;
          &loadGrammars( $dbname, $inputfile,0);
      }
      elsif ($table eq 'prompts') {
          my $inputfile = shift @ARGV;
          &updatePrompts( $dbname, $inputfile);
      }
      else {
        die <<STR

Unknown argument: $table

Usage: perl $0 <database name> -create 
       perl $0 <database name> -load [prompts|grammars] <input>
       perl $0 <database name> -update [prompts|grammars] <input>
       perl $0 <database name> -dump prompts
STR
     }
   }
   elsif ($function eq '-dump') {
       my $table = shift @ARGV;
       if ($table ne 'prompts') {
          badargs();
       }
       else {
          &dumpPrompts($dbname);
       }
    }        
}

sub badargs {
        die <<STR

Unknown argument: $table

Usage: perl $0 <database name> -create 
       perl $0 <database name> -load [prompts|grammars] <input>
       perl $0 <database name> -update [prompts|grammars] <input>
       perl $0 <database name> -dump prompts
STR
}

sub dumpPrompts {
  my ($dbname)= @_;
  my $dbconnstr = sprintf "dbi:SQLite:dbname=%s", $dbname;

   my $dbh = DBI->connect ($dbconnstr, "", "", {
       RaiseError => 1, AutoCommit => 0
        }) or die "Cannot connect: $DBI::errstr";

  my $query = 'SELECT * FROM Prompts';
    
  my $sth   = $dbh->prepare ($query);
  $sth->execute ();
  my $wording;
  while (my $row = $sth->fetchrow_hashref) {
          $wording = $row->{wording};
          $wavname = $row->{wavname};
          $wording =~ s/\&#39;/'/g;
          $wording =~ s/\&quot;/"/g;
          printf "%s\t%s\n", $wavname, $wording;
  }
  $sth->finish ();
  $dbh->disconnect();
}

sub createDatabase {
  my ($dbname) = @_;
 
  if ( -e $dbname) {
     $|=1;
     die "Database $dbname already exists; aborting operation";
  }

  my $dbconnstr = sprintf "dbi:SQLite:dbname=%s", $dbname;

  my $dbh = DBI->connect ($dbconnstr, "", "", {
       RaiseError => 1, AutoCommit => 0
        }) or die "Cannot connect: $DBI::errstr";

  $dbh->do ("CREATE TABLE GrammarResponseTagMap (grammar VARCHAR(64), response VARCHAR, tag CHAR(256), UNIQUE(grammar,response) )" );
  $dbh->do ("CREATE TABLE Prompts (id INTEGER PRIMARY KEY, wavname CHAR(64), wording VARCHAR, UNIQUE(wavname))");
  $dbh->commit;
  my $standardprompts = sprintf "%s/Utilities/standard_prompts.txt", $ENV{'ENTRAI_HOME'};
  loadPrompts($dbname,$standardprompts); 
}



sub loadGrammars {#Deletes any previous grammar entries and recreates from input file

   my ($dbname, $inputfile, $newload ) = @_;
   open (INPUT, $inputfile) || die "Can't open $inputfile";
   my %GRAMMARLIST;
   my $grammarnum;
   my $count=0;


  my $dbconnstr = sprintf "dbi:SQLite:dbname=%s", $dbname;

  my $dbh = DBI->connect ($dbconnstr, "", "", {
       RaiseError => 1, AutoCommit => 0
        }) or die "Cannot connect: $DBI::errstr";

   if ($newload) {
      $dbh->do ("DROP TABLE GrammarResponseTagMap");
   
      $dbh->do ("CREATE TABLE GrammarResponseTagMap (grammar VARCHAR(64), response VARCHAR, tag VARCHAR(256),  UNIQUE(grammar,response) )");
   }

   my $count=0;
   <INPUT>; #Go past header line
   while (<INPUT>) {
      last if /^<<END/;
      s/\s+$//;
      my ($grammar,$input, @tags) = split(/,/, $_);
      my $tag = 'out.' . join(';out.', @tags) . ';' ;
      $input = Misc::Utilities->normalizeInput ($input);

      my $query = sprintf "REPLACE INTO GrammarResponseTagMap (grammar, response, tag) VALUES (%s, %s, %s)",
        $dbh->quote($grammar), $dbh->quote($input), $dbh->quote($tag);

      $dbh->do($query);
 
      if ($count == 1000 ) { 
        $dbh->commit;
        $count = 0;
      }
      $count = $count+1;
   }
   $dbh->commit;
}




sub updatePrompts{

   my ($dbname, $inputfile ) = @_;
   open (INPUT, $inputfile) || die "Can't open $inputfile";

  my $dbconnstr = sprintf "dbi:SQLite:dbname=%s", $dbname;

  my $dbh = DBI->connect ($dbconnstr, "", "", {
       RaiseError => 1, AutoCommit => 0
        }) or die "Cannot connect: $DBI::errstr";

   <INPUT>; #Read past header
   my $count=0;
   while (<INPUT>) {
      s/\s+$//;
      s/'/\&#39;/g;
      s/"/\&quot;/g;
      my($wavname,$text) = split(/\t/, $_);
     next if $wavname =~ /^\s*$/;
     #next if $text =~ /^\s*$/;
   
      $text = Misc::Utilities->removeNonPrintable($text);
      my $query = sprintf "REPLACE INTO Prompts (wavname, wording) VALUES (%s, %s)", $dbh->quote($wavname), $dbh->quote($text);
      $dbh->do ($query);
   

      if ($count == 1000 ) { 
         $dbh->commit;
         $count = 0;
      }
      $count = $count+1;

   }
   $dbh->commit;
}


sub loadPrompts {

   my ($dbname, $inputfile ) = @_;
   open (INPUT, $inputfile) || die "Can't open $inputfile";

  my $dbconnstr = sprintf "dbi:SQLite:dbname=%s", $dbname;

  my $dbh = DBI->connect ($dbconnstr, "", "", {
       RaiseError => 1, AutoCommit => 0
        }) or die "Cannot connect: $DBI::errstr";

  $dbh->do("DROP TABLE Prompts");
  $dbh->do ("CREATE TABLE Prompts (id INTEGER PRIMARY KEY, wavname CHAR(64), wording VARCHAR, UNIQUE(wavname))");

   <INPUT>; #Read past header
   my $count=0;
   while (<INPUT>) {
      s/\s+$//;
      s/'/\&#39;/g;
      s/"/\&quot;/g;
      my($wavname,$text) = split(/\t/, $_);
      next if $wavname =~ /^\s*$/;
      #next if $text =~ /^\s*$/;
 
      $text = Misc::Utilities->removeNonPrintable($text);
      my $query = sprintf "REPLACE INTO Prompts (wavname, wording) VALUES (%s, %s)", $dbh->quote($wavname), $dbh->quote($text);
      $dbh->do ($query);
      

      if ($count == 1000 ) { 
         $dbh->commit;
         $count = 0;
      }
      $count = $count+1;

   }

   $dbh->commit;
}


