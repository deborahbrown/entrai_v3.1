#!/cygdrive/C/Strawberry/perl/bin/perl
use XML::Parser;
use Data::Dumper;
use lib "C:/Users/7DQVN32/Documents/Projects/entrAIProject/InProgress/entrAI/lib";
use Grammar::GrammarInterpreter;

my @RULES;
my @VARIABLES;


my $testgram =<<EOGRAM;
<grammar mode="dtmf" type="application/grammar+xml" version="1.0" root="ROOTDTMF">
        <rule id="ROOTDTMF" scope="public">
          <one-of>
            <item>
              1
              <tag>foundation_fld='1';</tag>
            </item>
            <item>
              2
              <tag>foundation_fld='2';</tag>
            </item>
          </one-of>
        </rule>
      </grammar>
EOGRAM


print process_swi($testgram, 1);

print Dumper(Grammar::GrammarInterpreter->GrammarInterpreter($testgram, "3", "inline"));
exit();

sub getRoot {
  my ($grammarstr) = @_;

  open(SAVEOUT, ">&STDOUT");
  open(SAVEERR, ">&STDERR");
  open(STDOUT, ">foo$$.out");
  open(STDERR, ">&STDOUT");
  select(STDERR); $|=1;
  select(STDOUT); $|=1;
  my $xmlDoc = new XML::Parser(Style => 'Debug'); 
  my $xmlHash = $xmlDoc->parse($grammarstr);
  close(STDOUT);
  close(STDERR);
  open(STDOUT, ">&SAVEOUT");
  open(STDERR, ">&SAVEERR");

  open(DUMPED, "foo$$.out");
  my $firstline = <DUMPED>;
  my ($root) = ($firstline =~ / root\s+(\w+)/ );
  my ($tag_format) = ($firstline =~ /tag-format\s+([^\s\)]+)/);
  if (!defined $tag_format || $tag_format eq '') {
      $tag_format = 'semantics/1.0';
  }
  
  if ($root ne '') {
     close DUMPED;
     return ($root, $tag_format);
  }
  while (<DUMPED>) {
     next if $_ !~ /\(id\s+/;
     ($root) = ($_ =~/\(id\s+(\w+)/);
     close DUMPED;
     return ($root, $tag_format);
  }
  die "Couldn't find root rule in $grammarname";


}

sub getVariables {
  my ($grammarstr) = @_;


  open(SAVEOUT, ">&STDOUT");
  open(SAVEERR, ">&STDERR");

  open(STDOUT, ">foo$$.out") || die "couldn't open foo$$.out";
  open(STDERR, ">&STDOUT");
  select(STDERR); $|=1;
  select(STDOUT); $|=1;

  my $xmlDoc = new XML::Parser(Style => 'Debug'); 
  my $xmlHash = $xmlDoc->parse($grammarstr);
  close(STDOUT);
  close(STDERR);
  open(STDOUT, ">&SAVEOUT");
  open(STDERR, ">&SAVEERR");


  open(DUMPED, "foo$$.out");
  my @assignments;
  while (<DUMPED>) {
     next if ! /tag \|\|/;
     push(@assignments, /(\b[A-Za-z_]\w*)\s*=\s*['"]?\w*['"]?/g );
  }
  close(DUMPED);
  return @assignments;
}

sub getRuleNames {
  my ($grammarstr) = @_;

  open(SAVEOUT, ">&STDOUT");
  open(SAVEERR, ">&STDERR");
  open(STDOUT, ">foo$$.out");
  open(STDERR, ">&STDOUT");
  select(STDERR); $|=1;
  select(STDOUT); $|=1;
  my $xmlDoc = new XML::Parser(Style => 'Debug'); 
  my $xmlHash = $xmlDoc->parse($grammarstr);
  close(STDOUT);
  close(STDERR);
  open(STDOUT, ">&SAVEOUT");
  open(STDERR, ">&SAVEERR");

  open(DUMPED, "foo$$.out");

  while (<DUMPED>) {
     next if ! /\\\\\s*\(id\s/;
     push(@RULES, /\(id (\w+)/g );
  }
  close(DUMPED);
  return @RULES;
}






##########################

sub process_swi {
  my($grammar,  $toplevel) = @_;

 my ($rootrule, $tag_format) = getRoot($grammar);
print STDERR "rootrule is $rootrule\n";
  @RULES=();
  @VARIABLES=();

  #Get list of assignments for variable names
  push( @VARIABLES, getVariables($grammar));

  my %Variables;
  foreach my $variable (@VARIABLES) {
     $Variables{$variable}++;
  }

  my $initial_declaration = "  <item>\n    <tag>";
  my $endruleassign = "    <tag>\n    ";
  
  my $initial_declaration = sprintf " <item>\n    <tag>var %s;\n      ", join(',', keys(%Variables));
  foreach $variable (keys %Variables) {
     next if $variable eq "SWI_disallow"; #will declare these in grammar interpretation routine
     next if $variable eq "SWI_decoy";
      next if $variable eq "SWI_scoreDelta";
     $endruleassign = sprintf "%s out.%s=%s;", $endruleassign, $variable, $variable;
  }

  #Get list of rules
  my @Rulenames = getRuleNames($grammar);

  foreach $rulename (@Rulenames) {
     $initial_declaration = sprintf "%s var %s = {};", $initial_declaration, $rulename;

     if ($rulename ne $rootrule) {
        $grammar =~ s#(<rule\s[^>]*\bid\s*=\s*['"]$rulename['"][^>]*>)#$1\n  <item>\n#;

     }
     elsif (! $toplevel) {

        $grammar =~ s#(<rule\s[^>]*\bid\s*=\s*['"]$rulename['"][^>]*>)#$1\n  <item>\n#;
     }
  }


  $initial_declaration = sprintf "%s</tag>\n", $initial_declaration;
  $endruleassign = sprintf "%s\n    </tag>\n</item>\n</rule>\n", $endruleassign;

  $grammar =~ s#</rule>#$endruleassign#g;
  if ($toplevel) {
   $grammar =~ s#<rule\s[^>]*id\s*=\s*['"]$rootrule['"][^>]*>#<rule id='$rootrule' scope='public'>\n$initial_declaration\n#;
  }

  $grammar =~ s#([^\;\s])\s*</tag>#$1 \; </tag>#gm;
  
  return $grammar;
}




