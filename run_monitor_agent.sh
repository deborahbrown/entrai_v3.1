#!/bin/sh

#Monitors upstream server by sending ping every ARG1 seconds
#If unsuccessful, sends message to entrAI server

cd /data/speechsoft/apps/entrAI
. $1

ERRORLIST="7 28 55 56"

while [ true ]
do
   sleep $pollperiod
#echo "got here"
   status='success'
   curl --fail  --max-time $waittime  --silent $host
   retcode=$?
   for error in $ERRORLIST
   do
      if [ $retcode == $error ]
      then
         status=failed
      fi
   done
   if [ $status == 'failed' ]
   then
      echo "failed"
      curl -X POST -H "Content-type: application/json"  --fail  --silent --data "{\"rqst\": \"AGENT:SVRNORESPONSE\" }" --cacert cert.pem $entraihost
 #     echo "response from entrai: $?"
   else
      echo "alive"
   fi
done
    
