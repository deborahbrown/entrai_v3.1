use lib './lib';

use strict;
use warnings;


use LWP::UserAgent;
use HTTP::Cookies;
use Time::HiRes qw (time);
use HTTP::Daemon;
use HTTP::Daemon::SSL;
use HTTP::Status;
use HTTP::Response;
use JSON::Parse 'parse_json';
use Agent::Upstream;
use URI::Encode qw(uri_decode);
use URI::Escape;
use Misc::Utilities;
use Data::Dumper;

umask 000;


my $ConfigFile = shift @ARGV;
our $ServerParams;
$ServerParams=readConfig($ServerParams, $ConfigFile);
our $LogLevel = $ServerParams->{LogLevel};
my $maxAgentWait = $ServerParams->{maxAgentWait};
my $sleeptime = 1;

our $sessionid;

our %BROWSERS;
my %USERNAMES;
#our %PIPES;
my %AGENTURI;

my %CHILDPID;
my %Endpoint;
my %Session;
$SIG{CHLD} = "IGNORE";
$SIG{PIPE} = "IGNORE";
my $dbmfile = './TMP/sessioninfo';

my $d;
if ($ServerParams->{UpstreamSvrProtocol} eq 'https' ) {
 $d = HTTP::Daemon::SSL->new(
  LocalAddr => $ServerParams->{LocalAddr},
  ReuseAddr=> 1,
  ReusePort=> 1,
  LocalPort => $ServerParams->{UpstreamSvrPort},
  SSL_cert_file => $ServerParams->{SSL_cert_file},
  SSL_cert_key => $ServerParams->{SSL_cert_key},
  Listen => 5
);
}
else {
 $d = HTTP::Daemon->new(
  LocalAddr => $ServerParams->{'LocalAddr'},
  ReuseAddr=> 1,
  ReusePort=> 1,
  LocalPort => $ServerParams->{UpstreamSvrPort},
);
}

print "Please contact me at: <URL:", $d->url, ">\n";

 while (my $c = $d->accept) {

        LogEvent(4,"AGENTSVRSTART", "upstreaminterface", $d->url);

       FORK: {
        if (my $pid = fork) {
            next;
        }

 
        if (my $r = $c->get_request) {

              checkEndSession($c, $r);
              LogEvent(4,"AGENTSVRMSG", "upstreaminterface", $r->url);
   
              if ($r->url->path =~ /mstshash/) {
                  $c->send_response(RC_FORBIDDEN);
                  $c->force_last_request;
                  $c->close();
                   undef $c;
                   LogEvent(4,"AGENTSVRBADMSG", "upstreaminterface",  $r->url->path);
                   exit();
              }


       
              if ($r->url->path =~ "/fromagent" ) { #This is a message from agent to SMS
              $c->send_response(RC_OK);
              $c->force_last_request;
              $c->close();
              undef $c;
             
              (my $sessionid = $r->url->path) =~ s/.*\///;
              my ($appname) = ($r->url->path =~ /fromagent.([^\/]*)/ );
                          
              my $sessioninfo;
              my $elapsed=0;            
              do {
                sleep $sleeptime;
                $elapsed = $elapsed + $sleeptime;
                if ($elapsed > $maxAgentWait) { printf STDERR "exitting"; exit();}
                dbmopen(%Session, $dbmfile, 0666) 
                      || LogEvent(1,"AGENTSVRERR", "upstreaminterface", "CANTOPENDBM $dbmfile", $sessionid);
                 
                $sessioninfo = $Session{$sessionid};         
                dbmclose(%Session);

                
              } until (defined $sessioninfo);
              my ($queueid, $endpt) = split(/\|/, $sessioninfo);
              my $msg1 = parse_json($r->decoded_content)->{'message'}->{'text'};
              #send to entrAI server to play prompt through SMS channel
               Agent::Upstream->sendSMS($msg1, $sessionid, $ServerParams->{'PromptReplace_' . $appname} );

              exit();
          }
           if ($r->url->path =~ "/tobrowser") {
              $c->send_response(RC_OK);
              $c->force_last_request;
              $c->close();
              undef $c; 
 
              my $tmp = $r->decoded_content;
              (my $sessionid = $r->url->path ) =~ s/.*\///;
              my ($appname) = ($r->url->path =~ /tobrowser.([^\/]*)/ );

              my $sessioninfo;
 
              my $elapsed=0;            
              do {
                sleep $sleeptime;
                $elapsed = $elapsed + $sleeptime;
                if ($elapsed > $maxAgentWait) { exit();}
                dbmopen(%Session, $dbmfile, 0666) 
                      || LogEvent(1,"AGENTSVRERR", "upstreaminterface",  "CANTOPENDBM", $dbmfile, $sessionid);
                 
                $sessioninfo = $Session{$sessionid};         
                dbmclose(%Session);

                
              } until (defined $sessioninfo);
              my ($t1, $endpt) = split(/\|/, $sessioninfo);
              my $msg1 = parse_json($r->decoded_content)->{'message'}->{'text'};
              Agent::Upstream->sendToBrowser($sessionid,$r->decoded_content,$ServerParams->{'PromptReplace_' . $appname} );
              exit();
          } 



          if ($r->url->path =~ "/startagent") {
              $c->send_response(RC_OK);
              $c->force_last_request;
              $c->close();
              undef $c;
              my $tmp = $r->decoded_content;
    
              $tmp =~ s/\s+/ /g;
              
              (my $sessionid = $r->url->path ) =~ s/.*\///;
              my ($appname) = ($r->url->path =~ /startagent.([^\/]*)/ );
               
             my $endpt =  Agent::Upstream->createAgentSession($sessionid, $r->decoded_content, $appname);     
             dbmopen(%Session, $dbmfile, 0777) 
                 || 
             LogEvent(1,"AGENTSVRERR", "upstreaminterface", "CANTOPENDBM", $dbmfile, $sessionid);
;
             my $queueid = parse_json($r->decoded_content)->{from};
             $Session{$sessionid} = join('|', $queueid, $endpt);
             dbmclose(%Session);





 
              exit();
          }  

          elsif ($r->url->path eq "/fromtropo") { 
                my($msg) = ($r ->url =~ /msg=([^\&]*)/);
       
             
               ($sessionid) = ($msg =~ /^INTERPRETER:USERINPUT:([^:]*):/);
               $msg =~ s/$sessionid$//;

               $c->send_response(RC_OK, "", undef, "{ok}");
              
               $c->force_last_request;

    
               $c->close();
                    
               undef $c;

       #parse request to get $message
               my($text) = ($msg =~ /INTERPRETER:USERINPUT:[^:]+:([^:]+):/);

               my $sessioninfo = $Session{$sessionid};
               my ($queueid, $endpt) = split(/\|/, $sessioninfo);
               my $retcd = Agent::Upstream->sendToAgent($sessionid, $text, $endpt);
               if (! $retcd) {
                  Agent::Upstream->sendEntraiRqstFromUpstream (
                    'AGENT:SVRTIMEDOUT', sessionid => $sessionid, msg => "none"
                  );
               }
               exit();
               
                    
         }    
          elsif ($r->url->path eq "/frombrowser") { 
               my $json_obj = parse_json($r->decoded_content);
               my $sessionid = $json_obj->{'sessionid'};
               my $msg = $json_obj->{'msg'};

               $c->send_response(RC_OK, "", undef, "{ok}");            
               $c->force_last_request; 
               $c->close();                 
               undef $c;

               dbmopen(%Session, $dbmfile, 0666)
                   || LogEvent(1,"AGENTSVRERR", "upstreaminterface", "CANTOPENDBM", $dbmfile, $sessionid);
               
               my $sessioninfo = $Session{$sessionid};
               dbmclose(%Session);
  
               if (!defined $sessioninfo) { exit(); }             
               my ($queueid, $endpt) = split(/\|/, $sessioninfo);
               my $retcd = Agent::Upstream->sendToAgent($queueid, $msg, $endpt);
               if (! $retcd) {
                  Agent::Upstream->sendEntraiRqstFromUpstream (
                    'AGENT:SVRTIMEDOUT', sessionid => $sessionid, msg => "none"
                  );
               }
               
               exit();
               
                    
         }   
         


          else { #don't know how to handle path
              $c->send_error(400);            
              $c->force_last_request;
              $c->close();
              undef $c;
              exit();     
          }

        }
     }
}


sub checkEndSession {
   my ($c, $r) = @_;

   if ($r->url->path =~ "/fromagent" or $r->url->path =~ "/tobrowser" ) {
        my $message = parse_json($r->decoded_content)->{'complete'};


         #Handle close agent session message
         if (defined $message) {
              $c->force_last_request;
              $c->close();
              undef $c;
              Agent::Upstream->sendEntraiRqstFromUpstream (
               'ENDSESSION', sessionid => $sessionid, msg => "none"
               );
              exit(); 
          }

     } 
     return;
}
