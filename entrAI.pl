if (@ARGV < 1 ) {
   die "Usage: perl entrAI <config file>";
}

my $ConfigFile  = shift @ARGV;

use lib './lib';
use Net::WebSocket::Server;
use VoiceXML::Client;
use LWP::UserAgent;
use HTTP::Cookies;
use Agent::Upstream;
use Agent::GenericAgent;
use Misc::Session;
use HTTP::Request;
use POSIX":sys_wait_h";
use Misc::Utilities;
use JSON::Parse 'parse_json';
use JSON::Create 'create_json';
use Sys::HostAddr;
use IO::Socket::SSL;
use URI::Escape;
use strict;
use Data::Dumper;
use entraiMsgs::processMsg;



our $ConfigParams;
our $ServerParams;
$ServerParams = readConfig($ServerParams, $ConfigFile);
my $port=$ServerParams->{port};
our $LogLevel = $ServerParams->{LogLevel};


our $random100;
our $session;
our $sessionid;
our $SESSIONS;
our $connopen;

$SIG{PIPE} = "IGNORE" ;
$SIG{CHLD} = "IGNORE";

sub HANDLER { 
     die "client_disconnect";
}
my $sysaddr = Sys::HostAddr->new();
our $ip = $sysaddr->main_ip();
#$ServerParams->{ip} = $sysaddr->main_ip();

our $ReportDBConnInfo = {
     connstr => $ServerParams->{DBconnstr},
     user => $ServerParams->{DBuser},
     pwd => $ServerParams->{DBpassword}
}; 
my $dbh = ConnectToDB($ReportDBConnInfo);
#our $EntraiConfig = getConfigFromDB($dbh);
our $EntraiConfig = {
    TuningDataPct => $ServerParams->{TuningDataPct}
};

printf STDERR "Starting entrAI server on port $port, press Ctr-C to disconnect\n";

sub getKeyPasswd {
    return $ServerParams->{SSL_key_passwd};
}

# simple server
my ($ssl_server, $websocket_port);

if ($ServerParams->{Protocol} eq 'wss') {
  $ssl_server = IO::Socket::SSL->new(
    # where to listen
    LocalAddr => $ServerParams->{LocalAddr},
    LocalPort => $port,
    Listen => 10,
    Proto => 'tcp',
    ReuseAddr => 1,
    ReusePort => 1,
    Blocking => 0,
    SSL_cert_file => $ServerParams->{SSL_cert_file},
    SSL_key_file => $ServerParams->{SSL_key_file},
    SSL_passwd_cb => \&getKeyPasswd,
 ) or die "failed to listen: $!";
 $websocket_port = $ssl_server;
} 
else {
  $websocket_port = $ServerParams->{port};
}
 

my $server = Net::WebSocket::Server->new(
    listen => $websocket_port,
    silence_max => $ServerParams->{silence_max},
    tick_period => $ServerParams->{tick_period},

    #check if connections have been heard from lately; if not terminate them

    on_tick => sub {
       foreach my $sessionid (keys %$SESSIONS  ) {
          my $session = $SESSIONS->{$sessionid};
          next if !defined $session->{channeltype} or $session->{channeltype} ne 'CHAT';
          if ($session->{isalive})  {
               $session->{isalive} = 0;  #Reset timer for this session
          }
          else { #This connection hasn't been heard from recently; end it
                ReportEvent $session, $EntraiConfig, $dbh, {sessionid => $sessionid, entraiapp => $session->{entraiapp}, cvpapp => '', 
                                         entraiserver => $main::ip, cvpserver => '',
                                         agentserver => '', event => 'BOT_Complete', subevent => 'Client_DisConn_Timeout', logdata => ''};
               kill 'TERM', $session->{childpid};

          }
       }
    },

    on_connect => sub {
        my ($serv, $conn) = @_;
        $conn->on(

            request => sub { #This is how an SMS application will communicate
                $connopen=1;
                my ($conn, $rqst)=@_;                
                my ($endpt, $msg) = ($rqst =~ m#GET\s*/([^\?]*)\?*(.*)\s+HTTP#);
		if (defined $endpt) { #was a GET
                  $msg = uri_unescape($msg); 
                  entraiMsgs::processMsg::processMsg("http", $conn, $msg, $dbh);
		}
		else { #a Post request
                 ($endpt) = ($rqst =~ m#POST\s*/([^\?]*)\?*.*\s+HTTP#);
    		  my ($sz) = ($rqst =~ /Content-Length:\s*([0-9]+)/);
		  $msg = substr($rqst, length($rqst) - $sz, $sz);
		  entraiMsgs::processMsg::processMsg("http", $conn, $msg, $dbh);
		}

           },

            utf8 => sub {
                my ($conn, $msg) = @_;
                if (defined  $conn->{'sessionid'}) 
                 { $SESSIONS->{$conn->{'sessionid'}}->{'isalive'}=1;}
                else {
                   #Determine % calls to log
                   $random100 =  int(rand(100)) + 1 ; 
                   $session = Misc::Session->new($random100);
                   ReportEvent $session,
                      $EntraiConfig,
                      $dbh,
                      {sessionid => '',
                         entraiapp => '',
                         cvpapp => '',
                         entraiserver => $main::ip,
                         cvpserver => '',
                         agentserver => '',
                         event => 'Client_Conn_Received',
                         subevent => '',
                         logdata => ''};
                }
                entraiMsgs::processMsg::processMsg("ws", $conn, $msg, $dbh);
            },

           pong => sub {
               if (defined  $conn->{sessionid}) 
                 { $SESSIONS->{$conn->{sessionid}}->{isalive}=1;}
           }

      );
 }
);
$server->start;


