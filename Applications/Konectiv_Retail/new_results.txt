Grammar,Input,RecoStatus,Interpretation
getQueryType.grxml,delivery,MATCH,+foundation_fld=delivery
delivery01.grxml,where is my item,MATCH,+foundation_fld=track
postcode.grxml,112233,MATCH,+MEANING=112233+SWI_meaning=112233
getYesNo.grxml,no,MATCH,+foundation_fld=no
return_text.grxml,great day! great day the righteous marching,MATCH,+foundation_fld=great day great day the righteous marching
return_text.grxml,hello world,MATCH,+foundation_fld=hello world
return_text.grxml, tomorrow is another day,MATCH,+foundation_fld=tomorrow is another day
