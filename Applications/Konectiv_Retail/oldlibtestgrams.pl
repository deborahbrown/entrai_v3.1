use lib 'C:/Users/7DQVN32/Documents/Projects/entrAIProject/Production/entrAI/lib/Grammar0608';
use GrammarInterpreter;
use Data::Dumper;
use strict;
=head
if (@ARGV < 1) {
print STDERR $ARGV[0];
    die 'Usage: perl testgram.pl <testfile>';
=cut


my $testfile = shift;

print "Grammar,Input,RecoStatus,Interpretation\n";
open(TESTFILE, $testfile) or "Can't open $testfile";
while (<TESTFILE>) {
   s/\s+$//;
   next if /^\s*$/;
   my($grammar, $userinput) = split(/,/);

   my $recoObj = Grammar::GrammarInterpreter->GrammarFileInterpreter($grammar,$userinput);
   my $interpretation = '';
   my $status = $recoObj->{'status'};
   if ($status eq 'MATCH') {
     my %slotpairs = %{$recoObj->{'interpretation'}};
     foreach my $slot (sort keys %slotpairs) {
       $interpretation = sprintf "%s+%s=%s", $interpretation, $slot, $slotpairs{$slot};
     }
   }
   printf "%s,%s,%s,%s\n", $grammar, $userinput, $status, $interpretation;
}
