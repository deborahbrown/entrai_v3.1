Name	Wording
FA0290-ini-1	For the *same* patient or a *different* one?
FA0290-exit	Okay�
FA0290-00	That was �the same one,� right?
FA0290-01	That was �a different one,� right?
FA0300-CS-1	To *transfer* you to the right place, I need this information.
FA0300-CS-2	Okay, please enter or say the customer�s 9-digit Cigna ID or social security number.
FA0300-CS-3	Okay, please say or enter the primary account owner�s 9-digit Cigna ID or social security number.
FA0300-00	�right?
FA0300-01	You�re not sure, right?
FA0300-ini-2	Now enter or say the *next* patient�s Cigna ID or social security number.
FA0300-ini-3	Okay. What's the Cigna ID or the Social Security Number of the primary account holder?
FA0300-00	�right?
FA0300-01	You�re not sure, right?
FA0310-ini-2	And what's your patient's date of birth?
FA0310-CS-1	To *transfer* you to the right place, I need this information.
Silence Prompt	<500 ms>
FA0310-CS-2	What�s your *patient�s* full date of birth?
FA0310-CS-3	Okay, what�s the full date of birth for the *person* you�re *calling* about? If you don�t know it, say �Not Sure�.
FA0310-00	�correct?
FA0310-01	You said you don�t know it, right?
FA0350_ini-3	That's... 
FA0350_ini-4	, right?
FA0350-ini-3	That's...  
FA0350-ini-4	, right?
FA3004_ini-1	For information about *this* benefit you�ll need to speak to someone.
FA3037_ini-2-alt	Do you want in-network coverage, out-of-network, or both?
FA3037_ini-2	Is that *in-network* or *out-of-network*?
FA3037_conf-1	You said in-network, right? 
FA3037_conf-2	You want out-of-network.right? 
FA3037_conf-3	You said both.right? 
FA3471_ini-1	This plan does not cover chiropractic care services.
FA3471_ini-2	By continuing, you accept and agree that the following information does not guarantee coverage or payment. We'll make a decision using the patient's Summary Plan Description, including its benefit limits and exclusions. Cigna pays for services that are medically necessary, meet eligibility requirements and that we cover under the plan.<br><br>For Chiropractic Care services, the co-pay is $35 per visit. For Cigna Designated doctors, the plan pays 80% co-insurance. The maxiumum number of visits is 10 per year.
FA3490_ini-3	<br><br>What else can I help you with?
FA3490_conf-1	You want a fax, right?
FA3490_conf-2	You want to hear deductible and out-of-pocket information, right
FA3490_conf-3	You want to hear a specific medical coverage, right?
FA3490_conf-4	Precertification, right?
FA3490_conf-5	Another member, right?
FA3040_ini-4	What coverage?
FA3040_conf-1	That�s office visits, right?
FA3040_conf-2	That�s hospital services right?
FA3040_conf-5	That�s physical therapy, right?
FA3040_conf-6	That�s mental health, right?
FA3040_conf-7	That�s surgery, right?
FA3040_conf-16	That�s chiropractic care, right?
FA3040_conf-17	You want to hear the next list, right?
FA3050_conf-10	That�s emergency room, right?
FA3041_ini-2	Second List: 1 � Medical Equipment; 2 � Lab Tests; 3 � Xrays; 4 � Preventive care.  5- Other Services.  6 Repeat That. 7 � Previous list.
FA3041_conf-3	That's medical equipment, right?
FA3041_conf-1	That�s lab tests, right?
FA3041_conf-2	That�s xrays, right?
FA3041_conf-4	That's preventive services, right?
FA3041_conf-5	Something else, right?
FA3041_conf-6	That�s emergency room, right?
FA3041_conf-7	You want to return to the previous list, right?
FA3115_ini-1	What coverage?
FA3115_conf-23	That�s ... American Well, right?
FA3115_conf-1	That�s� annual exam, right?
FA3115_conf-2	That�s� chiropractic, right?
FA3115_conf-3	That�s� diabetic supplies, right?
FA3115_conf-4	That�s� emergency room, right?
FA3115_conf-26	That�s ... home health care, right?
FA3115_conf-5	That�s� mammogram, right?
FA3115_conf-24	That�s ... MD Live, right?
FA3115_conf-6	That�s� pharmacy, right?
FA3115_conf-7	That�s� radiology and scans, right?
FA3115_conf-8	That�s� out-patient services, right?
FA3115_conf-9	Is that rehab for mental health reasons?
FA3115_conf-10	That�s�.substance abuse, right?
FA3115_conf-25	You�re calling about telehealth, right?
FA3115_conf-11	That�s� urgent care, right?
FA3115_conf-22	That�s� well child care, right?
FA0110-00	Welcome to our Provider chat demo.
FA1520-ini-1	What are you looking to do?
FA1520-CS-1	To get you to the right place, I need to know what you�re calling about today.
FA1520-CS-2	Please Say �Eligibility and Coverage� or press 1, �Claims� or press 2, �Precertification and PreTreatment� or press 3, �Specialist referral� or press 4. �Cigna Provider Network Questions� or press 5. or �Pharmacy Questions� or press 6.
FA1520-00	Eligibility and Coverage, right?
FA1520-01	Claims, right?
FA1520-02	Precertification, right?
FA1520-08	Referrals right?
FA1520-03	Pharmacy questions, right?
FA1520-04	Mailing address, right?
FA1520-05	Claims status, right?
FA1520-06	Dental pretreatment, right?
FA1520-07	Network questions, right?
FA0110-1	Welcome to our Provider Chat Demo.
MedicalSpecific	For Physical Therapy, the co-pay is $25 per visit. For Cigna Designated doctors, the plan pays 80% co-insurance. The maximum number of visits is 20 per year of which he has 15 remaining.
FA3271_ini-1	By continuing, you accept and agree that the following information does not guarantee coverage or payment. We'll make a decision using the patient's Summary Plan Description, including its benefit limits and exclusions. Cigna pays for services that are medically necessary, meet eligibility requirements and that we cover under the plan.
FA3271_ini-2	For Chiropractic Care services...the co-pay is $35 per visit. The maxiumum number of visits is 10 per year.
