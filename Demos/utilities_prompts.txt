Name	Wording
UT0020_out_01	Hi. This is the EntrAI Chatbot. 
UT0025_ini_01	What's your account number?
UT0025_ni1_01	Sorry. What's your account number?
UT0025_ni2_01	Sorry, to get started, what's your account number?
UT0025_nm1_01	Sorry. What's your account number?
UT0025_nm2_01	Sorry, to get started, what's your account number?
UT0025_conf_01	That's 
UT0025_conf_02	 right?
UT0035_out_01	Sorry, I couldn't find an account with that number. Let's try it again.
UT0050_ini_01	I see there is an outage in your area. Is that why you're contacting us today?
UT0050_ni1_01	Sorry. Are you contacting us today because of an outage in your area?
UT0050_ni2_01	Sorry, yes or no, are you contacting us today because of an outage in your area?
UT0050_nm1_01	Sorry. Are you contacting us today because of an outage in your area?
UT0050_nm2_01	Sorry, yes or no, are you contacting us today because of an outage in your area?
UT0060_out_01	A crew has been dispatched to make the repair. The repair is expected to be complete today.
UT0100_ini_01	What can I help you with? 
UT0100_ini_02	What else can I help you with today?
UT0100_ni1_01	Sorry. What can I help you with? I can help with things like �when is my bill due' or �what is the green plan'.
UT0100_ni2_01	Sorry, what can I help you with today? 
UT0100_nm1_01	Sorry. What can I help you with? I can help with things like �when is my bill due' or �what is the green plan'.
UT0100_nm2_01	Sorry, what can I help you with today?
UT0100_conf_01	You'd like the bill due date, right?
UT0100_conf_02	You'd like to know about the green plan, right?
UT0100_conf_03	You'd like to get information about a plan, right?
UT0100_conf_04	You'd like to know about the real time plan, right?
UT0100_conf_05	You'd like to get information about a plan, right?
UT0100_conf_06	You'd like to know about the standard plan, right?
UT0100_conf_07	You'd like to get information about a plan, right?
UT0100_conf_08	You'd like to learn about our plans, right?
UT0100_conf_09	You're all set for now, right?
UT0200_out_01	Your bill is due today.
UT0200_out_02	Your bill is due tomorrow.
UT0200_out_03	Your bill is due on 
UT0300_ini_01	You're currently on the standard plan. We have a few plans available. Which are you interested in, the green plan or the real time plan?
UT0300_ini_02	You're currently on the green plan. We have a few plans available. Which are you interested in, the standard plan or the real time plan?
UT0300_ini_03	You're currently on the real time plan. We have a few plans available. Which are you interested in, the green plan or the standard plan?
UT0300_ni1_01	Sorry. Which plan are you interested in, the green plan or the real time plan?
UT0300_ni1_02	Sorry. Which plan are you interested in, the standard plan or the real time plan?
UT0300_ni1_03	Sorry. Which plan are you interested in, the green plan or the standard plan?
UT0300_ni2_01	Sorry, you can get information about our other plans. Which one are you interested in, the green plan or the real time plan?
UT0300_ni2_02	Sorry, you can get information about our other plans. Which one are you interested in, the standard plan or the real time plan?
UT0300_ni2_03	Sorry, you can get information about our other plans. Which one are you interested in, the green plan or the standard plan?
UT0300_nm1_01	Sorry. Which plan are you interested in, the green plan or the real time plan?
UT0300_nm1_02	Sorry. Which plan are you interested in, the standard plan or the real time plan?
UT300_nm1_03	Sorry. Which plan are you interested in, the green plan or the standard plan?
UT0300_nm2_01	Sorry, you can get information about our other plans. Which one are you interested in, the green plan or the real time plan?
UT0300_nm2_02	Sorry, you can get information about our other plans. Which one are you interested in, the standard plan or the real time plan?
UT0300_nm2_03	Sorry, you can get information about our other plans. Which one are you interested in, the green plan or the standard plan?
UT0300_conf_01	That's the green plan, right?
UT0300_conf_02	That's the real time plan, right?
UT0300_conf_03	That's the standard plan, right?
UT0310_ini_01	Our green plan encourages you to use your electricity during off peak times. You don't pay for electricity on weekends. It's our cheapest rate. Would you like to sign up, get more details about plans, or was that it for today?
UT0310_ini_02	That's our green plan to encourage you to use your electricity during off peak times You don't pay for electricity on weekends. It's our cheapest rate. Would you like to sign up, get more details about plans, or was that it for today?
UT0310_ni1_01	Sorry. What would you like to do? Sign up for the green plan, get more details about plans, or wrap up for now?
UT0310_ni2_01	Sorry, which would you like? Sign up for the green plan, get more details about plans, or wrap up for now?
UT0310_nm1_01	Sorry. What would you like to do? Sign up for the green plan, get more details about plans, or wrap up for now?
UT0310_nm2_01	Sorry, which would you like? Sign up for the green plan, get more details about plans, or wrap up for now?
UT0310_conf_01	You'd like to sign up for the green plan, right?
UT0310_conf_02	You'd like to get more details about plans, right?
UT0310_conf_03	You're all set for now, right?
UT0320_ini_01	Our real time plan bills you according to when you use electricity. You're billed according to the price of electricity when you use it, according to your smart meter. Would you like to sign up, get more details about plans, or was that it for today?
UT0320_ini_02	That's our real time plan to encourage you to use your electricity during off peak times You're billed according to the price of electricity when you use it, according to your smart meter. Would you like to sign up, get more details about plans, or was that it for today?
UT0320_ni1_01	Sorry. What would you like to do? Sign up for the real time plan, get more details about plans, or wrap up for now?
UT0320_ni2_01	Sorry, which would you like? Sign up for the real time plan, get more details about plans, or wrap up for now?
UT0320_nm1_01	Sorry. What would you like to do? Sign up for the real time plan, get more details about plans, or wrap up for now?
UT0320_nm2_01	Sorry, which would you like? Sign up for the real time plan, get more details about plans, or wrap up for now?
UT0320_conf_01	You'd like to sign up for the real time plan, right?
UT0320_conf_02	You'd like to get more details about plans, right?
UT0320_conf_03	You're all set for now, right?
UT0330_ini_01	Our standard plan bills you one rate all the time. It is our basic plan. Would you like to sign up, get more details about plans, or was that it for today?
UT0330_ini_02	That's our standard plan. You're billed one rate all the time. Would you like to sign up, get more details about plans, or was that it for today?
UT0330_ni1_01	Sorry. What would you like to do? Sign up for the standard plan, get more details about plans, or wrap up for now?
UT0330_ni2_01	Sorry, which would you like? Sign up for the standard plan, get more details about plans, or wrap up for now?
UT0330_nm1_01	Sorry. What would you like to do? Sign up for the standard plan, get more details about plans, or wrap up for now?
UT0330_nm2_01	Sorry, which would you like? Sign up for the standard plan, get more details about plans, or wrap up for now?
UT0330_conf_01	You'd like to sign up for the standard plan, right?
UT0330_conf_02	You'd like to get more details about plans, right?
UT0330_conf_03	You're all set for now, right?
UT0800_out_01	Thank you for using the EntrAI Chatbot.
UT9010_out_01	Let me get an agent to help with that.
UT9010_out_02	Please hold while your call is connected
