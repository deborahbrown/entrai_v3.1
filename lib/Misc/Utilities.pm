package Misc::Utilities;

use strict;
use warnings;
#use HTTP::Request;
#use LWP::UserAgent;
#use DBD::ODBC  1.58;
use Time::HiRes qw(time);
use POSIX qw(strftime);
use Data::Dumper;
#use entraiMsgs::runTheApplication;
use Exporter  qw(import);
our @ISA = qw(Exporter);
our @EXPORT = qw(readConfig ReportEvent ConnectToDB LogRecogEvent LogEvent);


sub readConfig {
   my($Config, $configfile) = @_;
   open(CONFIG, "$configfile") || die "Can't read $configfile\n";
   while (<CONFIG>) {
     s/\s+$//;
     s/#.*//;
     my($param, $value) = split(/=/, $_, 2);
     next if (!defined $param or !defined $value);
     $value =~ s/[\'\"]//g;
     $Config->{$param} = $value; 
   }
  close CONFIG;
  return $Config;
}

sub removeNonPrintable {
    my ($self, $str) = @_ ;
    $str =~ s/^a-zA-Z0-9\s\~\`\@#\$%^\&\*\(\)_\-+=\|\{\}\[\]\'\":;\<\>,\.\?\///g;
    return $str;
}

sub normalizeInput {
    my ($self, $str) = @_;
    if (!defined $str) {return;}
    $str =~ s/^\s*([^\s\\])/\\$1/m;    #This prevents warning that $str is not defined; but not sure why it works or is needed
    $str =~ tr/A-Z/a-z/;
    $str =~ s/^\s+//om;
    $str =~ s/\s+$//om;
    $str  =~ s/\s+/ /omg;
    $str =~ s/\-/ /omg;
    $str =~ s/\// /omg;
    $str =~ s/([^\w\s])+//omg;
    return $str;
}

my @Levels = ('NOMG', 'CRIT', 'WARN', 'INFO', 'DBUG'); 

sub LogEvent {
    my $msglevel = shift;
    my $event = shift;
    my @info = @_;

    return if ($msglevel > $main::LogLevel); 

    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
    my $now = sprintf "%2.2d/%2.2d/%2.2d %2.2d:%2.2d:%2.2d", $mon+1, $mday, $year+1900, $hour, $min, $sec;    
    my $file =  sprintf "./Applications/Logs/events_%2.2d%2.2d%2.2d.log", $year+1900, $mon+1, $mday;
    open(LOG, ">>$file");
    if (defined $info[0]) {
       syswrite(LOG, join("\|", $Levels[$msglevel], $now, $event, @info, "\n"), 512);
    } else {
       syswrite(LOG, join("\|", $Levels[$msglevel], $now, $event, "\n"), 512);
     }
    
    close(LOG);
}


my $query_TEMPLATE =<<END_TEMPLATE;
INSERT INTO EVENTS
(callid, eventtime, entraiapp, cvpapp,entraiserver, cvpserver, agentserver, event, subevent)
VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)
END_TEMPLATE

our $dbh;
sub ConnectToDB {
   my ($connectinfo) = @_;
   eval {
   $dbh = DBI->connect($connectinfo->{connstr},
                     $connectinfo->{user},
                     $connectinfo->{pwd},
                    {mariadb_auto_reconnect => 1, AutoCommit => 1});
   } ;
   if ($@) {
     LogEvent(2, 'DBERR', $@);
     return undef;
   }
   bless $dbh, 'DBI::db';
   return $dbh;
}

my $defaultEntraiConfig = {
   TuningDataPct => 100,
   ReportDataPct => 100
};


sub ReportEvent {
    my ( $session, $EntraiConfig, $dbh,  $values) = @_;
    #values contains information for report; $logdata is any additional info for local log files

    my $t = time;
    my $date = strftime "%Y-%m-%d %H:%M:%S", localtime $t;
    $date .= sprintf ".%03d", ($t-int($t))*1000; # without rounding

    my $ReportDataPct = $main::ConfigParams->{RptDataPct} ?
           $main::ConfigParams->{RptDataPct}
           : $main::ServerParams->{RptDataPct};


    if ( $ReportDataPct and $session->{random100} <= $ReportDataPct ) {
      eval {
      my $query = sprintf $query_TEMPLATE, 
              $dbh->quote($values->{sessionid}),
              $dbh->quote($date),
              $dbh->quote($values->{entraiapp}),
              $dbh->quote($values->{cvpapp}),
              $dbh->quote($values->{entraiserver}),
              $dbh->quote($values->{cvpserver}),
              $dbh->quote($values->{agentserver}),
              $dbh->quote($values->{event}),
              $dbh->quote($values->{subevent});              
              $dbh->do($query);
      };
      if ($@) {
        LogEvent(2, 'DBERR', $@);
        return;
      }
    }
    my @logdata;
    foreach my $k ( 'event', 'sessionid', 'entraiapp', 'cvpapp', 'entraiserver',
                 'cvpserver', 'agentserver', 'subevent', 'logdata') {
        my $data = sprintf "%s=%s", $k, $values->{$k};
        push(@logdata, $data);
    }
    my $severity = defined($values->{severity}) ? $values->{severity} : 3;
    LogEvent($severity, @logdata);

}

my $recog_TEMPLATE =<<END_TEMPLATE;
INSERT INTO RECOG
(callid, eventtime, grammar, input,  status, interpretation)
VALUES (%s, %s, %s, %s, %s, %s)
END_TEMPLATE


sub LogRecogEvent {
    my ( $session, $EntraiConfig, $sessionid, $maskflag, $grammar,  $input, $status, $interpretation) = @_;
    my $TuningDataPct = $main::ConfigParams->{TuningDataPct} ?
           $main::ConfigParams->{TuningDataPct}
           : $main::ServerParams->{TuningDataPct};
    if (! $TuningDataPct or $session->{random100} > $TuningDataPct ) {
          return;
    }

    my $t = time;
    my $date = strftime "%Y-%m-%d %H:%M:%S", localtime $t;
    $date .= sprintf ".%03d", ($t-int($t))*1000; # without rounding

    $grammar =~ s/.*\///;
    if ($maskflag) {
      $input = 'XXXXXX';
      $interpretation ='XXXXXX';
    }

    eval {
    my $query = sprintf $recog_TEMPLATE, 
              $VoiceXML::Client::UserAgent::child_dbh->quote($sessionid),
              $VoiceXML::Client::UserAgent::child_dbh->quote($date),
              $VoiceXML::Client::UserAgent::child_dbh->quote($grammar),
              $VoiceXML::Client::UserAgent::child_dbh->quote($input),
              $VoiceXML::Client::UserAgent::child_dbh->quote($status),
              $VoiceXML::Client::UserAgent::child_dbh->quote($interpretation);              
              $VoiceXML::Client::UserAgent::child_dbh->do($query);
    } ;
   if ($@) {
     LogEvent(2, 'DBERR', $@);
     return $defaultEntraiConfig;
   }
}


1;
