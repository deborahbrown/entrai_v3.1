package Misc::Session;

sub new {
	my ($class, $random100, $id, $config, $channeltype, $user, $browser) = @_;
	my $self = {};
     bless $self, 'Misc::Session';
	
     $self->{random100} = $random100;
     if (!defined $id) {
           $self->{'id'} = ' ';
           return $self;
     }
	$self->{'id'} = $id;
	$self->{'state'} = 'IVR';
      $self->{'channeltype'} = $channeltype; #either SMS or CHAT
      $self->{'childpid'} = undef;
      $self->{'pipe'} = undef;
      $self->{'config'} = $config;
      $self->{'_sessionVariables'} = {};
      if (defined $browser ) {
          $self->{'browser'} = $browser;
          $self->{'username'} = $user;
       }
       else {
              $self->{'browser'} = undef;
       }
      if ($channeltype eq 'SMS') {
          $self->{usernum} = $user;
      }
	$self->setActive();
      $self->{'isalive'} = 1;

     ($self->{entraiapp} = $config ) =~ s/\.config$//;
     $self->{entraiapp} =~ s/.*\///;
	return $self;
}

sub setActive {
   my $self = shift;
   $self->{'active'} = 1;
}

sub setInactive {
   my $self = shift;
   $self->{'active'} = 0;
}

sub isActive {
   my $self = shift;
   return $self->{'active'};
}

sub setSessionVariable {
   my $self = shift;
   my $name = shift;
   my $value = shift;

   $self->{'_sessionVariables'}->{$name} = $value;
   return 1;
}

sub getSessionVariables {
   my $self = shift;
   return $self->{'_sessionVariables'};
}

sub assignSessionVal {
   my $self = shift;
   my $name = shift;
   my $value = shift;
   $self->{$name} = $value;
}
sub retrieveSessionVal {
   my $self = shift;
   my $name = shift;
   return $self->{$name};
}



1;