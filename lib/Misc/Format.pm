package Misc::Format;

sub formatNumbers {
  my($self, $prompt) = @_;



  #For numbers


  #$prompt =~ s/\s+/ /g;
  #$prompt =~ s/$/ /;
  #$prompt =~ s/((((\d000)__\s*)?((\d00)__\s*)?((\d\d)__\s*)?((\d)__)?))/addthem(scalar($3), scalar($5), scalar($7), scalar($9), $1)/ge;
  $prompt =~ s/(((([0-9]000)__\s*)?(([0-9]00)__\s*)?(([0-9][0-9])__\s*)?(([0-9])__)?))/addthem($3, $5, $7, $9, $1)/ge;

  $prompt =~ s/\s+/ /g;

  #For connected digits
  $prompt =~ s/((\d__ )+)/removeUnderscore($1)/ge;


  #For dollar amounts

  $prompt =~ s/\b(\d+)\s+dollars__\b/\$$1 /go;
  $prompt =~ s/(\d)\s+(and\s+)?(\d\d?)\s+cents__/makemoney($1, $3)/geo;
  

  $prompt =~ s/negative__\s+/\-/g;
  $prompt =~ s/\s+point__\s+/\./g;
  $prompt =~ s/oh__\s+(\d[^\d])/0$1/g;

  $prompt =~ s/((\d+)\s+)?(\d+)\s+([A|P])M__/sprintf "%s$4M", maketime($2, $3)/geo;
  $prompt =~ s/__//go; 
  return $prompt;
}

sub maketime {
  ($tm1, $tm2) = @_;

  if (!defined $tm1) {
    if (length($tm2) <= 2 ) {
      return sprintf "%d:00", $tm2;
    }
    else {
      $tm2=~ s/(\d\d)$/:$1/;
      return $tm2;
    }
  }
 
  return sprintf "%s:%s", $tm1, $tm2;
 
}


sub makemoney {
  my ($dollars, $cents) = @_;

   if (length($cents) == 1) {
    $cents = "0$cents";
  }

  return sprintf "%s.%s", $dollars, $cents;
}

sub addthem {
  my ($a, $b, $c, $d, $e) = @_;

  my $sum = 0;
  if (defined($a))  { $sum = $a; } 
  if (defined($b))  { $sum += $b;}
  if (defined($c))  { $sum += $c; }
  if (defined($d))  { $sum += $d; }


  if( $sum == 0 ) {
     if (defined ($d) and $d == 0) {
        return '0__';
     } else {
       $e = sprintf "%s", $e;
       return $e;
     }
  }
  $sum = sprintf "%d", $sum;
  $sum =~ s/(.)/$1__ /g;
  $sum =~ sprintf "%s ", $sum;
  return   $sum;
}

sub removeUnderscore {
  my ($str) = @_;

  $str =~ s/__//g;
  $str =~ s/ //g;
  $str = sprintf "%s ", $str;

  return $str;
}

1;
