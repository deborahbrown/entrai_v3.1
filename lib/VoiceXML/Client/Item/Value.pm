package VoiceXML::Client::Item::Value;

use base qw (VoiceXML::Client::Item);
use VoiceXML::Client::JavaScriptEngine;
use Data::Dumper qw(Dumper);


=head1 COPYRIGHT AND LICENSE

	

=cut




use strict;

use vars qw{
		$VERSION
};

$VERSION = $VoiceXML::Client::Item::VERSION;
 
sub init {
	my $self = shift;

      if (!defined ($self->{'XMLElement'}->attribute('expr')))
      { 
         return 0;
      }
	$self->{'expr'} = $self->{'XMLElement'}->attribute('expr') ;
	$self->{'type'} = 'VoiceXML::Client::Item::Value';
	return 1; 
}

sub execute {
	my $self = shift;
	my $handle = shift;
	my $optParms = shift;
	my $fileToPlay = VoiceXML::Client::JavaScriptEngine->JSEvalExpression($self->{'expr'});
     $handle->play($fileToPlay);

	return $VoiceXML::Client::Flow::Directive{'CONTINUE'};
}


1;
