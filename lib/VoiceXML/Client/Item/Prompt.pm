
package VoiceXML::Client::Item::Prompt;


use base qw (VoiceXML::Client::Item);
use VoiceXML::Client::Util;
use Data::Dumper;


=head1 COPYRIGHT AND LICENSE

	
	Copyright (C) 2007,2008 by Pat Deegan.
	All rights reserved
	http://voicexml.psychogenic.com

This library is released under the terms of the GNU GPL version 3, making it available only for 
free programs ("free" here being used in the sense of the GPL, see http://www.gnu.org for more details). 
Anyone wishing to use this library within a proprietary or otherwise non-GPLed program MUST contact psychogenic.com to 
acquire a distinct license for their application.  This approach encourages the use of free software 
while allowing for proprietary solutions that support further development.


This file is part of VoiceXML::Client.

 
 
    VoiceXML::Client is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VoiceXML::Client is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VoiceXML::Client.  If not, see <http://www.gnu.org/licenses/>.


=cut



use strict;


use vars qw{
		$VERSION
		$DefaultTimeoutSeconds
};

$VERSION = $VoiceXML::Client::Item::VERSION;

$DefaultTimeoutSeconds = 7;

sub init {
	my $self = shift;
	
	my $timeout =  $DefaultTimeoutSeconds;
    $self->{'cond'} = "1";
    if (defined $self->{'XMLElement'}->attribute('cond') and $self->{'XMLElement'}->attribute('cond') ne '' ) {
       $self->{'cond'} = $self->{'XMLElement'}->attribute('cond');
    }
    $self->{'type'} = 'VoiceXML::Client::Item::Prompt';
    $self->{'name'} = 'prompt';

	return 1; 
	
}

sub execute {
	my $self = shift;
	my $handle = shift;
	my $optParms = shift;

     if ( VoiceXML::Client::JavaScriptEngine->JSEvalExpression($self->{'cond'})){
	   $self->executeChildren($handle, $optParms);
     }

      return $VoiceXML::Client::Flow::Directive{'CONTINUE'};
}

sub createChildren {
	my $self = shift;
	my $params = shift;


     my $children = $self->{'XMLElement'}->{'_children'};
 

	my $numChildren = scalar @{$children};

	
	$self->{'children'} = [];
	my $itemCount = 0;
	for (my $i=0; $i<$numChildren; $i++)
	{
         my $name;
         if (defined $children->[$i]->name()) {
		   $name = $children->[$i]->name();
         }
         else {

            $name = 'prompttext';
         }
       		
		if ($name eq 'var')
		{
			# Variable declaration - ensure we have an interpreter context in this scope
			
			my $varName = $children->[$i]->attribute('name');
			if (defined $varName)
			{
				$self->{'declaredVariables'}->{$varName}++;
			}
			$self->createContext() unless ($self->{'_context'});
		}
       
			
		my $newItem = VoiceXML::Client::Item::Factory->newItem($name, $children->[$i], $self);
		
		unless (defined $newItem)
		{
			next;
		}
		
		if ($newItem) 
		{
			$self->{'children'}->[$itemCount++] = $newItem;
		} 

		
	}

     
	return $itemCount;
}


sub timeoutSeconds {
	my $self = shift;
	my $setTo = shift;
	
	if (defined $setTo && $setTo =~ m/^\d+$/)
	{
		$self->{'timeoutseconds'} = $setTo;
	}
	
	return $self->{'timeoutseconds'};
}



1;
