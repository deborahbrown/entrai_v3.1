
package VoiceXML::Client::Item::Option;


use base qw (VoiceXML::Client::Item);
use Misc::Utilities;
use VoiceXML::Client::JavaScriptEngine;
use Grammar::GrammarInterpreter;


=head1 COPYRIGHT AND LICENSE

	
	Copyright (C) 2007,2008 by Pat Deegan.

    You should have received a copy of the GNU General Public License
    along with VoiceXML::Client.  If not, see <http://www.gnu.org/licenses/>.


=cut



use strict;


use vars qw{
		$VERSION
};

$VERSION = $VoiceXML::Client::Item::VERSION;

sub init {
	my $self = shift;
	
  $self->{'value'} =  $self->{'XMLElement'}->attribute('value');
  $self->{'dtmf'} =  $self->{'XMLElement'}->attribute('dtmf');
  $self->{'text'} = $self->{'XMLElement'}->getValue();
  $self->{'name'} = 'option';
  $self->{'type'} = 'VoiceXML::Client::Item::Option';

	return 1;
}

sub execute {
	my $self = shift;
	my $handle = shift;
	my $optParms = shift;

	return $VoiceXML::Client::Flow::Directive{'CONTINUE'};
}

sub checkOption {
     my $self = shift;
     my $response = shift;
     my $fieldtoset = shift;

     if ($response eq Misc::Utilities->normalizeInput($self->{'text'}) || ($response eq $self->{'dtmf'}  )) {
       return Grammar::GrammarInterpreter->convertToGrammarResult("optiongrammar", $self->{'value'}, $fieldtoset, $self->{'value'});
     }
     return undef;
}

1;
