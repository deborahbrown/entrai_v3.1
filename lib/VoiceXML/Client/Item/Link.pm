package VoiceXML::Client::Item::Link;

use base qw (VoiceXML::Client::Item);
use VoiceXML::Client::Util;
use Grammar::GrammarInterpreter;
use VoiceXML::Client::Item::Events;
use VoiceXML::Client::Item::Grammar;
use VoiceXML::Client::Flow;
use Data::Dumper qw(Dumper);


=head1 COPYRIGHT AND LICENSE

	

=cut




use strict;

use vars qw{
		$VERSION
};

$VERSION = $VoiceXML::Client::Item::VERSION;

sub init {
    my $self = shift;

    my $linkattr ;
    
    my $count=0;
    foreach my $attr ('next', 'expr', 'event', 'eventexpr') {
        if (defined( $self->{'XMLElement'}->attribute($attr)) and ($self->{'XMLElement'}->attribute($attr) ne '' ) ) {
        $count++;
       }
    }
    if ($count != 1) {
       return 0;
    }
    $count=0;
    foreach my $attr ('message', 'messageexpr') {
        if (defined( $self->{'XMLElement'}->attribute($attr)) and ($self->{'XMLElement'}->attribute($attr) ne '' ) ) {
          $count++;
        }
    }
    if ($count > 1) {
       die "link: not defined properly";
    }

    foreach my $attr ('next', 'expr', 'event', 'eventexpr', 'message','messageexpr',
                     'dtmf', 'fetchaudio', 'fetchhint', 'fetchtimeout', 'maxage', 'maxstale') {

       if (defined $self->{'XMLElement'}->attribute($attr)) {
         $linkattr->{$attr} = $self->{'XMLElement'}->attribute($attr); 
       }
       else {
          $linkattr->{$attr} = '';
       }
    }

    $self->{'type'} = 'VoiceXML::Client::Item::Link';
    return 1;
	
}

	

sub execute {
	my $self = shift;
	my $handle = shift;
	my $optParms = shift;

     
	return $VoiceXML::Client::Flow::Directive{'CONTINUE'};	
}

sub handleLinks {
    my ($self, $linkarrayref, $response, $doc) = @_;


   foreach my $link (@$linkarrayref) {
           my $result = handleOneLink($self, $link, $response, $doc);

           if (defined $result) {
             return $result;
           }
      
    }
    return undef;


}

sub handleOneLink {
    my($self,  $link, $response, $doc) = @_;

    my $recoReturn;
    my $attributes = $link->{'XMLElement'}->{'_attributes'};



    for (my $i=0; $i < scalar(@{$link->{'children'}}); $i++ ) {
        my $grammar = $link->{'children'}->[$i];

        next if !defined $grammar->{'__elemname'} or $grammar->{'__elemname'} ne 'grammar';

        $recoReturn = VoiceXML::Client::Item::Grammar::isValidInput($grammar, $response);

        next if $recoReturn->{'status'} eq 'NOMATCH';

        if ($attributes->{'event'} ne '' ) {
         
          VoiceXML::Client::Item::Events::throwEvent($doc, $attributes->{'event'}, $attributes->{'message'}, $attributes->{'messageexpr'});

          return $VoiceXML::Client::Flow::Directive{'CONTINUE'} ;
        }
        elsif ($attributes->{'eventexpr'} ne '' ) {
          my $event = VoiceXML::Client::JavaScriptEngine->JSEvalExpression($attributes->{'eventexpr'});
          VoiceXML::Client::Item::Events::throwEvent($doc, $event,$attributes->{'message'}, $attributes->{'messageexpr'});
          return $VoiceXML::Client::Flow::Directive{'CONTINUE'} ;
        } 
        else {
          my $next;
          if ($attributes->{'expr'} ne '' ) {
               $next = VoiceXML::Client::JavaScriptEngine->JSEvalExpression($attributes->{'expr'});
          }
          else {
               $next = $attributes->{'next'};
          } 
             my $vxmlDoc = $link->getParentVXMLDocument();
          if ( $next !~ /^#/) { # a uri
            
            $vxmlDoc->nextDocument($next);
            return $VoiceXML::Client::Flow::Directive{'NEXTDOC'} ;
          } #end if
          else {
             $next =~ s/^#//;
             $vxmlDoc->nextFormId($next);
             return $VoiceXML::Client::Flow::Directive{'JUMP'} ;
          } #end else
       }

 #end else

    }#end while
   return undef;
} #end sub HandleOneLink







1;
