
package VoiceXML::Client::Item::Param;


use strict;


use base qw (VoiceXML::Client::Item);
use VoiceXML::Client::Util;
use VoiceXML::Client::JavaScriptEngine;

use vars qw{
		$VERSION
};

$VERSION = $VoiceXML::Client::Item::VERSION;



=head1 COPYRIGHT AND LICENSE

	
	Copyright (C) 2007,2008 by Pat Deegan.
	All rights reserved
	http://voicexml.psychogenic.com

=cut




sub init {
	my $self = shift;
	
	my $name = $self->{'XMLElement'}->attribute('name');
	my $expr = $self->{'XMLElement'}->attribute('expr') ;
	my $value = $self->{'XMLElement'}->attribute('value') ;

     if (defined $value) {
        $self->{'value'} = $value;
     }
     elsif (defined $expr) {
        $self->{'expr'} = $expr;
     }
     else {
          return undef;
     } 

	unless (defined $name)
	{
		return undef;
	}
	
	my $vxmlDoc = $self->getParentVXMLDocument()->registerVariable($name);
	    $self->{'type'} = 'VoiceXML::Client::Item::Param';
	
	$self->{'name'} = $name;

	return 1;
	
	
}

sub execute {
	my $self = shift;
	my $handle = shift;
	my $optParams = shift;
	
	
     my $expr= $self->{'expr'};
     my  $name = $self->{'name'};
     if (defined $expr) {
       VoiceXML::Client::JavaScriptEngine->JSAssign($name, $expr, 0);
	}
      else {
         $expr = sprintf "'%s'", $self->{'value'};
         VoiceXML::Client::JavaScriptEngine->JSAssign($name, $expr, 0);
	}     
	return $self->executeChildren($handle, $optParams);
}
	



1;
