
package VoiceXML::Client::Item::Goto;


use base qw (VoiceXML::Client::Item);
use Data::Dumper;


=head1 COPYRIGHT AND LICENSE

	
	Copyright (C) 2007,2008 by Pat Deegan.
	All rights reserved
	http://voicexml.psychogenic.com

This library is released under the terms of the GNU GPL version 3, making it available only for 
free programs ("free" here being used in the sense of the GPL, see http://www.gnu.org for more details). 
Anyone wishing to use this library within a proprietary or otherwise non-GPLed program MUST contact psychogenic.com to 
acquire a distinct license for their application.  This approach encourages the use of free software 
while allowing for proprietary solutions that support further development.


This file is part of VoiceXML::Client.

 
 
    VoiceXML::Client is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VoiceXML::Client is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VoiceXML::Client.  If not, see <http://www.gnu.org/licenses/>.


=cut



use strict;

use vars qw{
		$VERSION
};

$VERSION = $VoiceXML::Client::Item::VERSION;




sub init {
	my $self = shift;

      my $count=0;
      for my $attr ('next', 'nextitem', 'expr', 'expritem') {
        if ($self->{'XMLElement'}->attribute($attr)) {
          $count++;
          $self->{$attr} = $self->{'XMLElement'}->attribute($attr); 
        }
        else {
          $self->{$attr} = '';
        }
      }
      if ($count != 1) {
        die "Bad goto element";
      }
	
	$self->{'type'} = 'VoiceXML::Client::Item::Goto';

	return 1;	
	
}


sub execute {
	my $self = shift;
	my $handle = shift;
	my $optParms = shift;

	my $vxmlDoc = $self->getParentVXMLDocument();

    my $next='';
    my $nextitem='';
    if ($self->{'expr'} ne '' ) {
         $next = VoiceXML::Client::JavaScriptEngine->JSEvalExpression($self->{'expr'});
    }
    else {
         $next = $self->{'next'};
   }
   if ($self->{'expritem'} ne '' ) {
         $nextitem = VoiceXML::Client::JavaScriptEngine->JSEvalExpression($self->{'expritem'});
   }
   else {
         $nextitem = $self->{'nextitem'};
   }   
	
	if (($next ne '') and ($next !~ /^#/) )
	{
		# a uri to go to...
		$vxmlDoc->nextDocument($next);
		
		
		return $VoiceXML::Client::Flow::Directive{'NEXTDOC'};
	}

   elsif ($next ne '' ) {
          my $next1;
          ($next1 = $next) =~ s/^#//;
          $vxmlDoc -> nextFormId($next1 );
          return $VoiceXML::Client::Flow::Directive{'JUMP'};
   }
	
  elsif ($nextitem ne '' )
  {
		my $parentForm = $self->getParentForm();


		if ($parentForm->nextItem($nextitem))
		{
			$vxmlDoc->nextFormId($parentForm->id());

		} else {


for (my $i=0; $i < scalar @{$parentForm->{'XMLElement'}->{'_children'}}; $i++) {
   my $child = $parentForm->{'XMLElement'}->{'_children'}->[$i]; 
   my $name = $child->{'_attributes'}->{'name'};

   if (defined $name && $name eq $nextitem) {
      $parentForm->{'currentchild'} = $i - 1;
      last;
   }
}
             
 
		}
		return $VoiceXML::Client::Flow::Directive{'CONTINUE'};
	}
	

	
	return $VoiceXML::Client::Flow::Directive{'ABORT'};
}
	


1;
