package VoiceXML::Client::Item::Catch;

use base qw (VoiceXML::Client::Item);
use VoiceXML::Client::Util;
use Data::Dumper qw(Dumper);

use VoiceXML::Client::Item::Events;
use VoiceXML::Client::JavaScriptEngine;
use VoiceXML::Client::Device::Dummy;


=head1 COPYRIGHT AND LICENSE

	

=cut




use strict;

use vars qw{
		$VERSION
};

$VERSION = $VoiceXML::Client::Item::VERSION;




sub init {
	my $self = shift;

	$self->{'event'} = $self->{'XMLElement'}->attribute('event'); 
      if ( $self->{'XMLElement'}->attribute('count') ) {
	   $self->{'count'} = $self->{'XMLElement'}->attribute('count') ;
      }
      else {
	   $self->{'count'}=1;
      }
	$self->{'cond'} =  $self->{'XMLElement'}->attribute('cond') || '1';
      $self->{'type'} = 'VoiceXML::Client::Item::Catch';

	return 1;
	
}

sub doCatch {
	my $self = shift;
	my $handle = shift;
	my $optParams = shift;
     
     my $event = $self->{'event'}; 
     if ($event =~ /error/) { return $VoiceXML::Client::Flow::Directive{'CONTINUE'}; }
     my $sub = "catchev_" . lc($event);

     my $parentform = $self->getParentForm();
     if (defined $parentform) {
            VoiceXML::Client::Item::Events::clearEvent($parentform, $event);
     }
     $parentform = $self->getParentVXMLDocument();
     if (defined $parentform) {
            VoiceXML::Client::Item::Events::clearEvent($parentform, $event);
     }
     if (defined &$sub) { return $self->$sub( $event, $handle, $optParams) ; }
     return $self->executeChildren($handle, $optParams);
}

sub execute {

	my $self = shift;
	my $handle = shift;
	my $optParams = shift;

     return $VoiceXML::Client::Flow::Directive{'CONTINUE'};
}


sub catchev_noinput {
     my $self = shift;
 	my $event = shift;
	my $handle = shift;
	my $optParams = shift; 
 
     if ($self->{'count'} > $optParams->{'maxTimeout'}) {
        VoiceXML::Client::Device::Dummy->disconnect("MAXTIMEOUT");
     }

     return $self->executeChildren($handle, $optParams);


} 

sub catchev_nomatch {
   my $self = shift;
 	my $event = shift;
	my $handle = shift;
	my $optParams = shift; 
 
     if ($self->{'count'} > $optParams->{'maxNomatch'}) {
        VoiceXML::Client::Device::Dummy->disconnect("NOMATCHLIMIT");
     }

     return $self->executeChildren($handle, $optParams);
 } 



sub catchev_help{
     my $self = shift;
 	my $event = shift;
	my $handle = shift;
	my $optParams = shift; 


     my $rv = $self->executeChildren($handle, $optParams);
     if ($rv ne $VoiceXML::Client::Flow::Directive{'CONTINUE'}) {
 
        return $rv;
     }
 	my $parentForm = $self->getParentForm();
	$parentForm->reset();	
	my $vxmlDoc = $self->getParentVXMLDocument();
	$vxmlDoc->nextFormId($parentForm->id());
 
	return $VoiceXML::Client::Flow::Directive{'JUMP'};

} 
   
    
	

sub checkEvent { #check which events are thrown and handle
	my $self = shift;
	my $handle = shift;
	my $optParams = shift;
	
	my $parent = $self->{'parent'};
	
	$self->reset();
		
	# check if event is thrown
     
      
     foreach my $event (split(/\s+/, $self->{'event'})) {

        next if !VoiceXML::Client::Item::Events::isSet($parent, $event);
        next if VoiceXML::Client::Item::Events::countEvent($parent, $event) != $self->{'count'};
        next if !VoiceXML::Client::JavaScriptEngine->JSEvalExpression($self->{'cond'});

        if ($event eq 'nomatch'){
           if ($self->{'count'} >= 2) {

             VoiceXML::Client::Device::Dummy->disconnect("NOMATCHLIMIT");
           }
           else {
             return $event;
           }
        }
        elsif ($event eq 'noinput'){

           if ($self->{'count'} >= 2) {
             VoiceXML::Client::Device::Dummy->disconnect("NOINPUTLIMIT");
           }
           else {
             return $event;
           }
        }
        else {
            VoiceXML::Client::Device::Dummy->disconnect($event); 
        }         


	}
     
      return "noevent";

}

sub checkOneEvent {
    my $self = shift;
    my $document = shift;
    my $event = shift;
	my $handle = shift;
	my $optParams = shift;
	
     if (!VoiceXML::Client::Item::Events::isSet($document, $event)) {
         return $VoiceXML::Client::Flow::Directive{'CONTINUE'} ;
     }
      
      if ($event eq 'nomatch'){
           if ($document->{'count'} >= 2) {
             VoiceXML::Client::Device::Dummy->disconnect("NOMATCHLIMIT");
           }
           else {
             return $VoiceXML::Client::Flow::Directive{'CONTINUE'} ;
           }
      }
      elsif ($event eq 'noinput'){

           if ($document->{'count'} >= 2) {
             VoiceXML::Client::Device::Dummy->disconnect("NOINPUTLIMIT");
           }
           else {
             return $VoiceXML::Client::Flow::Directive{'CONTINUE'} ;
           }
      }
      else {
            VoiceXML::Client::Device::Dummy->disconnect($event); 
      }         

}



1;
