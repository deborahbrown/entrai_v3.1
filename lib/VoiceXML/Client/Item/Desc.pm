package VoiceXML::Client::Item::Desc;

use base qw (VoiceXML::Client::Item);
use VoiceXML::Client::Util;
use Data::Dumper qw(Dumper);


=head1 COPYRIGHT AND LICENSE

	

=cut




use strict;

use vars qw{
		$VERSION
};

$VERSION = $VoiceXML::Client::Item::VERSION;
 


sub execute {
	my $self = shift;
	my $handle = shift;
	my $optParms = shift;
	
	return $VoiceXML::Client::Flow::Directive{'CONTINUE'};
}


1;
