
package VoiceXML::Client::Item::Data;


use base qw (VoiceXML::Client::Item);
use VoiceXML::Client::Util;
use VoiceXML::Client::JavaScriptEngine;
use LWP::UserAgent;
use HTTP::Cookies;
use XML::DOM;
use Data::Dumper;


=head1 COPYRIGHT AND LICENSE


=cut



use strict;

use vars qw{
		$VERSION
};

$VERSION = $VoiceXML::Client::Item::VERSION;


sub init {
	my $self = shift;

    for my $attr ('src', 'name', 'srcexpr', 'method', 'namelist', 'enctype', 'fetchaudio',
                    'fetchhint', 'fetchtimeout', 'maxage', 'maxstale' ) {
          $self->{$attr} = $self->{'XMLElement'}->attribute($attr); 
    }
    if ($self->{'method'} eq '' ) {
        $self->{'method'} = 'GET';
    }
	
	$self->{'type'} = 'VoiceXML::Client::Item::Data';
	$self->{'name'} = 'data';

	return 1;	
	
}


sub execute {
	my $self = shift;
	my $handle = shift;
	my $optParms = shift;
   
     my $uri = $self->{'src'};
     if ($self->{'src'} eq '')
	{
	  $uri = VoiceXML::Client::JavaScriptEngine->JSEvalExpression($self->{'srcexpr'});		
	} 
     my $req = HTTP::Request->new( $self->{'method'}, $uri );
     my $ua = LWP::UserAgent->new;
     my $res;
     if ($self->{'method'} eq 'GET') {
       $res = $ua->get($uri);
     } else {
       $res = $ua->get($uri, '');
     }
     if (! ($res->is_success)) {
          $handle->disconnect("Couldn't retrieve document at uri $uri");
     }
         if ($self->{'name'} ne '' ) {

            #my $expr = sprintf "var %s= new DOMParser().parseFromString('%s')", $self->{'name'}, $res->content;
             my $expr = sprintf "var jsdom = require(\"jsdom\");  var dom = new jsdom();";
            VoiceXML::Client::JavaScriptEngine->JSEvalExpression($expr); 
            die "Data element not implemented";
            my $output = VoiceXML::Client::JavaScriptEngine->JSEvalExpression($self->{'name'});
         }
	
	return $VoiceXML::Client::Flow::Directive{'CONTINUE'};
}


1;
