package VoiceXML::Client::Item::Enumerate;

use base qw (VoiceXML::Client::Item);
use VoiceXML::Client::Util;
use Grammar::GrammarInterpreter;
use VoiceXML::Client::Item::Events;
use VoiceXML::Client::JavaScriptEngine;
use Data::Dumper qw(Dumper);


=head1 COPYRIGHT AND LICENSE

	

=cut




use strict;

use vars qw{
		$VERSION
};

use constant COMMA => ',';

$VERSION = $VoiceXML::Client::Item::VERSION;
 
sub init {
   my $self = shift;
   $self->{'type'} = 'VoiceXML::Client::Item::Enumerate';
   $self->{'name'} = 'enumerate';
   $self->{'template'} = $self->{'XMLElement'}->getValue();
   return 1;
	
}

sub execute {
	my $self = shift;
	my $handle = shift;
	my $optParms = shift;


     my $choices = $self->getParentFieldOrMenu()->{'_enumchoices'};

     my $dtmfval=0;
     for (my $i=0; $i < scalar(@$choices); $i++) {
	    my $choice = $choices->[$i];
         $dtmfval++;
         if ($self->{'template'} eq '' ) {
               $handle->play($choice);
               if ($i < scalar(@$choices) - 1 ) { $handle->play(COMMA); }
               next;
         }
         VoiceXML::Client::JavaScriptEngine->JSEvalExpression("_prompt = \"$choice\"");
         VoiceXML::Client::JavaScriptEngine->JSEvalExpression("_dtmf = \"$dtmfval\"");
         $self->executeChildren($handle, $optParms);
         if ($i < scalar(@$choices) - 1 ) { $handle->play(COMMA); }
         $self->{'currentchild'} = 0;
      }
	
	return $VoiceXML::Client::Flow::Directive{'CONTINUE'};

}




1;


