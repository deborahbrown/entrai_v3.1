package VoiceXML::Client::Item::Menu;

use base qw (VoiceXML::Client::Item);
use VoiceXML::Client::Util;
use Misc::Utilities;
use Data::Dumper;


=head1 COPYRIGHT AND LICENSE


=cut




use strict;

use vars qw{
		$VERSION
};

$VERSION = $VoiceXML::Client::Item::VERSION;




sub init {
	my $self = shift;
	my $params = shift;

	 unless (defined $self->{'name'})
	{
		$self->{'name'} = $self->{'XMLElement'}->attribute('name') || int(rand(999999)) . time();
		
	}
		
         my $vxmlDoc = $self->getParentVXMLDocument();

	   $vxmlDoc->registerForm($self, $self->{'name'}, "1");
      $self->{'id'} = $self->{'XMLElement'}->attribute('id') || int(rand(999999)) . time();
      $self->{'dtmf'} = $self->{'XMLElement'}->attribute('dtmf') || 'false';
      if (defined($self->{'XMLElement'}->attribute('accept')) and $self->{'XMLElement'}->attribute('accept')ne '' ) {
          $self->{'accept'} = $self->{'XMLElement'}->attribute('accept');
      }
      else {
          $self->{'accept'} = 'exact';
      }
      if (defined($self->{'XMLElement'}->attribute('scope')) and $self->{'XMLElement'}->attribute('scope')ne '' ) {
          $self->{'scope'} = $self->{'XMLElement'}->attribute('scope');
      }
      else {
          $self->{'scope'} = 'dialog';
      }
	 $self->{'enumprompt'} = 0;
       $self->{'template'} = '';

	$self->{'noinputflag'} = 0;
	$self->{'nomatchflag'} = 0;
	$self->{'_catch'} = [];
  $self->{'_enumchoices'} = [];
	$self->{'timeswithoutinput'} = 0;
	$self->{'timesnomatchinput'} = 0;
	$self->{'lastinputstate'} = '';
      $self->{'type'} = 'VoiceXML::Client::Item::Menu';

      $self->{'_catch'} = [];
      $self->{'_prompts'} = [];
      $self->{'_repromptFlag'} = 1;
	return 1;
	
}

sub execute {
	my $self = shift;
	my $handle = shift;
	my $optParams = shift;

	#$self->clearValue();
	$handle->registerInputReceivedCallback('fieldinput_' . $self->{'name'}, $self, 'inputReceivedInterrupt');
	
	$handle->inputMode($self->{'inputmode'}, $self->{'numdigits'});

      my $parentcatch = $self->getParentVXMLDocument()->{'_catch'};
	push @{$self->{'_catch'}}, @$parentcatch ;

 	$self->{'_enumchoices'} = [];	
	for (my $i=0; $i < @{$self->{'children'}}; $i++)
	{
		my $aChild = $self->{'children'}->[$i] || next;
		
		my $cType = $aChild->getType();
		
           if ($cType eq 'VoiceXML::Client::Item::Catch')
           { 
                 push @{$self->{'_catch'}}, $aChild;
                 if ($aChild->{'event'} eq 'nomatch') {$self->{'nomatchflag'}=1;}
                 if ($aChild->{'event'} eq 'noinput') {$self->{'noinputflag'}=1; }
           }
           elsif ($cType eq 'VoiceXML::Client::Item::Choice')
           { 
                 push @{$self->{'_choice'}}, $aChild;
                 push @{$self->{'_enumchoices'}}, $aChild->{'choice'};          
           }

	}
     my $rv = $self->executeChildren($handle, $optParams);
     return $rv if ($rv != $VoiceXML::Client::Flow::Directive{'CONTINUE'});
     my $response = $handle->readnum(undef, $self->{'currentinputtimeout'}, 1, $self->{'numdigits'});

    if (!length($response) ){ 
         $self->{'timeswithoutinput'}++; 
         if ($self->{'timeswithoutinput'} > $optParams->{'maxTimeout'} ) {
              $handle->disconnect("MAXTIMEOUT");
              return $VoiceXML::Client::Flow::Directive{'DONE'};
         }
         else {
              VoiceXML::Client::Item::Events::throwEvent($self->getParentForm(), 'noinput');             
         }
     }

       for (my $i=0; $i < @{$self->{'_choice'}}; $i++)
	  {
		  my $theChoice = $self->{'_choice'}->[$i] ;
             my $dtmfval;
             if ($i gt 8) {$dtmfval = ''; } else {$dtmfval = $i+1;}
		  my $recoObj = $theChoice->matchChoice($response, $dtmfval);
             next if $recoObj->{'status'} ne 'MATCH';
             #Got a match - set the application.lastresult value
             my $results = $recoObj->{'results'};
             my $expr = "application.lastresult\$[0] = $results";
            VoiceXML::Client::JavaScriptEngine->JSEvalExpression($expr);
            return $theChoice->takeAction();
      }

       $self->{'timesnomatchinput'}++; 
       if ($self->{'timesnomatchinput'} > $optParams->{'maxNomatch'} ) {
            $handle->disconnect("MAXNOMATCH");
            return $VoiceXML::Client::Flow::Directive{'DONE'};
       }
      else {
              VoiceXML::Client::Item::Events::throwEvent($self->getParentForm(), 'nomatch');             
         }

            for (my $i=0; $i < scalar(@{$self->{'_catch'}}); $i++) {
                    my $catchitem = $self->{'_catch'}->[$i];

                     my $bestcatchitem = $self->getBestCatchItem($handle, $optParams, $catchitem->{'event'}, $self->getParentForm());                                       
                     if (defined $bestcatchitem) {

                        my $rv = VoiceXML::Client::Item::Catch::doCatch($bestcatchitem, $handle, $optParams);
                        if ($rv ne $VoiceXML::Client::Flow::Directive{'CONTINUE'} ) {
                            return $rv;
                        }
                        last;
                     }
=head
                     else {
                       $bestcatchitem = $self->getBestCatchItem($handle, $optParams, $catchitem->{'event'}, $self->getParentVXMLDocument());
                       if (defined $bestcatchitem) {
                          my $rv = VoiceXML::Client::Item::Catch::doCatch($bestcatchitem, $handle, $optParams);
                          if ($rv ne $VoiceXML::Client::Flow::Directive{'CONTINUE'} ) {
                            return $rv;
                          }
                          last;
                       }
=cut
                     
        }

   $self->reset();
	my $vxmlDoc = $self->getParentVXMLDocument();
	$vxmlDoc->nextFormId($self->id());

	return $VoiceXML::Client::Flow::Directive{'JUMP'};
}



		

sub executeChildren {
	my $self = shift;
	my $handle = shift;
	my $optParams = shift;
	
    $self->{'currentchild'} = 0;
printf "Menu.pm got here 4\n";
	while ((my $curChild = $self->getCurrentChild()) && $self->shouldContinueProcessing())
	{

           my $cType = $curChild->getType();

           #if ($cType eq 'VoiceXML::Client::Item::Prompt' or $cType eq 'VoiceXML::Client::Item::Choice') {
           if ($cType ne 'VoiceXML::Client::Item::Catch' ) {

              my $rv = $curChild->execute($handle, $optParams) ;

             return $rv if ($rv != $VoiceXML::Client::Flow::Directive{'CONTINUE'});
          }
		$self->proceedToNextChild();
           
	}
printf "Menu.pm got here 10\n";	
	return $VoiceXML::Client::Flow::Directive{'CONTINUE'};
	
}

sub inputReceivedInterrupt {
	my $self = shift;
	my $handle = shift;
	my $input = shift;
	
	$handle->deleteInputReceivedCallback('fieldinput_' . $self->{'name'});

	if ($self->{'infilledelement'})
	{
	
		$self->abortProcessing(0) ;
	} else {
		
		$self->abortProcessing(1) ;
	}
		
	
	return;
}

sub getBestCatchItem {
	my $self = shift;
	my $handle = shift;
	my $optParams = shift;
        my $event = shift;
        my $document = shift;

 
        my $maxct = 0;
        my $bestmatch = undef;

        for (my $i=0; $i < scalar(@{$self->{'_catch'}}); $i++) {
            my $catchitem = $self->{'_catch'}->[$i];
            next if $catchitem->{'event'} ne $event;
            next if (!defined $document->{'_events'}->{$event});
            next if (! $document->{'_eventspending'}->{$event} );
            next if $catchitem->{'count'} > $document->{'_events'}->{$event};
            if ($catchitem->{'count'} > $maxct) {
                $maxct = $catchitem->{'count'};
                $bestmatch = $catchitem;
            }

       }
       if (defined $bestmatch) {  return $bestmatch;}


        return $bestmatch;

}



sub reset {
	my $self = shift;
	$self->SUPER::reset();
}

sub clearValue {
	my $self = shift;
	
	VoiceXML::Client::Item::Util->resetVariable($self, $self->{'name'});
	
}

sub id {
	my $self = shift;
	my $setTo = shift; # opptional
	
	if (defined $setTo)
	{
		$self->{'id'} = $setTo;
	}
	
	return $self->{'id'};
}

sub isaFieldItem {
	my $self = shift;
	
	return 1;
}
1;
