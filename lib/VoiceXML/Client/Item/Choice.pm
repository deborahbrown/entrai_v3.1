package VoiceXML::Client::Item::Choice;

use base qw (VoiceXML::Client::Item);
use VoiceXML::Client::Util;
use Grammar::GrammarInterpreter;
use Grammar::GrammarServer;
use VoiceXML::Client::Item::Events;
use Misc::Utilities;
use VoiceXML::Client::JavaScriptEngine;
use VoiceXML::Client::Flow;
use Data::Dumper qw(Dumper);


=head1 COPYRIGHT AND LICENSE

	

=cut




use strict;

use vars qw{
		$VERSION
};

$VERSION = $VoiceXML::Client::Item::VERSION;




sub init {
    my $self = shift;
    $self->{'type'} = 'VoiceXML::Client::Item::Choice';
    $self->{'name'} = 'choice';
    foreach my $attr ('next', 'expr', 'event', 'eventexpr', 'message','messageexpr',
                     'dtmf', 'fetchaudio', 'fetchhint', 'fetchtimeout', 'maxage', 'maxstale', 'dtmf', 'accept') {

       if (defined $self->{'XMLElement'}->attribute($attr)) {
         $self->{$attr} = $self->{'XMLElement'}->attribute($attr); 
       }
       else {
         $self->{$attr} = '';
       } 
    }
   
    my $children = $self->{'XMLElement'}->{'_children'};
    foreach my $child (@$children) {
         if (defined $child->{'_contents'} ) {
           $self->{'choice'} = Misc::Utilities->normalizeInput($child->{'_contents'}) ;
         } 
         elsif (defined $child->{'_name'} and $child->{'_name'} eq 'grammar') {
              $self->{'grammar'} = $child;
         }
    }

   
    return 1;
	
}


sub execute {
   my ($self, $handle, $optParams) = @_;
   return $VoiceXML::Client::Flow::Directive{'CONTINUE'};

}

sub matchChoice {
   my ($self, $response, $dtmfval) = @_;

   if (defined $self->{'grammar'} ) {

      my $grammar_attr = $self->{'grammar'}->{'_attributes'};
      if ($grammar_attr->{'srcexpr'} ne '' ) {
         return Grammar::GrammarInterpreter->GrammarFileInterpreter(
                   VoiceXML::Client::JavaScriptEngine->JSEvalExpression($grammar_attr->{'srcexpr'}), $response, 'choice');
      }
      else {
        return Grammar::GrammarInterpreter->GrammarFileInterpreter( $grammar_attr->{'src'}, $response, 'choice');
      }
                   
   }
  elsif (($self->getParentFieldOrMenu()->{'dtmf'} eq 'true') and ($response eq $dtmfval)) { 
      return Grammar::GrammarInterpreter->convertToGrammarResult("inline_grammar", $response);
  }
   
   elsif (defined $self->{'choice'}) {

      if ($self->{'choice'} eq $response) {

          return Grammar::GrammarInterpreter->convertToGrammarResult("inline_grammar", $response);
      }
      else {
          return Grammar::GrammarInterpreter->convertToNomatchResult("inline_grammar", $response, 'choice');
      }
   }
   return Grammar::GrammarInterpreter->convertToNomatchResult("inline_grammar", $response, 'choice');
 }
	
sub takeAction {
   my ($self) = @_;

   #First check if event should be thrown
   if ($self->{'event'} ne '') {
      VoiceXML::Client::Item::Events::throwEvent($self->getParentForm(), $self->{'event'}, $self->{'message'}, $self->{'messageexpr'});
      return $VoiceXML::Client::Flow::Directive{'CONTINUE'};
   }
   elsif ($self->{'eventexpr'} ne '') {
      VoiceXML::Client::Item::Events::throwEvent($self->getParentForm(),
                VoiceXML::Client::JavaScriptEngine->JSEvalExpression( $self->{'eventexpr'}), $self->{'message'}, $self->{'messageexpr'});
      return $VoiceXML::Client::Flow::Directive{'CONTINUE'};
   }

   #No event, so must be goto
   my $next;
    my $vxmlDoc = $self->getParentVXMLDocument();
   if ($self->{'expr'} ne '') {
      $next = VoiceXML::Client::JavaScriptEngine->JSEvalExpression($self->{'expr'});
   }
   else {
      $next = $self->{'next'};
   }
   if ( $next !~ /^#/) { # a uri

       $vxmlDoc->nextDocument($next);
       return $VoiceXML::Client::Flow::Directive{'NEXTDOC'};
   } #end if
   else {
       $next =~ s/^#//;
       $vxmlDoc->nextFormId($next);
       return $VoiceXML::Client::Flow::Directive{'JUMP'};
   } #end else



}








1;
