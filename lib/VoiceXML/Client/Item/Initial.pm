package VoiceXML::Client::Item::Initial;


use strict;


use base qw (VoiceXML::Client::Item::Field);
use VoiceXML::Client::Util;
use VoiceXML::Client::UserAgent;
use VoiceXML::Client::Item::Prompt;
use Net::WebSocket::Server;
use VoiceXML::Client::JavaScriptEngine;
use VoiceXML::Client::Item::Catch;
use Data::Dumper;

=head1 COPYRIGHT AND LICENSE

	
	Copyright (C) 2007,2008 by Pat Deegan.
	All rights reserved
	http://voicexml.psychogenic.com




=cut




use vars qw{
		$VERSION
		$DefaultNumDigits
};

$VERSION = $VoiceXML::Client::Item::VERSION;

$DefaultNumDigits = 1;

sub init {
	my $self = shift;
	
	unless (defined $self->{'name'})
	{
		$self->{'name'} = $self->{'XMLElement'}->attribute('name') || int(rand(999999)) . time();
		
	}
	$self->registerField($self->{'name'});


      $self->{'type'} = 'VoiceXML::Client::Item::Initial';
	

      $self->{'numdigits'} = '';
	 $self->{'inputmode'} = $VoiceXML::Client::DeviceHandle::InputMode{'MULTIDIGIT'};
		

	
	VoiceXML::Client::Item::Util->declareVariable($self, $self->{'name'});
	
	$self->{'noinput'} = [];
	$self->{'nomatch'} = [];
	$self->{'timeswithoutinput'} = 0;
	$self->{'timesnomatchinput'} = 0;
	$self->{'lastinputstate'} = '';
	$self->{'infilledelement'} = 0;
      
	$self->{'currentinputtimeout'} = $VoiceXML::Client::Item::Prompt::DefaultTimeoutSeconds;

      $self->{'_links'} = [];
      $self->{'_catch'} = [];
      return 1;
	
}



sub value {
	my $self = shift;
	my $RecoObj = shift;
  
	
	my $vxmlDoc =  $self->getParentVXMLDocument();
	
      my $expr_TEMPLATE = <<EXPR;
%s = '%s';
%s\$ = %s;
application.lastresult\$[0] = %s;
EXPR


   my $fieldlist = $self->getParentForm()->{'_fields'};
   foreach my $fieldname (@$fieldlist) {
      if (defined $RecoObj->{'interpretation'}->{$fieldname} ) {
         my $expr = sprintf $expr_TEMPLATE, $fieldname, $RecoObj->{'interpretation'}->{$fieldname}, $fieldname, $RecoObj->{'results'}, $RecoObj->{'results'};
         VoiceXML::Client::JavaScriptEngine->JSEvalExpression($expr);
      }
   }
return 1;  
}

sub clearValue {
	my $self = shift;
	
	return 1;
}
		
sub registerField {
     my $self = shift;
     my $name = shift;
     return 1;
}		



1;
