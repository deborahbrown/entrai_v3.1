
package VoiceXML::Client::Item::Transfer;


use base qw (VoiceXML::Client::Item);
use VoiceXML::Client::Util;

=head1 COPYRIGHT AND LICENSE

	
	Copyright (C) 2007,2008 by Pat Deegan.
	All rights reserved
	http://voicexml.psychogenic.com

=cut




use strict;

use vars qw{
		$VERSION
};

$VERSION = $VoiceXML::Client::Item::VERSION;


sub execute {
	my $self = shift;
	my $handle = shift;
	my $optParms = shift;
	
	
	$handle->disconnect("transfer");
	
	return $VoiceXML::Client::Flow::Directive{'DONE'};
}
	

1;
