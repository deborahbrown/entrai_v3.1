package VoiceXML::Client::Item::Sub;


use base qw (VoiceXML::Client::Item);

=head1 COPYRIGHT AND LICENSE

	

=cut



use strict;


use vars qw{
		$VERSION
};

$VERSION = $VoiceXML::Client::Item::VERSION;




sub init {
	my $self = shift;
    my $params = shift;

     if (!defined ($self->{'XMLElement'}->attribute('alias'))) {
       return 0;
     }
     $self->{'alias'} = $self->{'XMLElement'}->attribute('alias');
     $self->{'type'} = 'VoiceXML::Client::Item::Sub';
	return 1;
}


sub createChildren {
   my $self = shift;
   my $params = shift;


   return 0;
}

sub execute {
	my $self = shift;
	my $handle = shift;
	my $optParams = shift;
	
	$handle->play($self->{'alias'}) ;
	
	return $VoiceXML::Client::Flow::Directive{'CONTINUE'};
}

1;
