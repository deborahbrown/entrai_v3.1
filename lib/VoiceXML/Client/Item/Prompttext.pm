
package VoiceXML::Client::Item::Prompttext;


use base qw (VoiceXML::Client::Item);
use VoiceXML::Client::Util;
use VoiceXML::Client::Item::Util;
use Data::Dumper;


=head1 COPYRIGHT AND LICENSE

	

=cut



use strict;


use vars qw{
		$VERSION
};

$VERSION = $VoiceXML::Client::Item::VERSION;




sub init {
	my $self = shift;
      my $params = shift;

     $self->{'prompttext'} = $self->{'XMLElement'}->{'_contents'};
     $self->{'type'} = 'VoiceXML::Client::Item::Prompttext';
     $self->{'name'} = 'prompttext';
	return 1;
}


sub createChildren {
   my $self = shift;
   my $params = shift;


   return 0;
}

sub execute {
	my $self = shift;
	my $handle = shift;
	my $optParams = shift;
	
	$handle->play($self->{'prompttext'}) ;
	
	return $VoiceXML::Client::Flow::Directive{'CONTINUE'};
}

sub new {
	my $class = shift;
	my $xmlElement = shift || return VoiceXML::Client::Util::error("VoiceXML::Client::Item::Factory::new() Must pass an XML element to new.");
	my $parent = shift;
	my $params = shift;
	$|=1;
	
	my $self = {};
        
     bless $self, ref $class || $class;
	
	$self->{'type'} = ref $class || $class;
	$self->{'XMLElement'} = $xmlElement;
	$self->{'parent'} = $parent;
		
	if ($params && ref $params && exists $params->{'context'})
	{
		$self->{'_context'} =  $params->{'context'};
	} else {
		$self->{'_context'} = undef;
	}
	
	$self->{'currentchild'} = 0;
	
	my $retVal = $self->init($params);
     # my $retVal = $self;
	return $retVal unless ($retVal);  # if init didn't go well, we say so here and skip the kids...
	

	
	return $self;
}


1;
