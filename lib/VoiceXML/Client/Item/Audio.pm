
package VoiceXML::Client::Item::Audio;


use base qw (VoiceXML::Client::Item);
use VoiceXML::Client::JavaScriptEngine;
use Grammar::GrammarServer;
use Misc::Utilities;

=head1 COPYRIGHT AND LICENSE

	
	Copyright (C) 2007,2008 by Pat Deegan.
	All rights reserved
	http://voicexml.psychogenic.com

This library is released under the terms of the GNU GPL version 3, making it available only for 
free programs ("free" here being used in the sense of the GPL, see http://www.gnu.org for more details). 
Anyone wishing to use this library within a proprietary or otherwise non-GPLed program MUST contact psychogenic.com to 
acquire a distinct license for their application.  This approach encourages the use of free software 
while allowing for proprietary solutions that support further development.


This file is part of VoiceXML::Client.

 
 
    VoiceXML::Client is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VoiceXML::Client is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VoiceXML::Client.  If not, see <http://www.gnu.org/licenses/>.


=cut



use strict;


use vars qw{
		$VERSION
};

$VERSION = $VoiceXML::Client::Item::VERSION;




sub init {
	my $self = shift;
	

     if ($self->{'XMLElement'}->attribute('src') ) {

         $self->{'src'} = Grammar::GrammarServer->retrievePromptText ( $self->{'XMLElement'}->attribute('src'));
         if (defined $self->{'src'} ) {
           #having some characters in the output might interfere with rendering final text in browser
           #April 14, 1980 - allow { and } to appear in text
           $self->{'src'} =~ s/^[^\w\'\?\.\:\;\{\}\<\>]+//;
           $self->{'src'} =~ s/[^\w\'\?\.\:\;\{\}\<\>]+$//;
         }
     }
     
     $self->{'expr'} =  $self->{'XMLElement'}->attribute('expr');
     $self->{'text'} = $self->{'XMLElement'}->getValue();
	
	return 1;
}


sub execute {
	my $self = shift;
	my $handle = shift;
	my $optParams = shift;
	
	my $fileToPlay;
      if ($self->{'src'})
	{
		$fileToPlay = $self->{'src'};
		
	} elsif ($self->{'text'} !~ /^\s*$/ )

	{
  	   $fileToPlay = $self->{'text'};
     	}

      elsif ($self->{'expr'})
	{
           $fileToPlay = VoiceXML::Client::JavaScriptEngine->JSEvalExpression($self->{'expr'});
	}

      else {
           LogEvent(4, "NOPRMPTSET", "entrai");
     }
	
	
	my $errorFileToPlay;

	if (defined($fileToPlay) and $fileToPlay !~ /^\s*$/)
	{

		if (-e $fileToPlay && -r $fileToPlay)
		{
				$handle->play($fileToPlay);

		} else {
				
	                 $handle->play($fileToPlay); #Added by DWB

		} 
	} else {
           LogEvent(4, "EMPTYPROMPT", 'entrai');

	}


      return $VoiceXML::Client::Flow::Directive{'CONTINUE'};
}

sub getErrorMessageFile {
	my $self = shift;
	my $params = shift;
	
	return $params->{'errormsgfile'} 
		if (exists $params->{'errormsgfile'} && $params->{'errormsgfile'} && -r $params->{'errormsgfile'});

	return undef;
}

sub executeChildren {
	my $self = shift;
	my $handle = shift;
	my $optParms = shift;

	
	while ((my $curChild = $self->getCurrentChild()) && $self->shouldContinueProcessing())
	{
		
		my $rv = $curChild->execute($handle, $optParms) ;
	
		
		return $rv if ($rv != $VoiceXML::Client::Flow::Directive{'CONTINUE'});
		
		$self->proceedToNextChild();
	}

	return $VoiceXML::Client::Flow::Directive{'CONTINUE'};
	
}


1;
