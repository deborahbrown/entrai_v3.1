package VoiceXML::Client::Item::Log;

use base qw (VoiceXML::Client::Item);
use VoiceXML::Client::Util;
use Data::Dumper qw(Dumper);

use VoiceXML::Client::Item::Events;
use VoiceXML::Client::JavaScriptEngine;
use VoiceXML::Client::Device::Dummy;


=head1 COPYRIGHT AND LICENSE

	

=cut




use strict;

use vars qw{
		$VERSION
};

$VERSION = $VoiceXML::Client::Item::VERSION;




sub init {
	my $self = shift;

	$self->{'label'} = $self->{'XMLElement'}->attribute('label'); 
      $self->{'name'} = 'log';
      $self->{'type'} = 'VoiceXML::Client::Item::Log';
      $self->{'value'} = $self->{'XMLElement'}->getValue();

	return 1;
	
}



sub execute {

	my $self = shift;
	my $handle = shift;
	my $optParams = shift;

     return $VoiceXML::Client::Flow::Directive{'CONTINUE'};
}



1;
