package VoiceXML::Client::Item::Events;

use VoiceXML::Client::Util;
use Data::Dumper qw(Dumper);


=head1 COPYRIGHT AND LICENSE

	
	Copyright (C) 2007,2008 by Pat Deegan.
	All rights reserved
	http://voicexml.psychogenic.com


=cut




use strict;

use vars qw{
		$VERSION
};

$VERSION = $VoiceXML::Client::Item::VERSION;


my %currentEvents;

sub throwEvent {
     my ($element, $event, $message, $messageexpr) = @_;


       $element->{'_events'}->{$event} += 1;

       my $eventct = $element->{'_events'}->{$event};

       my $t = sprintf "%s.%d", $event, $eventct;
       $element->{'_events'}->{$t} +=1;
       $element->{'_events'}->{'allevents'} +=1;
       $element->{'_eventspending'}->{$event} = 1;

     return 1;
	
}

sub clearEvent {
     my $element = shift;
     my $event = shift;
    $element->{'_eventspending'}->{$event} = 0;
  
     return 1;
}

sub countEvent {
     my $element = shift;
     my $event = shift;
     if (! defined $element->{'_events'}->{$event} ) { return 0; }
     return $element->{'_events'}->{$event};
}

sub isSet {
     my $element = shift;
     my $event = shift;
     my $count = shift;

     if (defined $element->{$event} ) {
        return $element->{'_eventspending'}->{$event} ;
     }
     return 0;

}
	

1;
