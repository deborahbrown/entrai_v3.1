
package VoiceXML::Client::Item::Object;


use base qw (VoiceXML::Client::Item);
use VoiceXML::Client::Util;

=head1 COPYRIGHT AND LICENSE



=cut




use strict;

use vars qw{
		$VERSION
};

$VERSION = $VoiceXML::Client::Item::VERSION;

sub init {
	my $self = shift;
	
	$self->{'name'} = $self->{'XMLElement'}->attribute('name');
	$self->{'classid'} = $self->{'XMLElement'}->attribute('classid');
	$self->{'data'} = $self->{'XMLElement'}->attribute('data');
     $self->{'type'} = 'VoiceXML::Client::Item::Object';
	return 1;
	
}




sub execute {
	my $self = shift;
	my $handle = shift;
	my $optParms = shift;
	
	
	$handle->disconnect('object:' . $self->{'classid'});
	
	return $VoiceXML::Client::Flow::Directive{'DONE'};
}

sub createChildren {
	my $self = shift;
	my $params = shift;
   
	return 0;
}

	

1;
