package VoiceXML::Client::Item::Factory;

use Data::Dumper;

use VoiceXML::Client::Item::Assign;
use VoiceXML::Client::Item::Audio;
use VoiceXML::Client::Item::Block;
use VoiceXML::Client::Item::Break;
use VoiceXML::Client::Item::Catch;
use VoiceXML::Client::Item::Cdata;
use VoiceXML::Client::Item::Choice;
use VoiceXML::Client::Item::Clear;
use VoiceXML::Client::Item::Data;
use VoiceXML::Client::Item::Desc;
use VoiceXML::Client::Item::Disconnect;
use VoiceXML::Client::Item::Emphasis;
use VoiceXML::Client::Item::Elseif;
use VoiceXML::Client::Item::Else;
use VoiceXML::Client::Item::Enumerate;
use VoiceXML::Client::Item::Exit;
use VoiceXML::Client::Item::Field;
use VoiceXML::Client::Item::Filled;
use VoiceXML::Client::Item::Foreach;
use VoiceXML::Client::Item::Form;
use VoiceXML::Client::Item::Goto;
use VoiceXML::Client::Item::Grammar;
use VoiceXML::Client::Item::If;
use VoiceXML::Client::Item::Initial;
use VoiceXML::Client::Item::Link;
use VoiceXML::Client::Item::Log;
use VoiceXML::Client::Item::Mark;
use VoiceXML::Client::Item::Menu;
use VoiceXML::Client::Item::Meta;
use VoiceXML::Client::Item::Metadata;
#use VoiceXML::Client::Item::NoInput;
#use VoiceXML::Client::Item::NoMatch;
use VoiceXML::Client::Item::Option;
use VoiceXML::Client::Item::Object;
use VoiceXML::Client::Item::P;
use VoiceXML::Client::Item::Param;
use VoiceXML::Client::Item::Phoneme;
use VoiceXML::Client::Item::Prompttext;
use VoiceXML::Client::Item::Prompt;
use VoiceXML::Client::Item::Property;
use VoiceXML::Client::Item::Prosody;
use VoiceXML::Client::Item::Record;
use VoiceXML::Client::Item::Reprompt;
use VoiceXML::Client::Item::Return;
use VoiceXML::Client::Item::S;
use VoiceXML::Client::Item::SayAs;
use VoiceXML::Client::Item::Script;
use VoiceXML::Client::Item::SubDialog;
use VoiceXML::Client::Item::Submit;
use VoiceXML::Client::Item::Sub;
use VoiceXML::Client::Item::Throw;
use VoiceXML::Client::Item::Transfer;
use VoiceXML::Client::Item::Value;
use VoiceXML::Client::Item::Var;
use VoiceXML::Client::Item::Voice;


=head1 COPYRIGHT AND LICENSE

	
	Copyright (C) 2007,2008 by Pat Deegan.
	All rights reserved
	http://voicexml.psychogenic.com

This library is released under the terms of the GNU GPL version 3, making it available only for 
free programs ("free" here being used in the sense of the GPL, see http://www.gnu.org for more details). 
Anyone wishing to use this library within a proprietary or otherwise non-GPLed program MUST contact psychogenic.com to 
acquire a distinct license for their application.  This approach encourages the use of free software 
while allowing for proprietary solutions that support further development.


This file is part of VoiceXML::Client.

 
 
    VoiceXML::Client is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VoiceXML::Client is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VoiceXML::Client.  If not, see <http://www.gnu.org/licenses/>.


=cut




use strict;


use vars qw{
		$VERSION
};

$VERSION = $VoiceXML::Client::Item::VERSION;



=head2 new 

=cut

sub new {
	my $class = shift;
	my $defaultItemParent = shift;
	
	my $self = {};
        
        bless $self, ref $class || $class;
	
	$self->{'defaultItemParent'} = $defaultItemParent;
	
	return $self;
}




=head2 newItem NAME XMLELEMENT

Creates a new instance.


=cut

sub newItem {
	my $class = shift;
	my $name = shift;
	my $xmlElement = shift || return VoiceXML::Client::Util::error("VoiceXML::Client::Item::Factory::newItem() Must pass an XML element to new.");
	my $parent = shift ;
	
	if (! $parent && ref $class)
	{
		$parent = $class->{'defaultItemParent'};
	}
	
	return VoiceXML::Client::Util::error("VoiceXML::Client::Item::Factory::newItem() Must pass a name for form object to new.")
		unless (defined $name);
		

	my $sub = "genitem_" . lc($name);	
	return $class->$sub($xmlElement, $parent) if ($class->can($sub));
	
	return undef;
	
}


sub genitem_assign {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Assign->new($xmlElement, $parent);
}

sub genitem_audio {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Audio->new($xmlElement, $parent);
}

sub genitem_break {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Break->new($xmlElement, $parent);
}

sub genitem_block {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Block->new($xmlElement, $parent);
}

sub genitem_clear {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Clear->new($xmlElement, $parent);
}



sub genitem_cdata {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Cdata->new($xmlElement, $parent);
}


sub genitem_catch {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Catch->new($xmlElement, $parent);
}  


sub genitem_choice {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	return VoiceXML::Client::Item::Choice->new($xmlElement, $parent);
}

sub genitem_data {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	return VoiceXML::Client::Item::Data->new($xmlElement, $parent);
}
  
sub genitem_desc {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Desc->new($xmlElement, $parent);
}

sub genitem_disconnect {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Disconnect->new($xmlElement, $parent);
}

sub genitem_elseif {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Elseif->new($xmlElement, $parent);
}

sub genitem_else {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Else->new($xmlElement, $parent);
}

sub genitem_emphasis {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Emphasis->new($xmlElement, $parent);
}
                                  
sub genitem_enumerate {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Enumerate->new($xmlElement, $parent);
}  

sub genitem_exit {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Exit->new($xmlElement, $parent);
}

sub genitem_field {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Field->new($xmlElement, $parent);
}

sub genitem_filled {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Filled->new($xmlElement, $parent);
}

sub genitem_form {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Form->new($xmlElement, $parent);
}



sub genitem_foreach {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	return VoiceXML::Client::Item::Foreach->new($xmlElement, $parent);
}

sub genitem_goto {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Goto->new($xmlElement, $parent);
}


sub genitem_grammar {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	return VoiceXML::Client::Item::Grammar->new($xmlElement, $parent);
}

sub genitem_if {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::If->new($xmlElement, $parent);
}

sub genitem_initial {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Initial->new($xmlElement, $parent);
}

sub genitem_link {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Link->new($xmlElement, $parent);
}

sub genitem_log {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Log->new($xmlElement, $parent);
} 
sub genitem_mark {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Mark->new($xmlElement, $parent);
}



sub genitem_menu {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Menu->new($xmlElement, $parent);
}

sub genitem_meta {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Meta->new($xmlElement, $parent);
}

sub genitem_metadata {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Metadata->new($xmlElement, $parent);
}

=head
sub genitem_noinput {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::NoInput->new($xmlElement, $parent);
}


sub genitem_nomatch {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::NoMatch->new($xmlElement, $parent);
}
=cut

sub genitem_object {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Object->new($xmlElement, $parent);
} 
sub genitem_option {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Option->new($xmlElement, $parent);
}


sub genitem_p {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::P->new($xmlElement, $parent);
}

sub genitem_phoneme {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Phoneme->new($xmlElement, $parent);
}

sub genitem_prompt {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Prompt->new($xmlElement, $parent);
}

sub genitem_prompttext {
     my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Prompttext->new($xmlElement, $parent);
}

sub genitem_prosody {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Prosody->new($xmlElement, $parent);
}


sub genitem_param {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Param->new($xmlElement, $parent);
}

sub genitem_property {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Property->new($xmlElement, $parent);
}


sub genitem_record {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Record->new($xmlElement, $parent);
}

sub genitem_reprompt {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Reprompt->new($xmlElement, $parent);
}


sub genitem_return {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Return->new($xmlElement, $parent);
}

sub genitem_rule {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	return VoiceXML::Client::Item::Grammar::Rule->new($xmlElement, $parent);
}


sub genitem_s {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::S->new($xmlElement, $parent);
}

sub genitem_sayas {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::SayAs->new($xmlElement, $parent);
}

sub genitem_script {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Script->new($xmlElement, $parent);
} 

sub genitem_sub {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Sub->new($xmlElement, $parent);
}


sub genitem_subdialog {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::SubDialog->new($xmlElement, $parent);
}




sub genitem_submit {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Submit->new($xmlElement, $parent);
}



sub genitem_throw {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Throw->new($xmlElement, $parent);
}   

sub genitem_transfer {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Transfer->new($xmlElement, $parent);
} 



sub genitem_value {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Value->new($xmlElement, $parent);
} 

sub genitem_var {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Var->new($xmlElement, $parent);
}
 

sub genitem_voice {
	my $class = shift;
	my $xmlElement = shift || return;
	my $parent = shift ;
	
	return VoiceXML::Client::Item::Voice->new($xmlElement, $parent);
}  





1;
