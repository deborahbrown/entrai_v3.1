package VoiceXML::Client::Item::Script;

use base qw (VoiceXML::Client::Item);
use VoiceXML::Client::Util;
use Data::Dumper qw(Dumper);


=head1 COPYRIGHT AND LICENSE

	
	Copyright (C) 2007,2008 by Pat Deegan.
	All rights reserved
	http://voicexml.psychogenic.com



=cut




use strict;

use vars qw{
		$VERSION
};

$VERSION = $VoiceXML::Client::Item::VERSION;
use VoiceXML::Client::JavaScriptEngine;




sub init {
     my $self = shift;
   
     $self->{'code'} = $self->{'XMLElement'}->getValue();
     $self->{'type'} = 'VoiceXML::Client::Item::Script';

     return 1;
}

	

sub execute {
	my $self = shift;
	my $handle = shift;
	my $optParms = shift;
    VoiceXML::Client::JavaScriptEngine->JSEvalExpression($self->{'code'});

	return $VoiceXML::Client::Flow::Directive{'CONTINUE'};	
}

sub createChildren {
	my $self = shift;
	my $params = shift;
   
	return 0;
}

1;
