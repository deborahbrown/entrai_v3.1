
package VoiceXML::Client::Item::Foreach;


use base qw (VoiceXML::Client::Item);
use VoiceXML::Client::Util;
use VoiceXML::Client::JavaScriptEngine;
use Data::Dumper;


=head1 COPYRIGHT AND LICENSE

=cut



use strict;


use vars qw{
		$VERSION
		$DefaultTimeoutSeconds
};

$VERSION = $VoiceXML::Client::Item::VERSION;



sub init {
	my $self = shift;

      if (!defined ($self->{'XMLElement'}->attribute('array')) or  !defined ($self->{'XMLElement'}->attribute('item')))
      { 
         return 0;
      }
	$self->{'array'} = $self->{'XMLElement'}->attribute('array') ;
	$self->{'item'} = $self->{'XMLElement'}->attribute('item') ;
      $self->{'type'} = 'VoiceXML::Client::Item::Foreach';
	
	return 1; 
}

sub execute {
	my $self = shift;
	my $handle = shift;
	my $optParms = shift;
	
      my $jsexpr = sprintf "%s.length", $self->{'array'};
      my $arraysz = VoiceXML::Client::JavaScriptEngine->JSEvalExpression( $jsexpr );

      for (my $i=0 ; $i< $arraysz; $i++) {
             $jsexpr = sprintf "%s = %s[%d]", $self->{'item'}, $self->{'array'}, $i;
             VoiceXML::Client::JavaScriptEngine->JSEvalExpression ($jsexpr );
             $self->{'currentchild'} = 0;
             $self->executeChildren($handle, $optParms);
      }
	
	return $VoiceXML::Client::Flow::Directive{'CONTINUE'};
}



1;
