package VoiceXML::Client::Item::Grammar;


use base qw (VoiceXML::Client::Item);
use VoiceXML::Client::Util;
use VoiceXML::Client::Item::Events;
use Grammar::GrammarServer;
use Grammar::GrammarInterpreter;
use JSON::Parse 'parse_json';
use Data::Dumper;
use XML::Parser;



=head1 COPYRIGHT AND LICENSE

	
	Copyright (C) 2007,2008 by Pat Deegan.
	All rights reserved
	http://voicexml.psychogenic.com

This library is released under the terms of the GNU GPL version 3, making it available only for 
free programs ("free" here being used in the sense of the GPL, see http://www.gnu.org for more details). 
Anyone wishing to use this library within a proprietary or otherwise non-GPLed program MUST contact psychogenic.com to 
acquire a distinct license for their application.  This approach encourages the use of free software 
while allowing for proprietary solutions that support further development.


This file is part of VoiceXML::Client.

 
 
    VoiceXML::Client is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VoiceXML::Client is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VoiceXML::Client.  If not, see <http://www.gnu.org/licenses/>.


=cut



use strict;


use vars qw{
		$VERSION
};

$VERSION = $VoiceXML::Client::Item::VERSION;


my @RULES;
my @VARIABLES;

my $GrammarIDNum=1;
sub init {
	my $self = shift;
	
	my $mode = $self->{'XMLElement'}->attribute('mode');
	my $src = $self->{'XMLElement'}->attribute('src');
	my $srcexpr = $self->{'XMLElement'}->attribute('srcexpr');
	my $gmtype = $self->{'XMLElement'}->attribute('type');


	return 0 unless (defined $mode);
	
	$self->{'mode'} = $mode;
	

        $self->{'id'} = $GrammarIDNum++;
	
	$self->{'rules'} = {};

      $self->{'src'} = $src;
      $self->{'srcexpr'} = $srcexpr;
      $self->{'type'} = $gmtype;
      $self->{'inline'} = $self->{'XMLElement'}->toString();

      if ($self->{'inline'} =~ /<\/rule\>/ ) {
        $self->{'inline'} = process_swi($self->{'inline'}, 1);

      }
      else {
          undef $self->{'inline'};
      }


      $self->{'__elemname'} = 'grammar';
	 my $parentForm = $self->getParentForm();

      if (!defined $parentForm) {
         $parentForm = $self->getParentVXMLDocument();
      }
      if (defined $parentForm) {
	  $parentForm->registerGrammar($self->{'id'}, $self);
      }
	return 1;
	
}

sub registerRule {
	my $self = shift;
	my $id = shift;
	my $rule = shift || return;
	
	$self->{'rules'}->{$id} = $rule;
}

sub isValidInput {
    my $self = shift;
    my $handle = shift;
    my $input = shift || '';

    my $ActiveGrammar;

    if (defined $self->{'src'} ) {
      ( $ActiveGrammar = $self->{'src'}) =~ s%.*/%%;
    }
    elsif (defined $self->{'srcexpr'} ) {
       ($ActiveGrammar = VoiceXML::Client::JavaScriptEngine->JSEvalExpression($self->{'srcexpr'})) =~ s%.*/%%;
    }
    elsif (!defined $self->{'inline'} ) {
       VoiceXML::Client::Item::Events::throwEvent($self, 'error.badfetch');
    }


    if (defined $self->{'inline'} and $self->{'inline'} =~ /<rule/m) {
           my $RecoObj = Grammar::GrammarInterpreter->GrammarFileInterpreter($self->{'inline'}, $input, 'inline');
           if ($RecoObj->{'status'} eq 'MATCH') {
             my $RecoResults = $RecoObj->{'results'};
             &AssignToApplicationVariables($RecoResults);
             return($RecoObj);
           }
           else {
             return $RecoObj;
           }
    }

    (my $newinput = $input ) =~ s/\s+/%20/g;
    my $RecoObj;
    my $gramdir = $VoiceXML::Client::UserAgent::UserAgentParams->{'GrammarDir'};
    my $gramfile = "$gramdir/$ActiveGrammar";

    $RecoObj = Grammar::GrammarInterpreter->GrammarFileInterpreter($gramfile, $input, 'grxml');

    my $RecoResults = $RecoObj->{'results'};
   
    if ($RecoObj->{'status'} eq 'MATCH') {
          &AssignToApplicationVariables($RecoResults);
          return($RecoObj);
          
    }
    else  {
          &AssignToApplicationVariables($RecoResults, $ActiveGrammar);
          return($RecoObj);    
    }
    
}
	

sub execute {
	my $self = shift;
	my $handle = shift;
	my $optParams = shift;
	
	


	# skip over kids...
	return $VoiceXML::Client::Flow::Directive{'CONTINUE'};
}

sub AssignToApplicationVariables {
    my ($grammarreturn) = @_;


       if (defined $grammarreturn) {
      my $expr = "application.lastresult\$[0] = $grammarreturn";
      VoiceXML::Client::JavaScriptEngine->JSEvalExpression($expr);
    }
    
    return;  
    
}

sub createChildren {
	my $self = shift;
	my $params = shift;
	
	return 0;
}

sub getRoot {
  my ($grammarstr) = @_;

  my $xmlDoc = new XML::Parser(Style => 'Debug'); 
  my $xmlHash = $xmlDoc->parse($grammarstr);
  my @DUMPED = split(/\n/m, $xmlHash);
  my $firstline = $DUMPED[0];
  my ($root) = ($firstline =~ / root\s+(\w+)/ );
  my ($tag_format) = ($firstline =~ /tag-format\s+([^\s\)]+)/);
  if (!defined $tag_format || $tag_format eq '') {
      $tag_format = 'semantics/1.0';
  }
  
  if ($root ne '') {

     return ($root, $tag_format);
  }
  while (@DUMPED) {
     my $nxtline = shift @DUMPED;
     next if $nxtline !~ /\(id\s+/;
     ($root) = ($nxtline =~ /\(id\s+(\w+)/);
     return ($root, $tag_format);
  }
  die "Couldn't find root rule in $grammarstr";


}

sub getVariables {
  my ($grammarstr) = @_;
  my $xmlDoc = new XML::Parser(Style => 'Debug'); 
  my $xmlHash = $xmlDoc->parse($grammarstr);
  my @DUMPED = split(/\n/m, $xmlHash);
  my @assignments;
  while (@DUMPED) {
     my $nxtline = shift @DUMPED;
     next if ! ($nxtline =~ /tag \|\|/);
     push(@assignments, $nxtline =~ /(\b[A-Za-z_]\w*)\s*=\s*['"]?\w*['"]?/g );
  }
  return @assignments;
}

sub getRuleNames {
  my ($grammarstr) = @_;

  my $xmlDoc = new XML::Parser(Style => 'Debug'); 
  my $xmlHash = $xmlDoc->parse($grammarstr);
  my @DUMPED = split(/\n/m, $xmlHash);

  while (@DUMPED) {
     my $nxtline = shift @DUMPED;
     next if ! ($nxtline =~ /\\\\\s*\(id\s/ );
     push(@RULES, $nxtline =~ /\(id (\w+)/g );
  }
  return @RULES;
}






##########################

sub process_swi {
  my($grammar,  $toplevel) = @_;

 my ($rootrule, $tag_format) = getRoot($grammar);
  @RULES=();
  @VARIABLES=();

  #Get list of assignments for variable names
  push( @VARIABLES, getVariables($grammar));

  my %Variables;
  foreach my $variable (@VARIABLES) {
     $Variables{$variable}++;
  }

  my $initial_declaration = "  <item>\n    <tag>";
  my $endruleassign = "    <tag>\n    ";
  
  $initial_declaration = sprintf " <item>\n    <tag>var %s;\n      ", join(',', keys(%Variables));
  foreach my $variable (keys %Variables) {
     next if $variable eq "SWI_disallow"; #will declare these in grammar interpretation routine
     next if $variable eq "SWI_decoy";
      next if $variable eq "SWI_scoreDelta";
     $endruleassign = sprintf "%s out.%s=%s;", $endruleassign, $variable, $variable;
  }

  #Get list of rules
  my @Rulenames = getRuleNames($grammar);

  foreach my $rulename (@Rulenames) {
     $initial_declaration = sprintf "%s var %s = {};", $initial_declaration, $rulename;

     if ($rulename ne $rootrule) {
        $grammar =~ s#(<rule\s[^>]*\bid\s*=\s*['"]$rulename['"][^>]*>)#$1\n  <item>\n#;

     }
     elsif (! $toplevel) {

        $grammar =~ s#(<rule\s[^>]*\bid\s*=\s*['"]$rulename['"][^>]*>)#$1\n  <item>\n#;
     }
  }


  $initial_declaration = sprintf "%s</tag>\n", $initial_declaration;
  $endruleassign = sprintf "%s\n    </tag>\n</item>\n</rule>\n", $endruleassign;

  $grammar =~ s#</rule>#$endruleassign#g;
  if ($toplevel) {
   $grammar =~ s#<rule\s[^>]*id\s*=\s*['"]$rootrule['"][^>]*>#<rule id='$rootrule' scope='public'>\n$initial_declaration\n#;
  }

  $grammar =~ s#([^\;\s])\s*</tag>#$1 \; </tag>#gm;
  
  return $grammar;
}

	

1;
