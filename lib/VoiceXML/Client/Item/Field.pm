package VoiceXML::Client::Item::Field;


use strict;


use base qw (VoiceXML::Client::Item);
use VoiceXML::Client::Util;
use VoiceXML::Client::UserAgent;
use VoiceXML::Client::Item::Prompt;
use Net::WebSocket::Server;
use VoiceXML::Client::JavaScriptEngine;
use VoiceXML::Client::Item::Catch;
use VoiceXML::Client::Item::Util;
use VoiceXML::Client::Flow;
use VoiceXML::Client::Item::Link;
use Misc::Utilities;
use Data::Dumper;

=head1 COPYRIGHT AND LICENSE

	
	Copyright (C) 2007,2008 by Pat Deegan.
	All rights reserved
	http://voicexml.psychogenic.com

This library is released under the terms of the GNU GPL version 3, making it available only for 
free programs ("free" here being used in the sense of the GPL, see http://www.gnu.org for more details). 
Anyone wishing to use this library within a proprietary or otherwise non-GPLed program MUST contact psychogenic.com to 
acquire a distinct license for their application.  This approach encourages the use of free software 
while allowing for proprietary solutions that support further development.


This file is part of VoiceXML::Client.

 
 
    VoiceXML::Client is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VoiceXML::Client is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VoiceXML::Client.  If not, see <http://www.gnu.org/licenses/>.


=cut




use vars qw{
		$VERSION
		$DefaultNumDigits
};

$VERSION = $VoiceXML::Client::Item::VERSION;

$DefaultNumDigits = 1;

my %InputCodes = (
   'MATCH' , 1,
   'NOINPUT' , 2,
   'NOMATCH' , 3,
   'LINK' , 4
);
     


sub init {
	my $self = shift;
	
	unless (defined $self->{'name'})
	{
		$self->{'name'} = $self->{'XMLElement'}->attribute('name') || int(rand(999999)) . time();
		
	}
	$self->registerField($self->{'name'});

        $self->{'type'} = 'VoiceXML::Client::Item::Field';
	
	  $self->{'numdigits'} = '';
        $self->{'inputmode'} = $VoiceXML::Client::DeviceHandle::InputMode{'MULTIDIGIT'};
	  $self->{'builtin'} = $self->{'XMLElement'}->attribute('type');	
	
	VoiceXML::Client::Item::Util->declareVariable($self, $self->{'name'});
        $self->{'expr'} = (defined $self->{'_attributes'}->{'expr'} ) ? $self->{'_attributes'}->{'expr'} : undef;
     
	$self->{'noinputflag'} = 0;
	$self->{'nomatchflag'} = 0;
	$self->{'timeswithoutinput'} = 0;
	$self->{'timesnomatchinput'} = 0;
	$self->{'lastinputstate'} = '';
	$self->{'infilledelement'} = 0;
      
	
	$self->{'currentinputtimeout'} = $VoiceXML::Client::Item::Prompt::DefaultTimeoutSeconds;

        $self->{'_links'} = [];
        $self->{'_catch'} = [];
        $self->{'_enumchoices'} = [];
        $self->{'_option'} = [];
        $self->{'_repromptFlag'} = 1;
        return 1;
	
}

sub new {
	my $class = shift;
	my $xmlElement = shift || return VoiceXML::Client::Util::error("VoiceXML::Client::Item::Factory::new() Must pass an XML element to new.");
	my $parent = shift;
	my $params = shift;
	
	
	my $self = {};
        
        bless $self, ref $class || $class;
	
	$self->{'type'} = ref $class || $class;
	$self->{'XMLElement'} = $xmlElement;
	$self->{'parent'} = $parent;
	
	
	if ($params && ref $params && exists $params->{'context'})
	{
		$self->{'_context'} =  $params->{'context'};
	} else {
		$self->{'_context'} = undef;
	}
	
	$self->{'currentchild'} = 0;
	
	my $retVal = $self->init($params);
	return $retVal unless ($retVal);  # if init didn't go well, we say so here and skip the kids...
	
	# create children lastly
	$self->createChildren();
	
	return $self;
}



sub registerField {
     my $self = shift;
     my $name = shift;

     my $parent = $self->getParentForm();
     push(@{$parent->{'_fieldlist'}}, $name);    
     return 1;
}

sub inputReceivedInterrupt {
	my $self = shift;
	my $handle = shift;
	my $input = shift;
	
	$handle->deleteInputReceivedCallback('fieldinput_' . $self->{'name'});

	if ($self->{'infilledelement'})
	{
	
		$self->abortProcessing(0) ;
	} else {
		
		$self->abortProcessing(1) ;
	}
		
	
	return;
}
	
sub execute {
	my $self = shift;
	my $handle = shift;
	my $optParams = shift;
	
	$self->clearValue();
	   $self->clearValue();
	$handle->registerInputReceivedCallback('fieldinput_' . $self->{'name'}, $self, 'inputReceivedInterrupt');

	
	$handle->inputMode($self->{'inputmode'}, $self->{'numdigits'});
	
        my $parentlinks = $self->getParentForm()->{'_links'};
	push @{$self->{'_links'}}, @$parentlinks;

        $parentlinks = $self->getParentVXMLDocument()->{'_links'};
	push @{$self->{'_links'}}, @$parentlinks ;


        my $parentcatch = $self->getParentForm()->{'_catch'};
	push @{$self->{'_catch'}}, @$parentcatch ;

       $parentcatch = $self->getParentVXMLDocument()->{'_catch'};
	push @{$self->{'_catch'}}, @$parentcatch ;


      $self->{'_enumchoices'} = [];
      $self->{'_option'} = [];
	for (my $i=0; $i < @{$self->{'children'}}; $i++)
	{
		my $aChild = $self->{'children'}->[$i] || next;
		
		my $cType = $aChild->getType();

           if ($cType eq 'VoiceXML::Client::Item::Catch')
           { 
                 push @{$self->{'_catch'}}, $aChild;
                 if ($aChild->{'event'} eq 'nomatch') {$self->{'nomatchflag'}=1;}
                 if ($aChild->{'event'} eq 'noinput') {$self->{'noinputflag'}=1; }
           }
           elsif ($cType eq 'VoiceXML::Client::Item::Link')
		{
			push @{$self->{'_links'}}, $aChild;
		}
           elsif ($cType eq 'VoiceXML::Client::Item::Option')
           { 

                 push @{$self->{'_option'}}, $aChild;
                 push @{$self->{'_enumchoices'}}, $aChild->{'text'};          
           }

	}

	
	while (my $curChild = $self->getCurrentChild())
	{
		
		my $cType = $curChild->getType();
              
			
		$self->abortProcessing(0); 

        if (($cType eq 'VoiceXML::Client::Item::Prompt') or ($cType eq 'VoiceXML::Client::Item::Prompttext')) {

               if ( $self->{'_repromptFlag'}) {           
			$curChild->execute($handle, $optParams) ;
                $self->{'_repromptFlag'} = 0;
	        }			 
                my ($userInput, $nextaction) = $self->getUserInput($handle, $optParams);
  
	        if (!defined $userInput ){
   
                #$self->{'timeswithoutinput'}++; 
                    if ($self->{'timeswithoutinput'} > $optParams->{'maxTimeout'} ) {
                         $handle->disconnect("MAXTIMEOUT");
                         return $VoiceXML::Client::Flow::Directive{'DONE'};
                    }
                    else {

                      VoiceXML::Client::Item::Events::throwEvent($self->getParentForm(), 'noinput');
                     
                    }
		}
		elsif ( $userInput == $InputCodes{'LINK'} and $nextaction != $VoiceXML::Client::Flow::Directive{'CONTINUE'} ) 

	        {

     			return $nextaction;
	        }  
	        elsif ($userInput == $InputCodes{'NOMATCH'} ){

                     
                    if ($self->{'timesnomatchinput'} > $optParams->{'maxNomatch'} ) {
                         $self->{'timesnomatchinput'}++;
                         $handle->disconnect("MAXNOMATCH");
                         return $VoiceXML::Client::Flow::Directive{'DONE'};
                    }
                    else {
                      VoiceXML::Client::Item::Events::throwEvent($self->getParentForm(), 'nomatch', undef, undef);
                      
                    }
		}

	        $self->proceedToNextChild();
                next;

      }



      elsif ($cType eq 'VoiceXML::Client::Item::Catch' ) {
                 $self->proceedToNextChild();
	         next;
      }


      elsif ($cType eq 'VoiceXML::Client::Item::Filled') { #check if parent form or document items to catch

                 for (my $i=0; $i < scalar(@{$self->{'_catch'}}); $i++) {
                    my $catchitem = $self->{'_catch'}->[$i];

                     my $bestcatchitem = $self->getBestCatchItem($handle, $optParams, $catchitem->{'event'}, $self->getParentForm());
                     
                     

                     if (defined $bestcatchitem) {

                        my $rv = VoiceXML::Client::Item::Catch::doCatch($bestcatchitem, $handle, $optParams);
                        if ($rv ne $VoiceXML::Client::Flow::Directive{'CONTINUE'} ) {
                            return $rv;
                        }
                        last;
                     }
                     else {
                       $bestcatchitem = $self->getBestCatchItem($handle, $optParams, $catchitem->{'event'}, $self->getParentVXMLDocument());
                       if (defined $bestcatchitem) {
                          my $rv = VoiceXML::Client::Item::Catch::doCatch($bestcatchitem, $handle, $optParams);
                          if ($rv ne $VoiceXML::Client::Flow::Directive{'CONTINUE'} ) {
                            return $rv;
                          }
                          last;
                       }
                     }
                 }

                 if ( !(VoiceXML::Client::Item::Util->isDefinedVar( $self->{"name"}))) {

                   $self->proceedToNextChild();
	           next;
                 }



      }

      elsif (defined($curChild->{'__elemname'}) and $curChild->{'__elemname'} eq 'grammar') {
                 $self->proceedToNextChild();
		 next;
      }




	 my $rv = $curChild->execute($handle, $optParams) ;

	 return $rv if ($rv != $VoiceXML::Client::Flow::Directive{'CONTINUE'});
	
	 $self->proceedToNextChild();
	}


	

        my $parentForm = $self->getParentForm();
        $parentForm->reset();
	my $vxmlDoc = $self->getParentVXMLDocument();
	$vxmlDoc->nextFormId($parentForm->id());

	return $VoiceXML::Client::Flow::Directive{'JUMP'};
	
}

sub reset {
	my $self = shift;
	my $vxmlDoc =  $self->getParentVXMLDocument();
	$vxmlDoc->clearGlobalVar($self->{'name'});
	$self->SUPER::reset();
}

sub getBestCatchItem {
	my $self = shift;
	my $handle = shift;
	my $optParams = shift;
        my $event = shift;
        my $document = shift;

 
        my $maxct = 0;
        my $bestmatch = undef;

        for (my $i=0; $i < scalar(@{$self->{'_catch'}}); $i++) {
            my $catchitem = $self->{'_catch'}->[$i];
            next if $catchitem->{'event'} ne $event;
            next if (!defined $document->{'_events'}->{$event});
            next if (! $document->{'_eventspending'}->{$event} );
            next if $catchitem->{'count'} > $document->{'_events'}->{$event};
            if ($catchitem->{'count'} > $maxct) {
                $maxct = $catchitem->{'count'};
                $bestmatch = $catchitem;
            }

       }
       if (defined $bestmatch) {  return $bestmatch;}


        return $bestmatch;

}





sub getUserInput {
	my $self = shift;
	my $handle = shift;
	my $optParams = shift;


	my $response = $handle->readnum(undef, $self->{'currentinputtimeout'}, 1, $self->{'numdigits'});
	
	if (defined $response && length($response))
	{
		# got some input... 
	      $self->{'timeswithoutinput'} = 0;

            my $doLinks = VoiceXML::Client::Item::Link->handleLinks($self->{'_links'}, $response, $self->getParentForm());

            if (defined $doLinks ) {
                  return($InputCodes{'LINK'},$doLinks );
            }


           foreach my $option (@{$self->{'_option'}} )  {

              my $optreturn = $option->checkOption( $response, $self->{'name'});#checks option; if valid sets value
              if (defined $optreturn) { 

                   $self->value($optreturn, $response);
                   return ($InputCodes{'MATCH'},$VoiceXML::Client::Flow::Directive{'CONTINUE'} ) ;
              }

           } 

           
	      my $parentForm = $self->getParentForm();

	      my $grammarreturn = $parentForm->inputValidAccordingToGrammar($handle,$response, $self->{'name'}, $self->{'builtin'});

              my $status = $grammarreturn->{'status'};

		if ($status eq 'MATCH')
		{

                 $self->value($grammarreturn, $response);
		      return ($InputCodes{'MATCH'},$VoiceXML::Client::Flow::Directive{'CONTINUE'} ) ;
		}
		elsif ($self->{'nomatchflag'})
		{
			# the input came in, but was invalid and we have nomatch set...
		
			$self->{'lastinputstate'} = 'invalid';

			return ($InputCodes{'NOMATCH'}, $VoiceXML::Client::Flow::Directive{'CONTINUE'});
			
		} else {
		
			# no nomatches set... so no checking, just set the value 
			# whatever it may be...
			
			$self->value($grammarreturn, $response);
		
			
                return($InputCodes{'MATCH'},$VoiceXML::Client::Flow::Directive{'CONTINUE'} );
            }
      }
	
     #No input received
      $self->{'lastinputstate'} = 'none';
      $self->{'timeswithoutinput'}++;
      return (undef, undef);
}



sub value {
	my $self = shift;
	my $RecoObj = shift;
        my $response = shift;
  
	
	my $vxmlDoc =  $self->getParentVXMLDocument();
	
      my $expr_TEMPLATE = <<EXPR;
%s = '%s';
%s\$ = %s;
application.lastresult\$[0] = %s;
EXPR



   my $fieldtopopulate = $self->{'name'};
   my $expr;
   if (!defined $RecoObj->{'interpretation'}->{$fieldtopopulate}) {
     $expr = sprintf $expr_TEMPLATE, $self->{'name'}, $response, $self->{'name'}, $RecoObj->{'results'}, $RecoObj->{'results'};
   }
   else {
     $expr = sprintf $expr_TEMPLATE, $self->{'name'}, $RecoObj->{'interpretation'}->{$fieldtopopulate}, $self->{'name'}, $RecoObj->{'results'}, $RecoObj->{'results'};
   }


   VoiceXML::Client::JavaScriptEngine->JSEvalExpression($expr);
}

sub clearValue {
	my $self = shift;
	
	VoiceXML::Client::Item::Util->resetVariable($self, $self->{'name'});

	
}

sub initValue {
       my $self = shift;
       my $value = shift;

       my $expr = sprintf "var %s = '%s'", $self->{'name'}, $value;
       VoiceXML::Client::JavaScriptEngine->JSEvalExpression($expr);
}


sub isaFieldItem {
	my $self = shift;
	
	return 1;
}	


1;
