package VoiceXML::Client::Document;



use strict;
use VoiceXML::Client::JavaScriptEngine;
use Data::Dumper;
use base qw(VoiceXML::Client::Item);
use Misc::Utilities;
use VoiceXML::Client::UserAgent;


use vars qw{
		$VERSION
};

$VERSION = $VoiceXML::Client::Item::VERSION;


=head1 COPYRIGHT AND LICENSE

	
	Copyright (C) 2007,2008 by Pat Deegan.
	All rights reserved
	http://voicexml.psychogenic.com

This library is released under the terms of the GNU GPL version 3, making it available only for 
free programs ("free" here being used in the sense of the GPL, see http://www.gnu.org for more details). 
Anyone wishing to use this library within a proprietary or otherwise non-GPLed program MUST contact psychogenic.com to 
acquire a distinct license for their application.  This approach encourages the use of free software 
while allowing for proprietary solutions that support further development.


This file is part of VoiceXML::Client.

 
 
    VoiceXML::Client is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VoiceXML::Client is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VoiceXML::Client.  If not, see <http://www.gnu.org/licenses/>.


=cut



sub new {
	my ($class, $docname, $runtime) = @_;
	
	
	my $self = {};
        
        bless $self, ref $class || $class;
	
	
	$self->{'type'} = ref $class || $class;
	$self->{'_runtime'} = $runtime;
	$self->{'_context'} = $runtime->create_context();
	$self->{'docname'} = $docname;
	
	$self->{'forms'} = [];
	$self->{'formindices'} = {};
	
	$self->{'variables'} = {};
	
	$self->{'nextform'} = 0;
	$self->{'nextdocument'} = 0;
	$self->{'currentitem'} = 0;
      $self->{'_links'} = [];
      $self->{'_catch'} = [];	
	
	return $self;
}

sub init {
	my ($self) = @_;
	
	$self->{'items'} = [];

	
	return 1;
}


=head2 addItem ITEM

Appends ITEM to the list of items for the document.  Items may be any VXML::Item::XXX subclass, while
ITEM itself may be a single ITEM object reference or an ARRAY ref of VXML::Item::XXX objects (each will
be appended in order).

Returns the number of appended items.

=cut

sub addItem {

      my ($self, $item) = @_;	
	
	my $count = 0;
	if (ref $item eq 'ARRAY')
	{
		foreach my $singleItem (@{$item})
		{
			$count += $self->addItem($singleItem);
		}
	} else {
		push @{$self->{'items'}}, $item;
		$count = 1;
	}
	
	return $count;
	
}

sub nextFormId {
     my ($self, $setTo) = @_;
	
	if (defined $setTo && exists $self->{'formindices'}->{$setTo})
	{
		$self->{'nextform'} = $setTo;
		my $nextFormIdx = $self->getFormItemsIndex($setTo);
		
		$self->{'currentitem'} = $nextFormIdx;
		$self->{'items'}->[$nextFormIdx]->reset();
			return $setTo;
		
	}
	
	my $idx = 0;
	if ($self->{'nextform'})
	{
		my $nextFormName = $self->{'nextform'};
		
		if (exists $self->{'formindices'}->{$nextFormName}
			&& ( exists $self->{'forms'}->[$self->{'formindices'}->{$nextFormName}] )
		)
		{
			$idx = $self->{'formindices'}->{$nextFormName};
			$self->{'variables'}->{$self->{'forms'}->[$idx]->{'guard'}} = undef;
		}
	}
	
	for (my $i = $idx; $i < scalar @{$self->{'forms'}}; $i++)
	{
	
		if (!($self->{'variables'}->{$self->{'forms'}->[$i]->{'guard'}}))
		{
			return $self->{'forms'}->[$i]->{'name'} ;
		}
	}
	
	return undef;
}

sub getForm {
      my ($self, $id) = @_;
	
	my $idx = $self->getFormItemsIndex($id);
	
	
	return undef unless (defined $idx);

	return $self->{'items'}->[$idx];
	
}

sub getFormItemsIndex {
      my ($self, $id) = @_;
	
	return undef unless defined ($id);

	for (my $i=0; $i < scalar @{$self->{'items'}}; $i++)
	{
		my $itm = $self->{'items'}->[$i];
		
		if ($itm->{'type'} eq 'VoiceXML::Client::Item::Form'
				&& $itm->{'id'} eq $id)
		{
			
			return $i ;
		}
				
	}

	return undef;
}
	


sub getNextForm {
	my ($self) = @_;
	
	my $id = $self->nextFormId();
	
	
	return $self->getForm($id);
	
}

sub getNextItem {
	my ($self) = @_;

	return $self->getNextForm() if ($self->{'nextform'});


	$self->{'currentitem'} ||= 0;
	
	my $oldChildSetting = $self->{'currentitem'} ;
	my $i = $oldChildSetting;

	while ($i < scalar @{$self->{'items'}} )
	{
	
		$self->{'currentitem'} = $i;
	     
		  my $cType = $self->{'items'}->[$i]->getType();

             if ($cType eq 'VoiceXML::Client::Item::Link')
		  {
			push @{$self->{'_links'}}, $self->{'items'}->[$i];
		  }

             elsif ($cType eq 'VoiceXML::Client::Item::Catch')
		  {
			push @{$self->{'_catch'}}, $self->{'items'}->[$i];
		  }
		  if ($cType ne 'VoiceXML::Client::Item::Form')
		  {
			# not a form, just return it in sequence

			return $self->{'items'}->[$i];

		
		  } else 
		  {
			# it IS a form... handle guard vars...
			my $formName = $self->{'items'}->[$i]->id();
			
			if (exists $self->{'formindices'}->{$formName})
			{
				my $formIdx = $self->{'formindices'}->{$formName};
				
				if (! $self->{'variables'}->{$self->{'forms'}->[$formIdx]->{'guard'}})
				{
					# not visited yet... go for it.
					return $self->{'items'}->[$i];
				}
			}
		  }
          
		
		$i++;
		
	}
	
	$self->{'currentitem'} = $oldChildSetting;
	
	return undef;
}

sub currentItemPosition {
     my ($self, $setTo) = @_;
	
	if (defined $setTo && $setTo =~ m/^\d+$/ && $setTo < scalar @{$self->{'items'}})
	{
		$self->{'currentitem'} = $setTo;
	}
	
	return $self->{'currentitem'};
}

sub proceedToNextItem {
	my ($self) = @_;
	
	return $self->{'currentitem'}++;
	
}			


sub registerForm {

     my($self, $formName, $guard) = @_;
	
	push @{$self->{'forms'}}, {
				'name'	=> $formName,
				'guard'	=> $guard};
				
	$self->{'formindices'}->{$formName} = $#{$self->{'forms'}};
	
				
	$self->registerVariable($formName);
}

sub registerGrammar {

      my ($self, $id, $grammar) = @_;
      if (!$grammar) { return;}

	$self->{'grammars'}->{$id} = $grammar;
}

sub registerVariable {
      my ($self, $varName, $val) = @_;
      if (!$varName) { return;}	
	if (defined $val)
	{
		$self->{'variables'}->{$varName} = $val;
	} else {
		
		$self->{'variables'}->{$varName}  = undef;
	}
	
}

sub globalVar {
      my ($self, $varName, $val) = @_;
      if (!$varName) { return;}
	
	
	if (defined $val)
	{
		$self->{'variables'}->{$varName} = $val;

	}
	
	$self->{'variables'}->{$varName} = undef unless (exists $self->{'variables'}->{$varName});
		
	
	return $self->{'variables'}->{$varName};
}

sub clearGlobalVar {
      my ($self, $varName) = @_;
      if (!$varName) { return;}
	
	if (exists $self->{'variables'}->{$varName})
	{
		$self->{'variables'}->{$varName} = undef;
	}
      my $jsexpr = sprintf '%s = undefined', $varName;
      VoiceXML::Client::JavaScriptEngine->JSEvalExpression ($jsexpr );
}
	
sub pushPositionStack {
	my $self  = shift;

	unshift @{$self->{'itempositionstack'}}, $self->{'currentitem'} ;
	
	
}

sub latestPositionInStack {
	my ($self)  = @_;
	
	
	if (scalar @{$self->{'itempositionstack'}})
	{
		return $self->{'itempositionstack'}->[0];
	}
	
	return undef;
	
}

sub popPositionStack {
	my ($self)  = @_;
	
	
	if (scalar @{$self->{'itempositionstack'}})
	{
		return shift @{$self->{'itempositionstack'}};
	}

	return undef;
}

sub execute {
      my ($self, $handle, $params, $itemToExec) = @_;

      if (!$handle) {return undef;}
      if (!$itemToExec) { $itemToExec = $self->getNextItem(); }
	
	unless ($itemToExec)
	{
		warn "No form or other item to execute";
		return undef;
	}
	
	my $retVal = $VoiceXML::Client::Flow::Directive{'CONTINUE'};
	
	do {
		
		$self->{'nextform'} = undef; # make certain we start with a clean slate
		
		$retVal = $itemToExec->execute($handle, $params);
	
		$self->proceedToNextItem() if ($retVal != $VoiceXML::Client::Flow::Directive{'JUMP'});


		$itemToExec = $self->getNextItem();
		
		if ($retVal == $VoiceXML::Client::Flow::Directive{'SUBRETURN'})
		{
			my $latestPosInStack = $self->latestPositionInStack();
			if (defined $latestPosInStack)
			{
				$self->popPositionStack();
				$self->{'currentitem'} = $latestPosInStack;
				$itemToExec = $self->{'items'}->[$latestPosInStack];
				$retVal = $VoiceXML::Client::Flow::Directive{'CONTINUE'};
			}
		}
		
				
		
	} while ($itemToExec &&
			($retVal == $VoiceXML::Client::Flow::Directive{'CONTINUE'}
				||
			$retVal == $VoiceXML::Client::Flow::Directive{'JUMP'}));
	
	
	if ($retVal == $VoiceXML::Client::Flow::Directive{'CONTINUE'})
	{

           $handle->disconnect("CVP_hangup");


	}

	return $retVal if ($retVal == $VoiceXML::Client::Flow::Directive{'ABORT'}
				|| $retVal == $VoiceXML::Client::Flow::Directive{'DONE'});
	
	if ($retVal == $VoiceXML::Client::Flow::Directive{'NEXTDOC'})
	{
		# need to jump somewhere... 
		my $nextDoc = $self->nextDocument();
		warn "Told to go to next doc, but nextDocument not set" unless ($nextDoc);

		
	}
	
	return $retVal;
	
}
		
sub nextDocument {
      my ($self, $setTo) = @_;
	
	if (defined $setTo)
	{
		$self->{'nextdocument'} = $setTo;
	}
	
	return 	$self->{'nextdocument'};
}

sub setNextItem {
    my ($self, $nextItemNum) = @_;

    $self->{'currentitem'} = $nextItemNum;
    return $nextItemNum;
}
	

sub isaVXMLDocument {
	my ($self) = @_;
	
	return 1;
}

1;
