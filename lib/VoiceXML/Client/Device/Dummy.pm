package VoiceXML::Client::Device::Dummy;
use URI::Escape;
use HTTP::Request;
use VoiceXML::Client::UserAgent;
use Misc::Utilities;
use Misc::Format;
use HTTP::Cookies;
use VoiceXML::Client::Util;
use LWP::UserAgent;
use JSON::Create 'create_json';
use IO::Socket::SSL;
use Data::Dumper;

use strict;
use warnings;

use base qw(VoiceXML::Client::DeviceHandle);

our $UserInput;
our $lastUserInput;
my $PromptBuffer;
our $QueuedPrompt='';
our $BufferPrompt = 0;


=head1 COPYRIGHT AND LICENSE


    VoiceXML::Client::DeviceHandle module, device handle api based on the VOCP voice messaging system package
    VOCP::Device.
    
    Copyright (C) 2002,2008 Patrick Deegan
    All rights reserved


This file is part of VoiceXML::Client.
 
    VoiceXML::Client is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VoiceXML::Client is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VoiceXML::Client.  If not, see <http://www.gnu.org/licenses/>.




=cut


sub connect {
	my $self = shift;
	my $session = shift;


     $PromptBuffer='';
     return 1;
}

sub hangup {
	my $self = shift;
	my $session = shift;
      #Tell IVR to hangup

      my $cookie = $session->retrieveSessionVal('cookie');
      my $params = $session->retrieveSessionVal('ConfigParams');
      my %cookieParams = ();
      $cookieParams{'file'} = $params->{'cookiefile'};
      my $cookiejar = HTTP::Cookies->new(%cookieParams);

      my $ua = LWP::UserAgent->new('cookie_jar' => $cookiejar);
      my $url = $params->{'hangup_url'};
      my $req = HTTP::Request->new(GET => $url);
      $req->header('cookie' => $cookie);
      my $res = $ua->request($req);

}


sub disconnect {
    my $self = shift;
    my $reason = shift || 'normal';

     &emptyPromptBuffer();
     if ($reason ne 'CVP_hangup' and $reason ne 'normal' and $reason ne 'MAXTIMEOUT' and defined $main::ConfigParams->{'pretransferPrompt'} and $main::ConfigParams->{'enableagent'} ) {
         $self->play($main::ConfigParams->{'pretransferPrompt'} );
         &emptyPromptBuffer();
     }

     die $reason;
}



=head2 play PLAYPARAM

plays a sound (file, text-to-speech, whatever is appropriate) base on PLAYPARAM.  May or may not block during
play depending on device implementation.  Returns true on success.

=cut


sub play {
     my $self = shift;
     my $playthis = shift;
     my $type = shift;
     $playthis =~ s/\s+/ /g;
     $playthis =~ s/\$\s*([0-9]+)/ ' $' . Misc::Format->formatNumbers($1)/ge;

     $PromptBuffer = sprintf "%s %s", $PromptBuffer, $playthis;
     if ($BufferPrompt) {
       $QueuedPrompt = sprintf "%s %s", $QueuedPrompt, $playthis;
     }

     return 1;	

}



=head2 readnum PLAYTHIS TIMEOUT [REPEATTIMES]

Plays the PLAYTHIS and then waits for the sequence of the digit input finished. If none are entered within TIMEOUT 
seconds, it re-plays the message again. It returns failure (undefined value) if no digits are entered after the message
has been played REPEATTIMES (defaults to 3) times. 


It returns a string (a sequence of DTMF tones 0-9,A-D and `*') without the final stop key (normally '#'). 


=cut

sub readnum {
    my $self = shift;
    my $playthis = shift;
    my $timeout = shift;
    my $repeatTimes = shift || 3;
    my $os = "$^O";



    if ($os eq "MSWin32") {
         return readnum_MSWin32($self, $playthis, $timeout, $repeatTimes);
    }
    else {
         return readnum_linux($self, $playthis, $timeout, $repeatTimes);
    }
}


sub readnum_linux {
    my $self = shift;
    my $playthis = shift;
    my $timeout = shift;
    my $repeatTimes = shift || 3;

    &emptyPromptBuffer();
    $timeout = $VoiceXML::Client::UserAgent::UserAgentParams->{'timeout'};
    my $tmpfile = sprintf "TMP/%s", $main::sessionid;
    while ($timeout >= 0 ) {
      my $retcd = open(READHANDLE, "<$tmpfile");
      last if $retcd;
      sleep 1;
      $timeout -= 1;
    }

    if ($timeout >= 0 ) {
        my $value = <READHANDLE>;
        close(READHANDLE);
        unlink $tmpfile;
        ReportEvent $main::session, $VoiceXML::Client::UserAgent::EntrAIConfig,
         $VoiceXML::Client::UserAgent::child_dbh,
         {sessionid => $main::sessionid, 
          entraiapp => $main::session->{entraiapp},
          cvpapp => $VoiceXML::Client::UserAgent::cvpapp,
          entraiserver => $main::ip,
          cvpserver => ' ',
          agentserver => ' ',
          event => 'BOT_InProgress',
          subevent => 'Client_Input_Recvd',
          logdata => ''
         } ;
        my $msg = create_json ({who => "user", msg => $value} );
        push(@entraiMsgs::runTheApplication::Conversation, $msg);
        ($main::session->{lastinput} = $value ) =~ s/\s+/ /g;
        return(Misc::Utilities->normalizeInput(uri_unescape($value)));
     }
     else {
        $main::session->{lastinput} = '';
        return "";
     }
}



sub readnum_MSWin32 {
    my $self = shift;
    my $playthis = shift;
    my $timeout = shift;
    my $repeatTimes = shift || 3;

    &emptyPromptBuffer();
    $timeout = $VoiceXML::Client::UserAgent::UserAgentParams->{'timeout'};
    my $tmpfile = sprintf "TMP/%s", $main::sessionid;
    while ($timeout >= 0 ) {
    
      my $retcd = open(READHANDLE, "<$tmpfile");
      last if $retcd;
      sleep 1;
      $timeout -= 1;
    }

    if ($timeout >= 0 ) {
        my $value = <READHANDLE>;
        close(READHANDLE);
        unlink $tmpfile;
        ReportEvent $main::session, $VoiceXML::Client::UserAgent::EntrAIConfig,
         $VoiceXML::Client::UserAgent::child_dbh,
         {sessionid => $main::sessionid, 
          entraiapp => $main::session->{entraiapp},
          cvpapp => $VoiceXML::Client::UserAgent::cvpapp,
          entraiserver => $main::ip,
          cvpserver => ' ',
          agentserver => ' ',
          event => 'BOT_InProgress',
          subevent => 'Client_Input_Recvd',
          logdata => ''
         } ;
        my $msg = create_json ({who => "user", msg => $value} );
        push(@entraiMsgs::runTheApplication::Conversation, $msg);
        ($main::session->{lastinput} = $value ) =~ s/\s+/ /g;
        return(Misc::Utilities->normalizeInput(uri_unescape($value)));
     }
     else {
        $main::session->{lastinput} = '';
        return "";
     }
}



=head2 validDataFormats 

Returns an array ref of valid data formats (eg 'rmd', 'wav', 'au') the device will accept.

=cut

sub validDataFormats {
	my $self = shift;
	
	print "ValidDataFormats() queried\n";
	
	return ['rmd', 'wav', 'mp3'];
}


sub emptyPromptBuffer () {
  if ( $main::session->{'channeltype'} eq 'CHAT' ) {
     return emptyPromptBuffer_Chat();
  }
  elsif ( $main::session->{'channeltype'} eq 'SMS' ) {
     return emptyPromptBuffer_SMS();
  }
  else {
    die "unknown channel";
  }
} 

sub emptyPromptBuffer_Chat () {
  $PromptBuffer = Misc::Utilities->removeNonPrintable($PromptBuffer);


  if ($PromptBuffer !~ /^\s*$/ ) {
     my $msg = create_json ({who => "agent", msg => $PromptBuffer} );
     push(@entraiMsgs::runTheApplication::Conversation, $msg);
     $PromptBuffer =~ s/\s=/ /g;
     $PromptBuffer =~ s/\$\s*([0-9]+)/ ' $' . Misc::Format->formatNumbers($1)/ge;
     $PromptBuffer =~ s/\s+/ /g;
     $PromptBuffer =~ s/\&#226\;/\047/g;
     $PromptBuffer =~ s/[^[:ascii:]]/ /g;

     ReportEvent $main::session, $VoiceXML::Client::UserAgent::EntrAIConfig,
         $VoiceXML::Client::UserAgent::child_dbh,
         {sessionid => $main::sessionid, 
          entraiapp => $main::session->{entraiapp},
          cvpapp => $VoiceXML::Client::UserAgent::cvpapp,
          entraiserver => $main::ip,
          cvpserver => ' ',
          agentserver => ' ',
          event => 'BOT_InProgress',
          subevent => 'Client_Input_Sent',
          logdata => $PromptBuffer
         } ;
     
     my ($url, $ua);
     if ($main::ServerParams->{Protocol} eq 'wss' ) {
       $ua = LWP::UserAgent->new (
           ssl_opts => {verify_hostname => 0,
                        SSL_ca_file => $main::ServerParams->{SSL_cert_file},
                        protocols_allowed => ['https'],
           }
       );

       $url = sprintf "https://%s:%s", $main::ServerParams->{LocalAddr},
        $main::ServerParams->{port};
     } else {
       $ua = LWP::UserAgent->new();
       $url = sprintf "http://%s:%s", $main::ServerParams->{LocalAddr},
        $main::ServerParams->{port};
     }
     my $req = HTTP::Request->new(POST => $url);

     $req->content_type('application/json');

     my $content = create_json( {rqst => 'BROWSER:PROMPT', msg => $PromptBuffer, sessionid => $main::sessionid } ); 
     $req->content ($content);  
     my $res = $ua->request($req);

    if (!$res->is_success) {
          die 'CANTOUTPUT|In empty PromptBuffer Failed|', $res->status_line;
    }

     $PromptBuffer = '';
   }
   return 1;
}

sub emptyPromptBuffer_SMS (){

  my $s = $main::session;

  if (! $entraiMsgs::runTheApplication::playprompt) {
      $PromptBuffer='';
      $entraiMsgs::runTheApplication::playprompt=1;
      return 1;
  }

  $PromptBuffer =~ s/\$\s*([0-9]+)/ ' $' . Misc::Format->formatNumbers($1)/ge;
   

  $PromptBuffer = Misc::Utilities->removeNonPrintable($PromptBuffer);
   
  $PromptBuffer =~ s/\s+/ /g;
  $PromptBuffer =~ s/[^[:ascii:]]/ /g;

  if ($PromptBuffer !~ /^\s*$/) {

     ReportEvent $main::session, $VoiceXML::Client::UserAgent::EntrAIConfig,
         $VoiceXML::Client::UserAgent::child_dbh,
         {sessionid => $main::sessionid, 
          entraiapp => $main::session->{entraiapp},
          cvpapp => $VoiceXML::Client::UserAgent::cvpapp,
          entraiserver => $main::ip,
          cvpserver => ' ',
          agentserver => ' ',
          event => 'BOT_InProgress',
          subevent => 'Client_Input_Sent',
          logdata => $PromptBuffer
         } ;
     my $msg = create_json ({who => "agent", msg => $PromptBuffer} );
     push (@entraiMsgs::runTheApplication::Conversation, $msg);

     my $ua = LWP::UserAgent->new;  

     
     my $req = sprintf "%s&usernumber=%s&msg=%s&customer=%s", 
            $VoiceXML::Client::UserAgent::UserAgentParams->{SMSSvrURL}, uri_escape($s->{usernum}), uri_escape($PromptBuffer),
            uri_escape($VoiceXML::Client::UserAgent::UserAgentParams->{customer}); 

    $req =~ s/%85//g;
    $req =~ s/%92//g;

    my $res = $ua->get($req);

    unless ($res->is_success) {
         die 'CANTOUTPUT|In empty PromptBuffer Failed|', $res->status_line;    }

    $PromptBuffer='';
  }
  return 1;
}


1;
