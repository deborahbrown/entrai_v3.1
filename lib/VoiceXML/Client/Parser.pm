package VoiceXML::Client::Parser;

use XML::Mini::Document;
use VoiceXML::Client::Item::Factory;
use VoiceXML::Client::Document;
use Digest::MD5 qw(md5_hex);
use Data::Dumper;
use Storable ;


use strict;


=head1 COPYRIGHT AND LICENSE

	
	Copyright (C) 2007,2008 by Pat Deegan.
	All rights reserved
	http://voicexml.psychogenic.com

This library is released under the terms of the GNU GPL version 3, making it available only for 
free programs ("free" here being used in the sense of the GPL, see http://www.gnu.org for more details). 
Anyone wishing to use this library within a proprietary or otherwise non-GPLed program MUST contact psychogenic.com to 
acquire a distinct license for their application.  This approach encourages the use of free software 
while allowing for proprietary solutions that support further development.


This file is part of VoiceXML::Client.

 
 
    VoiceXML::Client is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VoiceXML::Client is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VoiceXML::Client.  If not, see <http://www.gnu.org/licenses/>.

 
=cut



sub new {
     my ($class, $file) = @_;

	
	my $self = {};
        
        bless $self, ref $class || $class;
	
	$self->parse($file) if ($file);
		
		
	return $self;
}


=head2 parse FILE

Parses VXML file FILE and returns the VoiceXML::Client::Document object.

=cut


my %SavedVXMLPages;

sub parse {

      my ($self, $fileOrString, $runtime) = @_;
	if(!$fileOrString ) { die "VoiceXML::Client::Parser::parse MUST pass string or file";}
	if (! $runtime ) {  die "VoiceXML::Client::Parser::parse MUST pass runtime";}

      my $savdoc = sprintf "%s/%s", $main::ConfigParams->{'cacheDir'}, md5_hex($fileOrString);

     if (-e "$savdoc.txt") { 
       return retrieve("$savdoc.txt");
     } 
     
     $fileOrString = preProcess($fileOrString);     
	
	$self->{'_XMLDoc'} = XML::Mini::Document->new();
	$self->{'vxml'} = []; # make sure we're nice and empty

	my $numChildrenFound;
	if ($fileOrString =~ m/\n/ || length($fileOrString) > 250)
	{

		# it is VXML content?
		# TODO: Need a better test...

		$numChildrenFound = $self->{'_XMLDoc'}->fromString($fileOrString);
 	
		die "VoiceXML::Client::Parser::parse() No xml children found in string "
			unless($numChildrenFound);
	} else {
		
		die "VoiceXML::Client::Parser::parse() Must pass a filename or contents"
			unless ($fileOrString);
	
		die "VoiceXML::Client::Parser::parse() Can't find file '$fileOrString'"
			unless(-e $fileOrString);
	
		die "VoiceXML::Client::Parser::parse() Can't read file '$fileOrString'"
			unless(-r $fileOrString);
	
		$numChildrenFound = $self->{'_XMLDoc'}->fromFile($fileOrString) if ($fileOrString);
	
		die "VoiceXML::Client::Parser::parse() No xml children found in  $fileOrString"
			unless($numChildrenFound);
	}
	

			
	$self->{'_XMLRoot'} = $self->{'_XMLDoc'}->getRoot();


	$self->{'itemFactory'} = VoiceXML::Client::Item::Factory->new();
	
	
	my $vxmlElement = $self->{'_XMLRoot'}->getElement('vxml') 
				|| die "VoiceXML::Client::Parser::parse() no vxml element found in document.";
      	
	my $vxmlChildren = $vxmlElement->getAllChildren();

	my $numChildren = scalar(@{$vxmlChildren});
	
	die "VoiceXML::Client::Parser::parse() no form elements found in vxml element."
		unless ($numChildren);
		
	
	my $docname = $fileOrString;
	$docname =~ s|^.*/||;
	
	$self->{'VXMLDoc'} = VoiceXML::Client::Document->new($docname, $runtime);
	
	for(my $i=0; $i < $numChildren; $i++)
	{
		my $name = $vxmlChildren->[$i]->name() || die "No name for child $i in VoiceXML::Client::Parser::parse()";
		$self->{'vxml'}->[$i] = $self->{'itemFactory'}->newItem($name, $vxmlChildren->[$i], $self->{'VXMLDoc'});
	
	}
	
	$self->{'VXMLDoc'}->addItem($self->{'vxml'});
      my @emptyarray = ();
      $self->{'VXMLDoc'}->{'_links'} = \@emptyarray;
      store $self->{'VXMLDoc'}, "$savdoc.txt";



      return $self->{'VXMLDoc'};
}


sub preProcess {
   my ($str) = @_;

   #Catch event shortcuts
   $str =~ s#<\s*noinput#<catch event="noinput"#go;
   $str =~ s#</noinput\s*>#</catch>#go;
   $str =~ s#<\s*nomatch#<catch event="nomatch"#go;
   $str =~ s#</nomatch\s*>#</catch>#go;
   $str =~ s#<\s*help#<catch event="help"#go;
   $str =~ s#</help\s*>#</catch>#go;
   $str =~ s#<\s*error#<catch event="error"#go;
   $str =~ s#</error\s*>#</catch>#go;
   $str =~ s#<\s*paragraph\s+#<p #go;
   $str =~ s#</paragraph#</p#go;

   $str =~ s#<\s*say\-as#<sayas#g;
   $str =~ s#</\s*say\-as#</sayas#g;
   
   $str = removeXMLComments ($str, '<!--', '-->');

   return $str;
}

sub removeXMLComments {
 
    my ($str, $BEGINSTR, $ENDSTR) = @_;

    my $beginpos = 0;
    my $beginstr_length = length($BEGINSTR);
    my $endstr_length = length($ENDSTR);
    while (($beginpos = index($str, $BEGINSTR, $beginpos)) >= 0) {
       my $endpos = index($str, $ENDSTR, $beginpos + length($BEGINSTR));
       if ($endpos < 0 ) {die "malformed comment in XML";}
       $str = sprintf "%s %s", substr($str,0, $beginpos-1), substr($str,$endpos+$endstr_length);
    }
    return $str;
}

1;
