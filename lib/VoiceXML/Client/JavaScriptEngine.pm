package VoiceXML::Client::JavaScriptEngine;



use VoiceXML::Client::Util;
use VoiceXML::Client::Item::Util;

use strict;
use warnings;
use WWW::Scripter;

my $w;

sub initJSEngine {
   my $self = shift;
   $w = new WWW::Scripter;
   $w->use_plugin('JavaScript');  # packaged separately
}

sub JSEvalExpression {
   my $self = shift;
   my $expr = shift;
 
   return $w->eval($expr);
}

sub JSAssign {
    my $self = shift;
    my $name = shift;
    my $expr = shift;
    my $varflag = shift;

    $expr =~ s/"/\\"/g;

    $expr = sprintf "%s = %s", $name, $expr;
    if ($varflag) {
       $expr =~ s/^/var /;
    }
    return $w->eval($expr);
}

1;

