package entraiMsgs::runTheApplication;

#Run the CVP Applications
use Net::WebSocket::Server;
use VoiceXML::Client;
use LWP::UserAgent;
use HTTP::Cookies;
use Agent::Upstream;
use Agent::GenericAgent;
use Misc::Session;
use HTTP::Request;
use POSIX":sys_wait_h";
use Misc::Utilities;
use JSON::Parse 'parse_json';
use JSON::Create 'create_json';
use strict;
use Data::Dumper;
 
my $ReportDBConnInfo = {
     connstr => 'DBI:MariaDB:database=entrAIDB;host=127.0.0.1',
     user => 'entrai',
     pwd => 'YES'
}; 

our $ConfigParams;

sub runTheApplication {
  $SIG{PIPE} = 'IGNORE';
   my($session, $appConfig, $playfirstmessage, $ip, $ReportDBConnInfo, $telephonyDevice)=@_;
   my $sessionid = $session->{id};

                     
   my $dbh = ConnectToDB($ReportDBConnInfo);
   my $EntraiConfig = {
    TuningDataPct => $main::ServerParams->{TuningDataPct}
   };
   my $LogLevel = $EntraiConfig->{LogLevel};
 
   #Read application configuration file
   $ConfigParams = $main::ServerParams;
   $ConfigParams = readConfig($ConfigParams, $appConfig);
   $main::ConfigParams = $ConfigParams;
   $ConfigParams->{cookiefile} = "cookie$sessionid.txt";
 
   $ConfigParams->{startURL} = sprintf $ConfigParams->{startURL}, $sessionid;
   $ConfigParams->{dbh} = $dbh;
   $ConfigParams->{EntraiConfig} = $EntraiConfig;

   our @Conversation;
   our $playprompt = $playfirstmessage;

   $session->{ConfigParams} = $ConfigParams; 
   my $vxmlUserAgent = VoiceXML::Client::UserAgent->new($ConfigParams->{sourceSite}, "/", $ConfigParams);

   eval {
       $vxmlUserAgent->runApplication($ConfigParams->{startURL},
       $telephonyDevice);
   };

   my $reason = $@;
   if ($reason eq 'CVP_hangup') {
      ReportEvent $session,
          $EntraiConfig,
          $dbh,
          {sessionid => $sessionid,
              entraiapp => $session->{entraiapp},
              cvpapp => $ConfigParams->{cvpapp}, 
              entraiserver => $ip,
              cvpserver => ' ',
              agentserver => ' ',
              event => 'BOT_Complete',
              subevent => 'CVP_DisConn_Hangup',
              logdata => ''};
   }
   elsif ($reason eq 'normal' or $reason =~/MAXTIMEOUT/) {
      ReportEvent $session,
          $EntraiConfig,
          $dbh,
          {sessionid => $sessionid,
          entraiapp => $session->{entraiapp},
          cvpapp => $ConfigParams->{cvpapp}, 
          entraiserver => $main::ip,
          cvpserver => ' ',
          agentserver => ' ',
          event => 'BOT_Complete',
          subevent => 'CVP_DisConn_Normal',
          logdata => ' '};
   }
   elsif ($reason =~ /client_disconnect/) {
     $telephonyDevice->hangup($session);
     exit();
  }
  else {
      ReportEvent $session,
          $EntraiConfig,
          $dbh,
          {sessionid => $sessionid,
             entraiapp => $session->{entraiapp},
             cvpapp => $ConfigParams->{cvpapp}, 
             entraiserver => $main::ip,
             cvpserver => ' ',
             agentserver => ' ',
             event => 'BOT_Complete',
             subevent => 'CVP_DisConn_Error',
             logdata => $reason};

   }

   $telephonyDevice->hangup($session);
 
   if ($ConfigParams->{enableagent} and
         ($reason !~ /^normal/
         and $reason !~ /^MAXTIMEOUT/
         and $reason !~ /CANTOUTPUT/
         and $reason !~ /CONNECTIONCLOSED/)) {

      sendEntraiRqst('SESSION:SETSTATE', $sessionid, 'agent');
      if ($ConfigParams->{'AgentVendor'} eq 'Upstream' ) {
            my $queueid = Agent::Upstream::getQueue($reason, $session->{username});

            my $retcd = Agent::Upstream->transferToAgent($sessionid,
             \@Conversation, $session->{'channeltype'}, 
             $ConfigParams->{'UpstreamApplication'},
             $session->{username},
             $queueid);
  
            if ($retcd) {
               ReportEvent $session,
                $EntraiConfig, $dbh,
                {sessionid => $sessionid,
                entraiapp => ' ',
                cvpapp => ' ', 
                entraiserver => $main::ip,
                cvpserver => ' ',
                agentserver => 'Upstream',
                event => 'Agent_Init',
                subevent => ' ',
                logdata => $queueid};
           } else {
               ReportEvent $session,
                $EntraiConfig, $dbh,
                {sessionid => $sessionid,
                entraiapp => ' ',
                cvpapp => ' ', 
                entraiserver => $main::ip,
                cvpserver => ' ',
                agentserver => 'Upstream',
                event => 'Agent_Timeout',
                subevent => ' ',
                logdata => $queueid};
                sendEntraiRqst('AGENT:SVRTIMEDOUT', $sessionid);

           }

      }

      elsif (defined $ConfigParams->{'AgentVendor'} ) {
            Agent::GenericAgent->transferToAgent($ConfigParams->{"AgentVendor"}, \@Conversation, $reason, $session);
      }
   }

   else { #clean up or pause session

            if ($session->{channeltype} eq 'CHAT') {
              my $cn = $session->{'browser'};
              my $json = create_json( {rqst => 'BROWSER:DISCONNECT'});
              $cn->send_utf8($json);
             
            }
            my $uri;
            if ($reason =~ /^MAXTIMEOUT/ and $session->{'channeltype'} eq 'SMS') {
              sendEntraiRqst('PAUSESMSSESSION', $sessionid);

            }
            elsif ($session->{'channeltype'} eq 'CHAT') {
              sendEntraiRqst('ENDSESSION', $sessionid);
            }
            else {
              sendEntraiRqst('ENDSMSSESSION', $sessionid);
            }


   }

   exit(0);
}


sub sendEntraiRqst {
  my ($rqst, $sessionid) = @_;

  my ($uri, $ua);
  if ($main::ServerParams->{Protocol} eq 'wss' ) {
    $uri =  sprintf "https://%s:%s", $main::ServerParams->{LocalAddr},
             $main::ServerParams->{port};
    $ua = LWP::UserAgent->new (
           ssl_opts => {verify_hostname => 0,
                        SSL_ca_file => $main::ServerParams->{SSL_cert_file},
                        protocols_allowed => ['https'],
     });
  }
  else {
    $uri =  sprintf "http://%s:%s", $main::ServerParams->{LocalAddr},
             $main::ServerParams->{port};
    $ua = LWP::UserAgent->new();

  }
  my $req = HTTP::Request->new(POST => $uri );
  $req->content_type('application/json');
  my $content = create_json({rqst => $rqst, sessionid => $sessionid});
  $req->content ($content);
  my $response = $ua->request($req);
}

1;
