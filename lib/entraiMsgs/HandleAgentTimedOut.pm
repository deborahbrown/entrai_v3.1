package entraiMsgs::HandleAgentTimedOut;

use Net::WebSocket::Server;
use LWP::UserAgent;
use HTTP::Cookies;
use HTTP::Request;
use POSIX":sys_wait_h";
use Misc::Utilities;
use strict;
use Data::Dumper;
use JSON::Create 'create_json';
use URI::Escape;

sub HandleAgentTimedOut {
  my ($ConfigParams, 
   $EntraiConfig,
   $dbh,
   $session,
   $conn,
   $json_obj,
   $ip,
   $protocol,
   $SESSIONS) = @_;

   my $sessionid = $session->{id};
   my $SESSIONID;
   my @SessionsToNotify;

   $conn->send_resp($protocol, 200, "OK"); 

   if ($json_obj->{rqst} eq 'AGENT:SVRNORESPONSE') {
      $SESSIONID = 'xxxxxx';
      @SessionsToNotify = keys %$SESSIONS;
   }
   else {
      $SESSIONID = $json_obj->{sessionid}; 
      $SESSIONS->{$SESSIONID}->{state} = 'agent';
      @SessionsToNotify = ($sessionid);
   }

   ReportEvent $session,
       $EntraiConfig, $dbh,
       {sessionid => $SESSIONID,
       entraiapp => ' ',
       cvpapp => ' ', 
       entraiserver => $ip,
       cvpserver => ' ',
       agentserver => 'Upstream',
       event => 'Agent_Timeout',
       subevent => 'Server not responding',
       severity => 1,
       logdata => ''};

  foreach my $sid (@SessionsToNotify) {
    my $s = $SESSIONS->{$sid};
    if ($s->{state} ne 'agent') { return;}
    if ( $s->{channeltype} eq 'CHAT' ) {
      my $cnn = $s->{browser};
      my $json = create_json( {rqst => 'AGENT:NORESPONSE'} );
      $cnn->send_utf8($json);
      delete $SESSIONS->{$sid};
    }
    else {
      my $ua = LWP::UserAgent->new;
      my $msg ="We are experiencing technical difficulty. Please contact us later";
      my $req = sprintf "%s&usernumber=%s&msg=%s&customer=%s",$main::ServerParams->{SMSSvrURL}, uri_escape($s->{usernum}),  uri_escape($msg),
              uri_escape($s->{ConfigParams}->{customer}); 
      $req =~ s/%85//g;
      $req =~ s/%92//g;
      my $res = $ua->get($req);
      delete $SESSIONS->{$sid};
    }

  }
  return;
}
1;
