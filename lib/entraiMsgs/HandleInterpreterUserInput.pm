package entraiMsgs::HandleInterpreterUserInput;

use LWP::UserAgent;
use Misc::Utilities;
use HTTP::Request;
use HTTP::Cookies;
use Agent::Upstream;
use Misc::Session;
use POSIX":sys_wait_h";
use Misc::Utilities;
use JSON::Create 'create_json';
use Data::Dumper;


sub HandleInterpreterUserInput {
  my($ConfigParams,
       $EntraiConfig,
       $dbh,
       $session,
       $conn,
       $json_obj,
       $ip,
       $anything,
       $connopen) = @_;

  my $sessionid = $session->{id};
  if (($session->{channeltype}) eq 'SMS' and $connopen ) {
     $conn->send_resp('http', 200, "OK");
     $conn->close();
     $connopen=0;
  }

  if ($session->{state} eq 'IVR') {
                     
       open(WRITEHANDLE, ">TMP/$sessionid") || return;
       print WRITEHANDLE $json_obj->{'msg'}, "\n";
       close (WRITEHANDLE);                           
       return;                 
  }
  else {
    if ($ConfigParams->{'AgentVendor'} eq 'Upstream') {
      ReportEvent $session,
       $EntraiConfig, $entrAI::runTheApplication::dbh,
       {sessionid => $sessionid,
        entraiapp => ' ',
        cvpapp => ' ', 
        entraiserver => $main::ip,
        cvpserver => ' ',
        agentserver => 'Upstream',
        event => 'Agent_InProgress',
        subevent => 'Client_Input_Recvd',
        logdata => ''};
      my $msg = $json_obj->{msg};
      #Pass information to Agent interface
      my $uri = sprintf "%s/frombrowser", $main::ServerParams->{UpstreamSvrAddr};
      my $ua = LWP::UserAgent->new;
      my $req = HTTP::Request->new(POST => $uri );
      $req->content_type('application/json');
      my $content = create_json({ sessionid => $sessionid,
          msg => $msg } );
      $req->content ($content);

      my $response = $ua->request($req);
    }
}
}
 1;
