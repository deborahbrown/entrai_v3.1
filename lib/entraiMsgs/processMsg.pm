package entraiMsgs::processMsg;

#use lib '../../lib';
use VoiceXML::Client;
use LWP::UserAgent;
use HTTP::Cookies;
use Agent::Upstream;
use Agent::GenericAgent;
use Misc::Session;
use HTTP::Request;
use POSIX":sys_wait_h";
use Misc::Utilities;
use JSON::Parse 'parse_json';
use JSON::Create 'create_json';
use Sys::HostAddr;
use URI::Escape;
use Data::Dumper;
use Text::Unidecode;
use strict;
use entraiMsgs::runTheApplication;
use entraiMsgs::HandleAgentTimedOut;
use entraiMsgs::HandleInterpreterUserInput;


my $sessioncount=0;
my $telephonyDevice;
my ($READHANDLE, $WRITEHANDLE);
my %UserNumToSession;




sub processMsg {
                my ($protocol, $conn, $msg, $dbh) = @_;
                $msg = checkFormat($msg, $protocol); 

                my $json_obj = parse_json($msg);
                my $rqst = $json_obj->{rqst};
                if (defined $json_obj->{msg} ) {
                   $json_obj->{msg} = unidecode($json_obj->{msg});
                   $json_obj->{msg} =~ s/"//g;
                   $json_obj->{msg} =~ s/'//g;
                }
    
                my $appConfig;
                if ($rqst eq 'SESSION:SETSTATE') {
                    $main::SESSIONS->{$json_obj->{sessionid}}->{state} = $json_obj->{msg};
                    return;
                }

                if ($rqst eq 'INTERPRETER:CONNECT' and $protocol eq 'ws') {
                    $sessioncount++;
                    my $username = $json_obj->{username};
                    my $queueid = $json_obj->{userid};
                    
                     
                    $appConfig = $json_obj->{'config'};
                    if (!defined $appConfig) {
                       $conn->send_resp($protocol, 400, "Bad Request: missing configuration file");
                       $conn->close();
                       return;
                    }
                    ( my $entraiapp = $appConfig) =~ s/.*\///;

                    $appConfig = sprintf "Applications/%s.config", $appConfig;


                    $main::sessionid = sprintf "chat-%s-%07d-%06d", $main::ip, $$, $sessioncount;
                    $conn->{'sessionid'} = $main::sessionid;
                    my $json = create_json( {rqst => 'BROWSER:SESSIONNUM', sessionid => $main::sessionid} );
                    $conn->send_utf8($json);
                                
          
                    $main::session = Misc::Session->new($main::random100, $main::sessionid, $appConfig, 'CHAT', $queueid,$conn);
                    $main::SESSIONS->{$main::sessionid} = $main::session;
                    ReportEvent $main::session, $main::EntraiConfig, $dbh, {sessionid => $main::sessionid, entraiapp => $main::session->{entraiapp}, cvpapp => '', entraiserver => $main::ip, cvpserver => '',
                                                  agentserver => '', event => 'Client_Conn_Accepted', subevent => '', logdata => ''};
                    $telephonyDevice = VoiceXML::Client::Device::Dummy->new();
                    $telephonyDevice->connect($main::session);
 
            
              }

              else {

                    $main::sessionid = $json_obj->{'sessionid'};
                    if ($protocol eq 'ws' and (!defined $main::sessionid or $main::sessionid eq '')) {
                       $conn->send_resp($protocol, 400, "Bad Request: session not defined");
                       $conn->close();
                       return;
                    }
                    $msg = $json_obj->{msg};
                    $main::session = $main::SESSIONS->{$main::sessionid};
 
                    #If SMS session is inactive, restart it

                    if ( defined $main::session  and $main::session->{channeltype} eq 'SMS' and !$main::session->isActive()  and $rqst eq 'INTERPRETER:USERINPUT' ) { #Got a late SMS response after timeout
                       $conn->send_resp($protocol, 200, "OK"); 
                       $conn->close(); 
                       $main::connopen=0;
                       $main::session = $main::SESSIONS->{$main::sessionid};
                       startSession(0, $msg, $main::session->{'config'}, $READHANDLE, $WRITEHANDLE);
                    }
                    
                    elsif ( (!defined ($main::session)) and ($rqst ne 'STARTSMS' ) ) {
                       $conn->send_resp($protocol, 400, "Bad Request: SMS $rqst session not defined and not start");
                       $conn->close();
                       return;
                    }
              }

                if ($rqst eq 'AGENT:SVRNORESPONSE' or $rqst eq 'AGENT:SVRTIMEDOUT')  {
                 entraiMsgs::HandleAgentTimedOut::HandleAgentTimedOut($main::ConfigParams,
                       $main::EntraiConfig,
                       $dbh,
                       $main::session,
                       $conn,
                       $json_obj,
                       $main::ip,
                       $protocol,
                       $main::SESSIONS);
                 return;
                }
                if ($rqst eq 'TRANSFERTOAGENT') {

                     my $connection = $main::session->{browser};
                     my $msg = create_json({rqst => 'TRANSFERTOAGENT',
                                            sessionid => $json_obj->{sessionid},
                                            msg => '',
                                            languageCode => $json_obj->{languageCode},
                                            countryCode => $json_obj->{countryCode},
                                            entryPointId => $json_obj->{entryPointId}
                    });
                  
                     $conn->send_resp($protocol, 200, "OK");
                     $connection->send_utf8($msg);

                      ReportEvent $main::session,
                       $main::EntraiConfig, $dbh,
                       {sessionid => $main::sessionid,
                        entraiapp => ' ',
                        cvpapp => ' ', 
                        entraiserver => $main::ip,
                        cvpserver => ' ',
                        agentserver => $main::session->{ConfigParams}->{AgentVendor},
                        event => 'Agent_Init',
                        subevent => 'Client_Input_Sent',
                        logdata => ''};

                    return;
                }
              if ($rqst eq 'STARTSMS' and $protocol eq 'http') {  


                    $conn->send_resp($protocol, 200, "OK"); 
                    $conn->close();  
                    $main::connopen=0;

                    my $config = $json_obj->{'config'};

                    if (!defined $config or $config eq "") {
                        $config = "Default/default";
                    }  



                    my $usernum = $json_obj->{usernum};
                    $sessioncount++;
                    $main::sessionid = sprintf "sms-%s-%07d-%06d", $main::ip, $$, $sessioncount;
                    $main::session = setupSMSsession($protocol, $main::sessionid, $config, $usernum); 
                    $appConfig =  $config . '.config'; 
                    $main::session->{random100} =  int(rand(100)) + 1 ; #This is used to percentage of session that will be logged
                    ReportEvent $main::session, $main::EntraiConfig, $dbh, {sessionid => $main::sessionid, entraiapp => $main::session->{entraiapp}, cvpapp => '', entraiserver => $main::ip, cvpserver => '',
                                                  agentserver => '', event => 'Client_Conn_Accepted', subevent => '', logdata => ''};            
              }

              elsif ($rqst eq 'ENDSESSION'  and $protocol eq 'http') {

                    $conn->send_resp('http', 200, "OK");
                    $conn->close(); 
                    my $cn = $main::session->{'browser'};
                    my $json = create_json( {rqst => 'DISCONNECT'} );
                    eval($cn->send_utf8($json));
                    delete $main::SESSIONS->{$main::sessionid};
                    return;

                   
              }

              elsif ($rqst eq 'ENDSMSSESSION'  and $protocol eq 'http') {

                    $conn->send_resp('http', 200, "OK");
                    $conn->close();
                    
                   # delete $UserNumToSession{$main::session->{usernum}};
                    #delete $main::SESSIONS->{$main::sessionid};
 
                    return;

                   
              }

              elsif ($rqst eq 'PAUSESMSSESSION' and $protocol eq 'http') {
 
                    $conn->send_resp('http', 200, "OK");
                    $conn->close(); 

                    $main::SESSIONS->{$main::sessionid}->setInactive();
                    return;
              }

                

              elsif ($rqst eq 'DISCONNECT' and $protocol eq 'ws') {
                     ReportEvent $main::session,
                     $main::EntraiConfig,
                     $dbh,
                     {sessionid => $main::sessionid,
                        entraiapp => $main::session->{entraiapp},
                        cvpapp => '',
                        entraiserver => $main::ip,
                        cvpserver => '',
                        agentserver => '',
                        event => 'BOT_Complete',
                        subevent => 'Client_DisConn_Manual',
                        logdata => ''};

                      kill 'TERM', $main::session->{childpid};
                      delete $main::SESSIONS->{$main::session}->{id};
                     return;
              }
              elsif ($rqst eq 'BROWSER:PROMPT') {

                     my $connection = $main::session->{browser};
                     my $json = create_json( {rqst => 'BROWSER:PROMPT', msg => $msg} );
                  
                     $conn->send_resp($protocol, 200, "OK");
                     $connection->send_utf8($json);



                     my $sender = $json_obj->{sender};
                     if (defined $sender and $sender eq 'agent') {
                       $main::session->{state} = 'agent';
                      ReportEvent $main::session,
                       $main::EntraiConfig, $dbh,
                       {sessionid => $main::sessionid,
                        entraiapp => ' ',
                        cvpapp => ' ', 
                        entraiserver => $main::ip,
                        cvpserver => ' ',
                        agentserver => $main::session->{ConfigParams}->{AgentVendor},
                        event => 'Agent_InProgress',
                        subevent => 'Client_Input_Sent',
                        logdata => ''};
                     }
                     return;

              }



              elsif ($rqst eq 'AGENTTOSMS' and $protocol eq 'http') {
                      ReportEvent $main::session,
                       $main::EntraiConfig, $dbh,
                       {sessionid => $main::sessionid,
                        entraiapp => '',
                        cvpapp => '', 
                        entraiserver => $main::ip,
                        cvpserver => '',
                        agentserver => $main::session->{ConfigParams}->{AgentVendor},
                        event => 'Agent_InProgress',
                        subevent => 'Client_Input_Recvd',
                        logdata => ''};

                      my $msg = $json_obj->{msg};

                      $main::session->{state} = 'agent';

                      $conn->send_resp($protocol, 200, "OK");

                       
                      my $ua = LWP::UserAgent->new;
                      $msg =~ s/[^[:ascii:]]/ /g;
  
                      my $req = sprintf "%s&usernumber=%s&msg=%s&customer=%s",$main::ServerParams->{SMSSvrURL}, uri_escape($main::session->{usernum}),  uri_escape($msg),
                              uri_escape($main::ConfigParams->{customer}); 

                      $req =~ s/%85//g;
                      $req =~ s/%92//g;
                      my $res = $ua->get($req);

                     return;
              }

              elsif ($rqst ne 'INTERPRETER:USERINPUT' and $rqst ne 'INTERPRETER:CONNECT') {
                      $conn->send_resp($protocol, 400, "Bad Request");
                      $conn->close();
                      return;
              }



              elsif ($rqst eq 'INTERPRETER:USERINPUT') { 
                 entraiMsgs::HandleInterpreterUserInput::HandleInterpreterUserInput($main::ConfigParams,
                       $main::EntraiConfig,
                       $dbh,
                       $main::session,
                       $conn,
                       $json_obj,
                       $main::ip,
                       $WRITEHANDLE,
                       $main::connopen);
                  return;

             }

             
             startSession(1, $msg, $appConfig, $READHANDLE, $WRITEHANDLE);         

}

sub startSession {
       my ($playfirstmessage, $msg, $appConfig, $READHANDLE, $WRITEHANDLE) = @_;
                
              FORK: {
 

                   pipe($READHANDLE, $WRITEHANDLE);
                   my $oldfh = select($WRITEHANDLE);
                   $|=1;
                   select($oldfh);
                   $main::session->{pipe} = $WRITEHANDLE;
                   my $pipe = $main::session->{pipe};
                   
                   if (my $pid = fork) {
                      $main::ConfigParams = readConfig($main::ConfigParams,$appConfig);
                      $main::session->{childpid} = $pid;
                      $main::session->{ConfigParams} = $main::ConfigParams;
                      close $READHANDLE;

                      return;                 
       
                   }
                   else {
                      #Define signal handler - send SIGTERM if client ends
                      $SIG{TERM} = 'HANDLER';
                      sub HANDLER { 
                       die "client_disconnect";

                      }
                      close $WRITEHANDLE;
                      entraiMsgs::runTheApplication::runTheApplication($main::session,
                          $appConfig,
                          $playfirstmessage,
                          $main::ip,
                          $main::ReportDBConnInfo,
                          $telephonyDevice);


                   }
              }
}


sub setupSMSsession {
    my ($protocol,$sessionid, $config, $usernum) = @_;


    $main::session = Misc::Session->new($main::random100, $main::sessionid, $config . '.config', 'SMS', $usernum);
    $main::session->{username} = 'user@speech-soft.com';
    $main::SESSIONS->{$main::sessionid} = $main::session;
    $UserNumToSession{$usernum} = $main::sessionid;
    $telephonyDevice = VoiceXML::Client::Device::Dummy->new();
    $telephonyDevice->connect($main::session);
    
    return $main::session;
}



#The following converts messages that are not json to json
#Temporary until have all messages conform to API

sub checkFormat { 
   my ($msg, $protocol) = @_;

   if ($msg =~ /^STARTSMS:/) {
        my ($cfg, $usernum) = ($msg =~ /^STARTSMS:([^\s:]+):([^\s:]+)/ );
        my $json = create_json( {rqst => 'STARTSMS', config => $cfg, username => '' , queueid => '', 
                                   usernum => $usernum, sessionid => '' , msg => '' } );
        return $json;
   }
   elsif ($msg =~ /^INTERPRETER:USERINPUT:/) {
        my ($usernum, $mesg) = ($msg =~ /^INTERPRETER:USERINPUT:([^:]+):([^:]+):/ );
        my $json = create_json( {rqst => 'INTERPRETER:USERINPUT', config => '', username => '' , queueid => '', 
                                   usernum => $usernum, sessionid => $UserNumToSession{$usernum} , msg => $mesg } );
        return $json;
   }
   return $msg;
}                 

1;
