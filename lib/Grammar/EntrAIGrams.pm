package Grammar::EntrAIGrams;
use Grammar::GrammarServer;
use JSON::Parse 'parse_json';
use JSON::Create 'create_json';
use NLU::NLU;
use IPC::Cmd qw [ run];
use VoiceXML::Client::Device::Dummy;
use Misc::Utilities;
use Data::Dumper;

use strict;
use warnings;


=head
Grammars included:

    _entrai_:digits
    _entrai_:NULL
    _entrai_:GARBAGE
    _entrai_:VOID
    _entrai_:DBLookup?gramname=<grammar>
    _entrai_:RemoteSvr?gramname=<grammar>
    _entrai_:Learn?gramname=<grammar>
    _entrai_:Custom?gramname=<grammar>&parameter1=<parameter1 value>&...&parametern=<parameter value>
    _entrai_:NLU?model=<modelfile>




=cut
   
#Lex states
my %LexFinished = ('ConfirmIntent', 0, 'Close', 1, 'ElicitIntent', 0,
                   'ElicitSlot', 0, 'Failed', 1, 'ReadyForFulfillment', 1);
my %LexBotErr = ('ConfirmIntent', 0, 'Close', 0, 'ElicitIntent', 0,
                   'ElicitSlot', 0, 'Failed', 1, 'ReadyForFulfillment', 0);

#Builtins names have form _entrai_:grammarname?parameters
sub match_Special {
    my ($self, $builtin, $response, $metaParams) = @_;
    my ($grammarname) = ($builtin =~ /_entrai_:([^\?]+)/);
    if ($grammarname eq 'GARBAGE') {
       my ($firstword) = split(/\s+/, $response, 2);
       if (!defined $firstword) { $firstword = ''; }
       return builtInResponse($firstword, '');
    }
    if ($grammarname eq 'NULL') {
       return nullResponse($response);
    }
    elsif ($grammarname eq 'VOID') {
       my @emptyarray = ();
       return \@emptyarray;
    } 


    my ($parameters) = ($builtin =~ /\?(.*)/);

    if ($grammarname =~ /^_*digits$/ ) {
       return entrAI_digits($response, $parameters, $metaParams);
    }

    elsif ($grammarname eq 'Learn' ) {
       return entrAI_Learn($response, $parameters, $metaParams);
    }

    elsif ($grammarname eq 'DBLookup' ) {
       return entrAI_DBLookup($response, $parameters, $metaParams);
    }

    elsif ($grammarname eq 'RemoteSvr' ) {
       return entrAI_RemoteSvr($response, $parameters, $metaParams);
    }
    elsif ($grammarname eq 'NLU' ) {
       return entrAI_NLU($response, $parameters, $metaParams);
    }
    elsif ($grammarname eq 'Custom' ) {
       return entrAI_Custom($response, $parameters, $metaParams);
    }
    die "Don't know how to handle special grammar $grammarname";
}

sub entrAI_digits {
   my ($response, $parameters, $metaParams) = @_;
   my ($nospace_response) = ($response =~ /^\s*([\d]+)/);
   if (defined $nospace_response) {
     $nospace_response =~ s/\s+//g;
     if ($nospace_response =~ /^\d+$/) {
       return builtInResponse($nospace_response, $nospace_response, $metaParams);
     }
   }
   {
     my @emptyarray = ();
     return \@emptyarray;
   }
}



sub entrAI_Learn {
   my($response, $parameters, $metaParams) = @_;
   
   my $Params = parseParams ($parameters);
   if (!defined $Params->{'gramname'} ) {
      die "EntrAIGrams.pm: no grammar defined for DBLookup";
   }
   my $result = Grammar::GrammarServer->retrieveInterpretation($response, $Params->{'gramname'});

   if ( $result->{'status'} eq 'NOMATCH') {
     LogRecogEvent ( $main::session,
        undef,
        $main::sessionid,
        $metaParams->{mask_logging},
        $metaParams->{gramfile},
        $response,
        'NOMATCH',
     '');
       return entrAI_RemoteSvr($response, $parameters, $metaParams);
   } 
   else {
     return builtInDBResponse($response, $result->{'tag'}, $metaParams);
   }      

}

sub entrAI_DBLookup {
   my($response, $parameters, $metaParams) = @_;
   
   my $Params = parseParams ($parameters);
   if (!defined $Params->{'gramname'} ) {
      die "EntrAIGrams.pm: no grammar defined for DBLookup";
   }
   my $result = Grammar::GrammarServer->retrieveInterpretation($response, $Params->{'gramname'});

   if ( $result->{'status'} eq 'NOMATCH') {
      my @emptyarray = ();
      return \@emptyarray;
   } 
   else {
     return builtInDBResponse($response, $result->{'tag'}, $metaParams);
   }      

}

sub entrAI_RemoteSvr {
   my($userresponse, $parameters, $metaParams) = @_;

   my $origuserresponse = $userresponse;
   my $Params = parseParams ($parameters);

   if (!defined $Params->{'gramname'} ) {
      die "EntrAIGrams.pm: no grammar defined";
   }
   $metaParams->{altgramname} = $Params->{gramname};

   my ($message, $cmd, $bot_state, $bot_msg);
   $cmd = sprintf "node /data/speechsoft/apps/entrAI/lib/NodeJS/main.js %s \"%s\" %s",
     $Params->{gramname}, $userresponse, $main::sessionid;
     my($success, $error_message, $full_buf, $stdout_buf, $stderr_buf) =
     run(command => $cmd, verbose => 0);

     $message = join '', @$stdout_buf;
     $bot_state = parse_json($message)->{bot_state};
     $bot_msg = parse_json($message)->{bot_msg};
     while (!$LexFinished{$bot_state}) {
     LogRecogEvent ( $main::session,
        undef,
        $main::sessionid,
        $metaParams->{mask_logging},
        $Params->{gramname},
        $userresponse,
        'NOMATCH',
     '');
        VoiceXML::Client::Device::Dummy->play($bot_msg);
        VoiceXML::Client::Device::Dummy->emptyPromptBuffer();
        $userresponse = VoiceXML::Client::Device::Dummy->readnum();
        $cmd = sprintf "node /data/speechsoft/apps/entrAI/lib/NodeJS/main.js %s \"%s\" %s",
        $Params->{'gramname'}, $userresponse, $main::sessionid;
        my($success, $error_message, $full_buf, $stdout_buf, $stderr_buf) =
        run(command => $cmd, verbose => 0);

        $message = join '', @$stdout_buf;
        $bot_state = parse_json($message)->{bot_state};
        $bot_msg = parse_json($message)->{bot_msg};
     }
     my $tag = parse_json($message)->{tag};
     my $status = parse_json($message)->{match};
     my $slots = parse_json($message)->{slots};

     if ((scalar($LexBotErr{$bot_state}) == 0 ) and (defined $status) and ($status eq 'MATCH')) {
     Grammar::GrammarServer->updateTag($Params->{gramname}, $userresponse, $tag);
     LogRecogEvent($main::session,
                   undef,
                   $main::sessionid,
                   $metaParams->{mask_logging},
                   $Params->{gramname},
                   $userresponse,
                   'UPDATE',
                   $tag
                  );
     return builtInResponseWithSlots($origuserresponse, $tag, $slots, $metaParams);
   }
   else {
     my @emptyarray = ();
     return \@emptyarray;
   }

}

sub entrAI_NLU {
   #require 'C:/Users/7DQVN32/Documents/Projects/NLU/testNLU.pl';
   my($userresponse, $parameters, $metaParams) = @_;

   my $Params = parseParams ($parameters);

   if (!defined $Params->{'model'} ) {
      die "EntrAIGrams.pm: no model defined for NLU";
   }

   my $model = $Params->{'model'};
   my $interpretation = NLU::NLU::getIntent($userresponse, $model);
   if ($interpretation->{'match'} eq 'MATCH') {
     my $tag = $interpretation->{'tag'};
     my $slots = $interpretation->{'slots'};
     return builtInResponseWithSlots($userresponse, $tag, $slots);
   }
   else {
     my @emptyarray = ();
     return \@emptyarray;
   }

}


sub parseParams {
   my ($params) = @_;

   if (!defined $params) {
      return undef ;
   }
   my @Pairs = split (/\&/, $params);
   my $parameters = {};
   foreach my $nv_pair (@Pairs) {
        my ($name, $value) = split(/=/, $nv_pair);
        $parameters->{$name} = $value;
   }
   return $parameters;
}


#For custom coded grammars
#If matched, should return
sub entrAI_Custom {
   my($response, $parameters, $metaParams) = @_;

   my $answerTemplate =<<EOTEMPLATE;

     { 'tag' => '%s',
       'matched' => "%s "
     }
EOTEMPLATE

#Logic for custom grammar
=head
  my $tag = {...Code to compute tag...}

  if (response is matched) {
     my $answer = sprintf $answerTemplate, $tag, $response;
     my @answer = (eval($answer));
     return \@answer;
  } else

  {
     my @emptyarray = ();
     return \@emptyarray;
   }
=cut
}


sub builtInResponse {
  my ($input, $tag) = @_;

  my $answerTemplate =<<EOTEMPLATE;

     { 'tag' => 'out.MEANING="%s"; __textArray.push("%s");',
       'matched' => "%s"
     }

EOTEMPLATE

  my $answer = sprintf $answerTemplate, $tag, $input, $input;

  my @answer = (eval($answer));

  return \@answer;
 
}

sub nullResponse {
  my ($input) = @_;

  my $answerTemplate =<<EOTEMPLATE;

     { 'tag' => 'out.MEANING="%s";',
       'matched' => "%s"
     }

EOTEMPLATE

  my $answer = sprintf $answerTemplate, $input, $input;

  my @answer = (eval($answer));

  return \@answer;
 
}

sub builtInDBResponse {
  my ($input, $tag, $metaParams) = @_;

  my $answerTemplate =<<EOTEMPLATE;

     { 
       'matched' => "%s",
       'tag' => 'out.MEANING="%s"; __textArray.push("%s");'
     }

EOTEMPLATE

  my $answer = sprintf $answerTemplate, $input, $tag, $input;
  my @answer = (eval($answer));

  return \@answer;
 
}

sub builtInResponseWithSlots {
  my ($input, $intent, $slots, $metaParams) = @_;

  my $answerTemplate =<<EOTEMPLATE;

     { 'tag' => '%s ',
       'matched' => '%s '
     }

EOTEMPLATE
  my $tag = sprintf 'out.MEANING="%s"; __textArray.push("%s");', $intent, $input;

  foreach my $slot (keys %$slots) {
     if (defined $slots->{$slot}) {
       $tag = sprintf '%sout.%s="%s";', $tag, $slot, $slots->{$slot};
     }
  }


  my $answer = sprintf $answerTemplate, $tag, $input;
  my @answer = (eval($answer));

  return \@answer;
 
}




1;
