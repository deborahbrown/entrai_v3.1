package Grammar::GrammarInterpreter;
use XML::Parser;       
use strict;
use WWW::Scripter;
use Misc::Utilities;
use Grammar::EntrAIGrams;
use NLU::NLU;
use JSON::Create 'create_json';
use Data::Dumper;

my $initTagTemplate =<<TEMPLATE;

var out = { };
var __textIdx = 0;
var _starttxt;
out.__priority=0;
var _sessionVariables = {};
var SWI_decoy=0; var SWI_disallow=0; var SWI_vars = {};
var __meta_current = {text: '%s', score: 100};
var __meta_latest = {text: '', score: 100};
var meta = {
           current: function () { return __meta_current;},
           latest: function () { return __meta_latest;}
};
var rules = { latest: function (r) 
      { 
        if (typeof r == 'undefined') { return(this._latest);}
      
        else {this._latest = r; return r;}
      }, 
       
       _latest: ''
} ; 

var __grmState = {
     stateArray: [],
     push: function (v) {
         this.stateArray.push(v);
     },
     pop: function () {
         return this.stateArray.pop();
     }
} 

var __textArray = {
     textArray: [],
     push: function (v) {
         this.textArray.push(v);
     },
     pop: function () {
         return this.textArray.pop();
     }
} 


var __textIdxArray = {
     idxArray: [],
     push: function (v) {
         this.idxArray.push(v);
         __textIdx = __textIdx+1;
     },
     pop: function () {
         return this.idxArray.pop();
     }
} 



     
TEMPLATE


my $beginRuleTagTemplate =<<TEMPLATE;
__meta_current.text = '%s';
__rule_r = function () {
__grmState.push(out);
__textIdxArray.push(__textIdx);

out={};
TEMPLATE

my $endRuleTagTemplate =<<TEMPLATE;
    if (!out.__priority) {out.__priority=1;}
    _starttxt = __textIdxArray.pop();
    var tmpArray = __textArray.textArray.slice(_starttxt);
    __TEXT__ = tmpArray.join(' ');
    __TEXT__ = __TEXT__.replace(/^\\s+/, '');
    __TEXT__ = __TEXT__.replace(/\\s+\$/, '');
    __TEXT__ = __TEXT__.replace(/\\s+/g, ' ');
     __meta_latest=out;
    rules.latest(out);
    rules.%s = out;
    meta.%s = out;
    __meta_latest.text=__TEXT__;
    __meta_latest.score=100;
    meta.%s.text=__TEXT__;
    meta.%s.score=100;
    var tmp=out;
    out = __grmState.pop();
    delete __TEXT__;
    return tmp;
}
__rule_r();

TEMPLATE

my $endTagTemplate =<<TEMPLATE;
var __nBestStr = '';
delete out.__priority;
for (var __key in out) {
    var __value = out[__key];
    __nBestStr = __nBestStr + '+' + __key + ':' + __value;
}
__nBestStr;
TEMPLATE


####################################################################





sub convertToGrammarResult {
     my ($self, $grammarname, $input, $fieldtoset, $value) = @_;
  
     my $recoObj;
     if ($grammarname =~ /^_builtin/) {
        $recoObj = convertBuiltin($self, $grammarname, $input, $fieldtoset, $value);
     }
   

     elsif (!defined $fieldtoset or !defined $value) {
         
          $recoObj = create_json ( {
               grammar => $grammarname,
               match => 'MATCH',
               user_input => $input,
               confidence => '1.0',
               utterance => $input,
               inputmode => 'voice',
               interpretation => $input,
               triggeredgrammar => $grammarname
            });
     }
     else {
         

         $recoObj = create_json( {
             grammar => $grammarname,
             match => 'MATCH',
             user_input => $input,
             confidence => '1.0',
             utterance => $input,
             inputmode => 'voice',
             interpretation => {$fieldtoset => $value},
             triggeredgrammar => $grammarname
          });
         
     }
     LogUserResponses($self, $main::session, $main::EntraiConfig, $recoObj, $main::session->{lastinput}, $grammarname, undef, undef);
     return $recoObj;
}

sub convertToNomatchResult {
     my ($self, $grammarname, $input) = @_;

     my $recoObj = { status => 'NOMATCH' };
     LogUserResponses($self, $main::session, $main::EntraiConfig, $recoObj,  $main::session->{lastinput}, $grammarname, undef, undef);
     return $recoObj;
}

sub convertToNotFoundResult {
     my ($self, $grammarname, $input) = @_;

     my $recoObj = { status => 'GRMNOTFND' };
     LogUserResponses($self, $main::session, $main::EntraiConfig, $recoObj,  $main::session->{lastinput}, $grammarname, undef, undef);
     $recoObj = { status => 'NOMATCH' };
     return $recoObj;
}
      
        
###### PARSING XML #######
        

sub GrammarFileInterpreter {
   my ($self, $grammarname, $response, $gramtype, $parsetype) = @_;

   if ($gramtype eq 'inline') {
    my $RecoObj = GrammarInterpreter($grammarname, $response, "inline");
    return $RecoObj;
   }

   my $fh;
   my $grammar = do {
     local $/ = undef;
     if ((-e "$grammarname") and (open $fh, "<", "$grammarname")) {                             
       <$fh>;
     } else {
              return convertToNotFoundResult($self, $grammarname, $main::session->{lastinput}, $gramtype);
          }

   };
   close $fh;
   return $self->GrammarInterpreter( $grammar, $response, $grammarname);
}


sub GrammarInterpreter {
   my $self = shift;
   my $grammar = shift;
   my $response = shift;
   my $grammarname = shift || "inline_grammar";
   my $parsetype = shift;

   $response = Misc::Utilities->normalizeInput($response);

   # create a new object
   my $xmlDoc = new XML::Parser(Style => 'Tree'); 


   # init the doc from an XML string
   my $xmlHash = $xmlDoc->parse($grammar);
   my $metaParams = getMetaParams (@$xmlHash[1]);
   if (defined $parsetype) {
      $metaParams->{parsetype} = $parsetype;
   }
   $metaParams->{gramfile} = $grammarname;
   
   my $match = match_Grammar($response, @$xmlHash[1], undef, undef, $metaParams);
   if (scalar(@$match) == 0 ) {
         my $recoObj = {
              'status' => 'NOMATCH',
              'tag' => '',
               'results' => create_json ( {
                   grammar => $grammarname,
                   match => 'NOMATCH',
                   process_user_input => '',
                   processed_match_option => [ ],
                   user_input => '',        
                   confidence => '1.0',
                   utterance => '',
                   inputmode => 'voice',
                   interpretation => '',
                   triggeredgrammar => ''
                 }
                )
             };
 
         LogUserResponses($self, $main::session, $main::EntraiConfig, $recoObj, $main::session->{lastinput}, $grammarname, $metaParams->{mask_logging},
      $metaParams->{altgramname});
         return $recoObj;
   }

   if (!defined $parsetype and $metaParams->{parsetype} eq 'fuzzy') {


      my $package;
      if (defined $main::session->{ConfigParams}->{GrammarDir}) {
         $package = sprintf "%s/Packages/%s", $main::session->{ConfigParams}->{GrammarDir}, $metaParams->{package};
      }
      else {
        $package = "./Packages/default_package";
      }
      my $result = NLU::NLU::getFuzzyMatch($response, $package, $match); 
      if ($result->{match} eq 'NOMATCH') {
             my $recoObj =  NoMatchResult($grammarname, $main::session->{lastinput});
             LogUserResponses($self, $main::session, $main::EntraiConfig, $recoObj, $main::session->{lastinput}, $grammarname, $metaParams->{mask_logging},
      $metaParams->{altgramname});
             return $recoObj;
      }
      my $nbestindices = $result->{nbest};
      my $bestresponsematch = $match->[$nbestindices->[0]]->{'matched'};

      return GrammarInterpreter($self,$grammar, $bestresponsematch, $grammarname, 'exact'); 


   }


   my $max__priority=0;
   for (my $i=0; $i< scalar(@$match); $i++) {  

     next if $metaParams->{parsetype} eq 'exact' and  $response ne Misc::Utilities->normalizeInput(@$match[$i]->{'matched'});
     my $tag;

     if ($metaParams->{parsetype} eq 'exact') {
       $tag =  @$match[$i]->{'tag'};
       $tag =~ s/\\'/'/g;
     }
      
     my $w;
     $w = new WWW::Scripter;
     $w->use_plugin('JavaScript');

     $w->eval($tag);  


     #Added 4/9/2018
     next if $w->eval("SWI_disallow");
     #Added 4/13/2018
     next if $w->eval('out.__priority') <= $max__priority;
     $max__priority = $w->eval('out.__priority');  
     if ($w->eval("SWI_decoy")   ) {
        my $recoObj = NoMatchResult($grammarname, $response);
        LogUserResponses($self, $main::session, $main::EntraiConfig, $recoObj, $main::session->{lastinput}, $grammarname, $metaParams->{mask_logging},
      $metaParams->{altgramname});
        return $recoObj;
     } 

     my $interpretation = getInterpretation($w);
     setSessionVariables($w);

     my $temp = create_json ( {
           grammar => $grammarname,
           match => 'MATCH',
           match_option => $response,
           process_user_input => $response,
           processed_match_option => [ ],
           user_input => $response,
           confidence => '1.0',
           utterance => $response,
           inputmode => 'voice',
           interpretation => $interpretation,
           triggeredgrammar => $grammarname
        });

     my $recoObj = {
              'status' => 'MATCH',
              'results' => $temp
      };
      my @kv_pairs = split(/\+/, $interpretation);
      shift @kv_pairs;
      foreach my $kv (@kv_pairs) {
          my($key, $value) = split(':', $kv);
          next if $key eq '' or $key eq 'id' or $key eq 'grammarnum' or $key eq 'grammar';
          $recoObj->{'interpretation'}->{$key} = $value;
      }
      LogUserResponses($self, $main::session, $main::EntraiConfig, $recoObj, $main::session->{lastinput}, $grammarname, $metaParams->{mask_logging},
      $metaParams->{altgramname});
      return $recoObj;
   }
   my $recoObj = NoMatchResult($grammarname, $main::session->{lastinput});
   LogUserResponses($self, $main::session, $main::EntraiConfig, $recoObj, $main::session->{lastinput}, $grammarname, $metaParams->{mask_logging},
      $metaParams->{altgramname});
   return $recoObj;
}

sub NoMatchResult {

   my($grammarname, $response) = @_;
   my $temp = create_json ( {
         grammar => $grammarname,
         match => 'NOMATCH',
         match_option => '',
         process_user_input => '',
         user_input => $response,
         confidence => '1.0',
         utterance => $response,
         inputmode => 'voice',
         interpretation=> '',
         triggeredgrammar => ''
     });
   my $recoObj = {
              'status' => 'NOMATCH',
              'results' => $temp
   };

   return $recoObj;

}
 


my %Rules;


sub match_Grammar {
   my($response, $grammar, $isexternalflg, $externalroot, $metaParams) = @_;
   my @parses;


   #Find grammar root rule
   my $rootname;
   if ((!defined $isexternalflg) || (! defined $externalroot) || ($externalroot eq '' )) {
     $rootname = @$grammar[0]->{'root'};
   }
   else {
     $rootname = $externalroot;
   }

   #Determine root rule; by default it is first rule
   my $rootrule;
   for (my $i=1; $i<scalar(@$grammar); $i = $i+2) {
        next if @$grammar[$i] ne 'rule';
        my $rule = @$grammar[$i+1];

        my $id = @$rule[0]->{'id'};
        $Rules{$id} = $rule;
        if ($i == 1) {
           $rootrule = $rule;
           next;
        }
        if (defined ($rootname) && $id eq $rootname) {
          $rootrule = $rule;
        }
   }
  my $answers = &match_Rule(Misc::Utilities->normalizeInput($response), $rootrule,0, $metaParams);
  foreach my $ans (@$answers) {
      if (!defined $isexternalflg) {
        my $initTagT= sprintf $initTagTemplate, $response;
        $ans->{'tag'} = sprintf "%s\n%s\n%s", $initTagT, $ans->{'tag'}, $endTagTemplate;
      }
     $ans->{'matched'} = Misc::Utilities->normalizeInput($ans->{'matched'});
  }
  return $answers;
 
}


sub match_Rule {
   my($response, $rule, $depth, $metaParams) = @_;

   my $ruleid = @$rule[0]->{'id'};

   my $element = @$rule[3];

   if ($element eq 'item') {
      my $r =  match_Item($response, @$rule[4], $depth, $metaParams);
      return $r;
   }
   elsif ($element eq 'one-of') {
       my $r = match_Oneof($response, @$rule[4], $depth, $metaParams);
      if ($metaParams->{parsetype} eq 'exact') {
        foreach my $ans (@$r) {
           $ans->{'tag'} = sprintf "%s ", $ans->{'tag'};
        }
      }

      return $r;
   }
   else {
       die "Can't parse rule";
   }
}



sub match_Item {
   my($response, $item, $depth, $metaParams) = @_;
   $response = Misc::Utilities->normalizeInput($response);

   my $item_list_sz = scalar(@$item);
   if ((@$item[$item_list_sz - 2] eq '0') and (@$item[$item_list_sz - 1] =~ /^\s*$/)) {
     pop(@$item);
     pop(@$item);
   }
   my ($minrepeat, $maxrepeat);

   if (!defined($item->[0]->{'repeat'})) {
      $minrepeat=1;
      $maxrepeat=1;
   }
   else {
     my $repeat = $item->[0]->{'repeat'} ;
     ($minrepeat, $maxrepeat) = ($repeat =~ /\s*(\d+)\-(\d+)/);
   }


   my @parses = ();
   my $matches = match_ItemElements($response, $item,1, $depth, $metaParams);

   if (scalar($minrepeat) <= 1) {
        push(@parses,@$matches);
   }


   for (my $i =  2; $i <= $maxrepeat; $i++) {

      my $save_response = $response;

      last if scalar(@$matches) == 0;

      for (my $j = 0; $j < scalar(@$matches); $j++) {
        my $mr = Misc::Utilities->normalizeInput( @$matches[$j]->{'matched'});
        my $mt = @$matches[$j]->{'tag'};
        $save_response =~ s/^\s*$mr\s*//;

        $matches = match_ItemElements($save_response, $item,1, $depth, $metaParams);

        foreach my $m (@$matches) {
          $m->{'matched'} = join(' ', $mr, $m->{'matched'});
          if ($metaParams->{parsetype} eq 'exact' ) {
            $m->{'tag'} = join(' ',  $mt, $m->{'tag'});
          }
        }
        if ($i >= $minrepeat) {
          push(@parses, @$matches);
        }
     }
   }


   return \@parses;
   
}


sub match_ItemElements {
   my($response, $itemElements, $index, $depth, $metaParams) = @_;
   $response = Misc::Utilities->normalizeInput($response);

  
   if ($index >= scalar(@$itemElements) ) { #end of items

      if ( (scalar($depth) == 0) && ($response !~ /^\s*$/)) {
         my @parses = ();
        return \@parses;
      }

      my @parses = ({'tag' => '', 'matched' => ''});
      return \@parses;
   }
   
   my $element_type = @$itemElements[$index];

   if ($element_type eq '0') { #next element is a leaf node

     my $contents = Misc::Utilities->normalizeInput(@$itemElements[$index+1]);

     if ( $metaParams->{parsetype} eq 'fuzzy'  or !defined($response) or ($response =~ /^$contents/) ) {

          my $unmatched_response;

          if (!defined $response) { $unmatched_response =''; }
          else {
          ($unmatched_response = $response) =~ s/$contents//;
          }
          my $matches = &match_ItemElements($unmatched_response, $itemElements, $index+2,  $depth, $metaParams);
          my @parses = ();

          for (my $i=0; $i < scalar(@$matches) ; $i = $i+1) {
             my $newtag = '';
             if ($metaParams->{parsetype} eq 'exact') {
               $newtag = sprintf "%s __textArray.push('%s'); __textIdx = __textIdx+1
; ", @$matches[$i]->{'tag'}, $contents;
             }

                                                        
             $parses[$i] = { 'tag' => $newtag,  'matched' => sprintf( "%s %s", $contents, @$matches[$i]->{'matched'})};
          }
          return \@parses;

     }

     else {
          my @parses = ();
          return \@parses;
     }

   }
   elsif ($element_type eq 'item') {

       my @parses = ();
       my ($minrepeat, $maxrepeat);

      my $item = @$itemElements[$index+1];
      if (!defined($item->[0]->{'repeat'})) {
         $minrepeat=1;
         $maxrepeat=1;
      }
      else {
        my $repeat = $item->[0]->{'repeat'} ;
        ($minrepeat, $maxrepeat) = ($repeat =~ /\s*(\d+)\-(\d+)/);
      }

       if ($minrepeat == 0 ) {
          my $matched_response = &match_ItemElements($response, $itemElements, $index+2, $depth, $metaParams);
          push(@parses, @$matched_response);
       }
      
       my $matched_response = &match_Item($response, @$itemElements[$index+1], $depth+1, $metaParams);
      
       foreach my $mr (@$matched_response) {
          my $mr1 = Misc::Utilities->normalizeInput($mr->{'matched'});
          
          my ($unmatched_response) = ($response =~ /$mr1\b(.*)/);

          my $matches = &match_ItemElements($unmatched_response, $itemElements, $index+2, $depth, $metaParams);

          foreach my $m (@$matches) {
              my $newtag = '';
              if ($metaParams->{parsetype} eq 'exact') {
               $newtag = sprintf "%s %s", $mr->{'tag'}, $m->{'tag'};
              }
              push(@parses, {'tag' => $newtag, 'matched' => sprintf ("%s %s", $mr1, $m->{'matched'})});
          }
       }

       return \@parses;
   }
   elsif ($element_type eq 'ruleref') {


      my @parses = ();
      my $ruleref = @$itemElements[$index+1];
      my $ruleid;
      if (defined @$ruleref[0]->{'uri'}) {
        $ruleid = @$ruleref[0]->{'uri'};
      }
      else {
        $ruleid = sprintf "_entrai_:%s", @$ruleref[0]->{'special'};
      }
      
      my $matched_response;
      if (! ($ruleid =~ /^#/) and ! ($ruleid =~ /^_entrai_:/) ){
          $matched_response = match_ExternalRule($response, $ruleid, $metaParams);
      }
      else {

        $ruleid =~ s/^#//;
        if (! (defined $Rules{$ruleid} ) and ($ruleid !~ /^_entrai_/) ){

           return \@parses;
        }

      

        if ($ruleid =~ /^_entrai_/ ) {
           $matched_response =  Grammar::EntrAIGrams->match_Special($ruleid, $response, $metaParams);

        }

        else {
          $matched_response = &match_Rule($response, $Rules{$ruleid}, $depth+1, $metaParams);
        }
      }

      foreach my $mr (@$matched_response) {

          my $mrr = Misc::Utilities->normalizeInput($mr->{'matched'});
          my ($unmatched_response) = ($response =~ /$mrr\b(.*)/);
          my $matches = &match_ItemElements($unmatched_response, $itemElements, $index+2, $depth, $metaParams);
          foreach my $m (@$matches) {
             my $t = $m->{'matched'};
             my $t2 = $m->{'tag'};

            my $barerule;
            if ($ruleid =~ /#/) {
               ($barerule ) = ($ruleid =~ /#([^\s]+)/);
            }
            else {
              ($barerule = $ruleid) =~ s/.*://;
              $barerule =~ s/\?.*//;
              $barerule =~ s/[^\w]/_/g;
            }

            my $endrulestr = sprintf($endRuleTagTemplate, $barerule, $barerule, $barerule, $barerule);
            my $beginR = sprintf $beginRuleTagTemplate, $response;
            if ($metaParams->{parsetype} eq 'exact' ) {
              push(@parses, {'tag' => sprintf ("%s %s %s %s", $beginR, 
                                    $mr->{'tag'}, 
                                    $endrulestr,
                                    $t2
                                     ),
                  'matched' => sprintf ("%s $t", $mrr)});
             } else {
              push(@parses, {'tag' => '',
                  'matched' => sprintf ("%s $t", $mrr)});
             }
          }
       }

       return \@parses;
  }
   elsif ($element_type eq 'one-of') {
      my @parses = ();
      my $matched_response = &match_Oneof($response, @$itemElements[$index+1], $depth+1, $metaParams);

      foreach my $mr (@$matched_response) {
          my $mr1 = Misc::Utilities->normalizeInput($mr->{'matched'});
          my ($unmatched_response) = ($response =~ /$mr1\b(.*)/);
          my $matches = &match_ItemElements($unmatched_response, $itemElements, $index+2, $depth+1, $metaParams);         
          foreach my $m (@$matches) {
             my $newtag = '';
             if ( $metaParams->{parsetype} eq 'exact' ) {
                $newtag = sprintf "%s %s", $mr->{'tag'}, $m->{'tag'};
             }
             push(@parses, {'tag' => $newtag, 'matched' => sprintf ("%s %s", $mr1, $m->{'matched'})});
          }
       }
       return \@parses;
   }
   elsif ($element_type eq 'tag') {

      my @parses = ();
      my $tag =  @$itemElements[$index+1]->[2];

      my $matches = &match_ItemElements($response, $itemElements, $index+2, $depth, $metaParams);

      foreach my $m (@$matches) {
        if ($m->{'tag'} !~ /^\s*$/ and $metaParams->{parsetype} eq 'exact') {
          push(@parses, {'tag'=> sprintf ("%s %s", $tag, $m->{'tag'}), 'matched' => $m->{'matched'}} );
        }
        else {
          push(@parses, {'tag'=> $tag, 'matched' => $m->{'matched'}} );
        }
       }

      return \@parses;

   }

}


sub match_Oneof {
   my($response, $oneof,  $depth, $metaParams) = @_;
   $response = Misc::Utilities->normalizeInput($response);
   my @parses = ();

   for (my $i=1; $i<scalar(@$oneof); $i = $i+2) {
        next if @$oneof[$i] ne 'item';
        my $matches = &match_Item($response, @$oneof[$i+1], $depth+1, $metaParams);
        push(@parses, @$matches);
   }
   return \@parses;
}







sub getInterpretation {
   my ($JSEnvironment) = @_;

   my $expr = <<EXPR;
   __getInterpretation = function() {
       var interpretation='';
       for (var key in out) {
          if (typeof(out[key]) != "undefined" ) {
          interpretation = interpretation + '+' + key +':' + out[key];
          }
       }
       return interpretation;
   }
  __getInterpretation();
EXPR


  return $JSEnvironment->eval($expr);
}

sub setSessionVariables {
   my ($JSEnvironment) = @_;

   my $expr = <<EXPR;
   __getVariables = function() {
       var Variables ='';
       for (var key in _sessionVariables) {
          if (typeof(_sessionVariables[key]) != "undefined" ) {
          Variables = Variables + '#' + key +':' + _sessionVariables[key];
          }
       }
       return Variables;
   }
  __getVariables();
EXPR


  my $varList = $JSEnvironment->eval($expr);
  my @varList = split('#', $varList);
  foreach my $nvpair (@varList) {
    my ($name, $value) = ($nvpair =~ /([^:]+):([^:]+)/);
    next if !defined $name;
    next if !defined $value;
    $main::session->setSessionVariable( $name, $value);
  }
}


sub convertBuiltin {

   my($self, $builtin, $response, $fieldtoset, $value) = @_;

   my  ($grammar, $args) = ( $builtin =~ /builtin_([^\?]+)\?*([^\?]*)/ );

   if ($grammar eq 'digits' or $grammar eq "_digits") {
       (my $nospace_response = $response) =~ s/\s+//g;
       my $recoObj;
       if ($nospace_response !~ /^\d+$/) {
            $recoObj= convertToNomatchResult($self, $grammar, $main::session->{lastinput}, 'builtin');
       }
       if (! defined $args) {
            $recoObj= convertToGrammarResult($self, $grammar, $main::session->{lastinput}, $fieldtoset, $value);
       }
       my ($diglen) = ($args =~ /length=([0-9]+)/ );
       if (length($nospace_response) != $diglen) {
            $recoObj =  convertToNomatchResult($self, $grammar, $main::session->{lastinput}, 'builtin');
       }
       my ($minlen) = ($args =~ /minlen=([0-9]+)/ );
       if (defined($minlen) and length($nospace_response) < $minlen) {
            $recoObj =  convertToNomatchResult($self, $grammar, $main::session->{lastinput},  'builtin');
       }
       my ($maxlen) = ($args =~ /maxlen=([0-9]+)/ );
       if (defined ($maxlen) and length($nospace_response) > $diglen) {
            $recoObj =  convertToNomatchResult($self, $grammar, $main::session->{lastinput}, 'builtin');
       }
       else {
         $recoObj =  convertToGrammarResult($self, $grammar, $main::session->{lastinput}, $fieldtoset, $value);
       }

       return $recoObj;

   }

   else  { #Generic handling for builtins
       my $language = $main::session->{"ConfigParams"}->{'language'};
       my $recoObj =  $self->GrammarFileInterpreter("./Languages/$language/Grammars/entrAI_inline_$grammar.grxml", $response); 
       LogUserResponses($self, $main::session, $main::EntraiConfig, $recoObj, $main::session->{lastinput}, $grammar, undef, undef);
       return $recoObj;

   }

}

sub match_ExternalRule {
   my($response, $uri, $metaParams) = @_;

   my $gramdir = $main::ConfigParams->{GrammarDir} ? $main::ConfigParams->{GrammarDir}  : '.';
 
   my ($grammarname, $t, $externalroot) = ( $uri =~ /([^#]+)(#(.+))*/);
   $grammarname = "$gramdir/$grammarname";


   my $fh;
   my $grammar = do {
     local $/ = undef;
     if (open $fh, "<", "$grammarname") {                             
       <$fh>;
     } else {
         LogEvent(2, "GRAMNOTFND", 'entrai', $grammarname);
              return [] ;
          }

   };
   close $fh;


   $response = Misc::Utilities->normalizeInput($response);

   # create a new object
   my $xmlDoc = new XML::Parser(Style => 'Tree'); 

   # init the doc from an XML string
   my $xmlHash = $xmlDoc->parse($grammar);

   my $match = match_Grammar($response, @$xmlHash[1], 1, $externalroot, $metaParams);
   return $match;
}


# getMetaParams - takes a parsed grammar (XML Doc) (an array from xmlHash)
#                 returns an object with the names and values of each meta parameter in the grammar
sub getMetaParams { 
   my ($docarray) = @_;

   #set defaults
   my $Params = {
         parsetype => 'exact'
   };
   # now read values from grammar
   for(my $i=0; $i<scalar(@$docarray); $i++) {
      next if $docarray->[$i] ne 'meta';
      $i++;                               #found a meta element; now look at its definition (next element in list)
      my $metadefn = $docarray->[$i]->[0];
      $Params->{$metadefn->{name}} = $metadefn->{content};
   }

   return $Params;
}

sub LogUserResponses {
    my ($self, $session, $entraiConfig, $RecoObj, $input, $grammar, $maskflag, $altgramname) = @_;


      if (defined $altgramname) {$grammar = $altgramname; }
      if (!defined $maskflag) {$maskflag = $main::session->{ConfigParams}->{builtin_mask_logging} };
      if ($RecoObj->{'status'} eq 'MATCH' ) {

           my $interpretation = '';
           foreach my $tag (keys %{$RecoObj->{'interpretation'}}) {
                  next if ! defined $RecoObj->{'interpretation'}->{$tag};
                  $interpretation = sprintf  "%s+%s=%s", $interpretation, $tag, $RecoObj->{'interpretation'}->{$tag};
           }
           LogRecogEvent($session, $entraiConfig, $main::sessionid, $maskflag, $grammar, $input, 
               $RecoObj->{'status'}, $interpretation);
           return;

       } else {
           LogRecogEvent($session, $entraiConfig, $main::sessionid, $maskflag, $grammar, $input,  $RecoObj->{'status'},
           '');
           return;

       }
}

   
1;
