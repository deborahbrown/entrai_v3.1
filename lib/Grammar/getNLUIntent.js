//This is the code residing on Amazon Lambda for interacting with Amazon Lex
'use strict';
console.log('Loading getNLUIntent');
 
exports.handler = function(event, context, callback) {
    let grammar = "DemoCardNum";
    let userresp = '12345';
    let responseCode = 200;
    console.log("request: " + JSON.stringify(event));
    
   // get grammar name and user response
   if (event.body !== null && event.body !== undefined) {
        let body = JSON.parse(event.body);
        if (body.userresp) {
            userresp = body.userresp;
            console.log("Received user response: " + body.userresp);
        }
        
        if (body.grammar) {
            grammar = body.grammar;
            console.log("Received user response: " + body.grammar);
        }
    }


  
    var AWS = require ('aws-sdk');

    AWS.config.region = 'us-east-1'; // Region
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
     IdentityPoolId: 'us-east-1:1c27adda-368a-4bc2-a1fa-0d96d39b2d14',
    });
    
    
	var lexruntime = new AWS.LexRuntime();
	var lexUserId = 'chatbot-demo' + Date.now();
	var sessionAttributes = {};

    
    		function pushChat() {

			
			// send it to the Lex runtime
                var params = {
					botAlias: '$LATEST',
					botName: grammar,
					inputText: userresp,
					userId: lexUserId,
					sessionAttributes: sessionAttributes
				};
				var responseBody;
				lexruntime.postText(params, function(err, data) {
					if (err) {
						console.log(err, err.stack);
						console.log('Error:  ' + err.message + ' (see console for details)');
						 responseBody = {
                              grammar: grammar,
                              match: 'NOMATCH'
                          };
					}
					if (data) {
						// capture the sessionAttributes for the next cycle
						sessionAttributes = data.sessionAttributes;
						// show response and/or error/dialog status
						console.log("The intent is: " + data.intentName);
						if (data.intentName)
						  responseBody = {
                              tag: data.intentName,
                              grammar: grammar,
                              match: 'MATCH',
                              slots: data.slots
                          };
                        else
                          responseBody = {
                              grammar: grammar,
                              match: 'NOMATCH'
                          };
                
 
                      var response = {
                       statusCode: responseCode,
                       headers: {
                           "x-custom-header" : "my custom header value"
                       },
                       body: JSON.stringify(responseBody)
                       };
                       console.log("response: " + JSON.stringify(response));
                       callback(null, response);
					}

				});
                                


			return false;
		}
		
	pushChat();

   
};