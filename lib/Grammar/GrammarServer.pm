package Grammar::GrammarServer ;

use strict;
use DBI;
use Misc::Utilities;
use Data::Dumper;


sub ConnectToPromptDB {
  
#Open database 
#my $dbconnstr = $VoiceXML::Client::UserAgent::UserAgentParams->{'DBConnStr'};
my ($dbconnstr) = @_;

$DBI::dbh = DBI->connect ($dbconnstr, "", "", {
        RaiseError => 1,
        }) or die "Cannot connect: $DBI::errstr";

return $DBI::dbh;
}




sub updateTag {
    my ($self,$grammar, $userinput, $interpretation) = @_;

    return if ! $main::ConfigParams->{enableLearning};
    $grammar =~ s/^_remote_//;
    my $dbh = $Misc::Utilities::dbh;

    
    my $query = sprintf "REPLACE INTO INTERPRETATIONS (grammar, input, interpretation, verified ) VALUES (%s, %s, %s, 'N')",
        $dbh->quote($grammar), $dbh->quote($userinput), $dbh->quote($interpretation);
    $dbh->do($query);
    LogEvent(3, "GRAMUPD",  'entrai', $grammar, $userinput, $interpretation);


}



sub retrieveInterpretation {

    my ($self, $userinput, $grammarname) = @_;
printf STDERR "In retrieveInterpretation\n";
printf STDERR "grammar is $grammarname\ninput is $userinput\n";
    #my $normalized = Misc::Utilities->normalizeInput($userinput);
    my $interpretation;
    my %ReturnTag;
    my $dbh = $Misc::Utilities::dbh;

    my $processed_match_options = '[ ]';
    my $processed_input = sprintf '\["%s"\]', $userinput;

    my $query = "SELECT * FROM INTERPRETATIONS WHERE grammar = '$grammarname' and input='$userinput';";
    
    my $sth   = $dbh->prepare ($query);
       $sth->execute ();
       my $tag = "xxx_UNKNOWN_xx";
       while (my $row = $sth->fetchrow_hashref) {
          $tag = $row->{interpretation};
          last;
       }
    $sth->finish ();
printf STDERR "found tag $tag\n";
    if ($tag eq "xxx_UNKNOWN_xx") { 
        return {'status' => 'NOMATCH'};
    }
    return {'status' => 'MATCH', 'tag' => $tag } ;
}


sub retrievePromptText {

    my ( $self, $wav) = @_;

    my $dbconnstr = $VoiceXML::Client::UserAgent::UserAgentParams->{'DBConnStr'};
    my $dbh = &ConnectToPromptDB($dbconnstr);

    $wav =~ s/.wav$//;
    $wav =~ s/.*\///;


    my $query = sprintf "SELECT * FROM Prompts where wavname=%s", $dbh->quote($wav);
   
    my $sth   = $dbh->prepare ($query);
    $sth->execute ();
    my $text ; #
    while (my $row = $sth->fetchrow_hashref) {
        $text = $row->{wording};
        last;
    }
    $sth->finish ();
    #$dbh->disconnect();

    return($text);
}

sub getInterpretation {
   my ($interpretation) = @_;

   my $int_str = '';
   foreach my $key (keys %$interpretation) {
    next if ! defined $interpretation->{$key};
    $int_str = sprintf "%s+%s:%s", $int_str, $key, $interpretation->{$key};
   }

   return $int_str;

}






1;
