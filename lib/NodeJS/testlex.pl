use IPC::Cmd qw[can_run run ];

my $cmd = sprintf 'node /data/speechsoft/apps/entrAI/lib/NodeJS/main.js BookTrip "book a car" %s', 'chatbot-demo' . 'BBBBBBBBA' ;
my($success, $error_message, $full_buf, $stdout_buf, $stderr_buf) = 

   run(command => $cmd, verbose => 0);

if ($success) {
   print "stdout is ",  join("", @$stdout_buf);
   print "\n\n";
   print "stderr is ",  join("", @$stderr_buf);
   print "\n";
}
