//This is the code residing on Amazon Lambda for interacting with Amazon Lex

    'use strict';
 
    const args = process.argv.slice(2);
    var grammar =  args[0];
    var userresp = args[1];
    var lexUserId = args[2];

    var fs = require('fs');
    var data =fs.readFileSync('/home/ssadmin/.aws/identity', "utf8");
   
     var region = JSON.parse(data).region;
     var pool = JSON.parse(data).pool ;
    var AWS = require ('aws-sdk');

    AWS.config.region = region; // Region
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
     IdentityPoolId: pool
    });
   
    
	var lexruntime = new AWS.LexRuntime();
	var sessionAttributes = {};

    
    		function pushChat() {

			
			// send it to the Lex runtime
                var params = {
					botAlias: '$LATEST',
					botName: grammar,
					inputText: userresp,
					userId: lexUserId,
					sessionAttributes: sessionAttributes
				};
				var responseBody;
				lexruntime.postText(params, function(err, data) {
					if (err) {
						process.stderr.write('Error:  ' + err.message + ' (see console for details)');
						 responseBody = {
                              grammar: grammar,
                              match: 'NOMATCH',
                              bot_state: 'Failed'
                          };
					}
					if (data) {
						// capture the sessionAttributes for the next cycle
						sessionAttributes = data.sessionAttributes;
						// show response and/or error/dialog status
						// process.stderr.write("The intent is: " + data.intentName);
			if (data.intentName)
						  responseBody = {
                              tag: data.intentName,
                              grammar: grammar,
                              match: 'MATCH',
                              bot_state: data.dialogState,
                              bot_msg: "",
                              slots: data.slots
                          };
                        else
                          responseBody = {
                              grammar: grammar,
                              match: 'NOMATCH',
                              bot_state: data.dialogState,
                              bot_msg: data.message,
                          };
                
 
                       process.stdout.write(JSON.stringify(responseBody));
                       process.stderr.write(JSON.stringify(data));
					}

				});
                                


			return false;
		}
		
	pushChat();

   
