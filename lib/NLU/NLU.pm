package NLU::NLU;
use NLU::wordstemmer;
use Storable;
use Data::Dumper;
use strict;
#use warnings;



sub new {
	my ($class, $modeltype) = @_;
	my $self = {};
     $modeltype = $modeltype || 'nlu';
        
      bless $self, ref $class || $class;
	$self->{NumUniqueWords} = {};  
        $self->{WordIndex} ={};
        $self->{TagCount} = {};
        $self->{TagWordProbVector} = {};
        $self->{TagWordMissingVector} = {};
        $self->{LogTagProbability} = {};
        $self->{excludedWords} = {};
        $self->{StemmerExceptions} = {};
        $self->{ClassDefinitions} = {};
        $self->{Params} = {};
        $self->{SentenceCount} = 0;
        $self->{modeltype} = $modeltype;
	return $self;
}


my %Score;


sub getBest {
   my ($model, $sentence) = @_;

   my $vector  = {};
   my $WordIndex = $model->{'WordIndex'};
   my $TagCount = $model->{'TagCount'};
   my $TagWordProbVector= $model->{'TagWordProbVector'};
   my $TagWordMissingVector = $model->{'TagWordMissingVector'};

   my $sentencestems = getStems($sentence, $model); 
   my $numwordsintags=0;
   foreach my $word (keys %$sentencestems ) {
     if (defined $WordIndex->{$word}) {
       $vector->{$WordIndex->{$word}} = 1; 
       $numwordsintags++;
     }  
   }

   if ($numwordsintags == 0) {#none of words in sentence match any words in training set
        return { 'match' => 'NOMATCH'} ;
   }

   %Score = ();
   my $bestscore = undef;
   my $besttag;
   foreach my $tag (keys %$TagCount)  {
      my $score = computeScore($model, $vector, $tag);
      $Score{$tag} = $score;
      if (!defined $bestscore or $score >= $bestscore) {
          $bestscore = $score;
          $besttag=$tag;
      }
   }
   my @nbestlist = sort byscore (keys %$TagCount);

   my $slots;
   return {'tag' => $besttag, 'score' => $bestscore, 'nbest' => \@nbestlist, 
         'slots' => NLU::Normalization::getSlots($sentence, $model, $slots), 'match' => 'MATCH'} ;
}


sub byscore {
    $Score{$b} <=> $Score{$a};
}



sub computeScore { 
   my ($model, $sentencevector, $tag) = @_;
    
   my $TagWordProbVector = $model->{'TagWordProbVector'};
   my $TagWordMissingVector =  $model->{'TagWordMissingVector'};
   my $LogTagProbability = $model->{'LogTagProbability'};

   my $score = 0;
   for (my $i=0; $i< $model->{'NumUniqueWords'}; $i++){

     if (defined  $sentencevector->{$i}) { #the word in sentence 
         $score = $score + $TagWordProbVector->{$tag}->{$i};
     }
     else { #the word is in tag but not in sentence
         $score = $score + $TagWordMissingVector->{$tag}->{$i};
     }
   }
   $score = $score + $LogTagProbability->{$tag};
   return $score;
}




sub computeWordAndTagCounts { #tag and word counts
  my ($model, $corpus) = @_;


  my $TagCount;
  my $WordIndex;
  my $NumUniqueWords = 0;
  my $WordOccurrences;
  my $WordCount;
  my $SentenceCount=0;
  my $FullSentenceTradeOff = $model->{'Params'}->{'FullSentenceTradeOff'};

  open(CORPUS, $corpus);
  while(<CORPUS>) {
   s/\s+$//;
   my ($sentence, $tag, $count) = split(/,/);
   next if !defined $sentence;
   next if $sentence =~ /^\s*$/;
   
   $TagCount->{$tag} = $TagCount->{$tag} + (1-$FullSentenceTradeOff) * $count + $FullSentenceTradeOff;


   my %stems = %{getStems($sentence, $model)};
   foreach my $word (keys %stems) {
     if (!defined $WordIndex->{$word}) {
        $WordIndex->{$word} = $NumUniqueWords;
        $NumUniqueWords++;
     }

     $WordOccurrences->{$word, $tag} = $WordOccurrences->{$word, $tag} + (1-$FullSentenceTradeOff)*$count + $FullSentenceTradeOff;
     $WordCount->{$word} = $WordCount->{$word} + (1-$FullSentenceTradeOff)*$count + $FullSentenceTradeOff;
     $SentenceCount = $SentenceCount + (1-$FullSentenceTradeOff)*$count + $FullSentenceTradeOff;
  
   }
   
  }
  close CORPUS;
  $model->{'TagCount'} = $TagCount;
  $model->{'WordIndex'} = $WordIndex;
  $model->{'NumUniqueWords'} = $NumUniqueWords;
  $model->{'WordOccurrences'} = $WordOccurrences;
  $model->{'WordCount'} = $WordCount;
  $model->{'SentenceCount'} = $SentenceCount;
}


sub computeWordAndTagCountsFromGrammar { #tag and word counts
  my ($model, $list) = @_;
  my $TagCount;
  my $WordIndex;
  my $NumUniqueWords = 0;
  my $WordOccurrences;
  my $WordCount;
  my $SentenceCount=0;
  
  foreach my $item (@$list) {

   my $sentence = $item->{'matched'};
   my $tag = $SentenceCount;
   if (defined $TagCount->{$tag} ) {
      $TagCount->{$tag} = $TagCount->{$tag} + 1;
   } else
   {
      $TagCount->{$tag} = 1;
   }
   my %stems = %{getStems($sentence, $model)};
   foreach my $word (keys %stems) {
     if (!defined $WordIndex->{$word}) {
        $WordIndex->{$word} = $NumUniqueWords;
        $NumUniqueWords++;
     }

     $WordOccurrences->{$word, $tag} = 1;
     if (defined $WordCount->{$word} ) {
       $WordCount->{$word} = $WordCount->{$word} + 1;
     } else {
       $WordCount->{$word} = 1;
     }
 
   }
   $SentenceCount++;
   
  }
  $model->{'TagCount'} = $TagCount;
  $model->{'WordIndex'} = $WordIndex;
  $model->{'NumUniqueWords'} = $NumUniqueWords;
  $model->{'WordOccurrences'} = $WordOccurrences;
  $model->{'WordCount'} = $WordCount;
  $model->{'SentenceCount'} = $SentenceCount;
}



sub computeWordLikelihood { #Probability of a word, given a tag

 my ($model) = @_;
 my $TagCount = $model->{'TagCount'};
 my $WordIndex = $model->{'WordIndex'};
 my $WordOccurrences = $model->{'WordOccurrences'};
 my $TagWordMissingVector;
 my $TagWordProbVector;

 my $MINUS_INFINITY= $model->{'Params'}->{'MINUS_INFINITY'};

 foreach my $tag (keys %$TagCount) {
     foreach my $word (keys %$WordIndex) {
     if (!defined $WordOccurrences->{$word, $tag} ) {
            $TagWordProbVector->{$tag}->{$WordIndex->{$word}} = $MINUS_INFINITY;
     } 
     else {
      $TagWordProbVector->{$tag}->{$WordIndex->{$word}} = log( $WordOccurrences->{$word, $tag} / $TagCount->{$tag});
     }
     if ( $WordOccurrences->{$word, $tag} == $TagCount->{$tag} ) {
            $TagWordMissingVector->{$tag}->{$WordIndex->{$word}} = $MINUS_INFINITY;
     } 
     else {
      $TagWordMissingVector->{$tag}->{$WordIndex->{$word}} = log(1 - ($WordOccurrences->{$word, $tag} / $TagCount->{$tag}));
     }
     
    }

 }
  $model->{'TagWordMissingVector'} = $TagWordMissingVector;
  $model->{'TagWordProbVector'} = $TagWordProbVector;
}



sub computeWordLikelihoodFromGrammar { #Probability of a word, given a tag

 my ($model) = @_;
 my $TagCount = $model->{'TagCount'};
 my $WordIndex = $model->{'WordIndex'};
 my $WordOccurrences = $model->{'WordOccurrences'};
 my $TagWordMissingVector;
 my $TagWordProbVector;

 my $MINUS_INFINITY= $model->{'Params'}->{'MINUS_INFINITY'};

 foreach my $tag (keys %$TagCount) {
     foreach my $word (keys %$WordIndex) {
     if (!defined $WordOccurrences->{$word, $tag} ) {
            $TagWordProbVector->{$tag}->{$WordIndex->{$word}} = -1;
     } 
     else {
      $TagWordProbVector->{$tag}->{$WordIndex->{$word}} = 1;
     }
     if ( defined $WordOccurrences->{$word, $tag}  ) {
            $TagWordMissingVector->{$tag}->{$WordIndex->{$word}} = -1;
     } 
     else {
      $TagWordMissingVector->{$tag}->{$WordIndex->{$word}} = 1;
     }
     
    }

 }
  $model->{'TagWordMissingVector'} = $TagWordMissingVector;
  $model->{'TagWordProbVector'} = $TagWordProbVector;
}


sub calculateTagProb {
    my ($model) = @_;
    my $totalcount = 0;
    my $LogTagProbability;
    my $TagCount = $model->{'TagCount'};
    foreach my $tag (keys %$TagCount) {
       $totalcount = $totalcount + $TagCount->{$tag};
    }
    foreach my $tag (keys %$TagCount) {
       $LogTagProbability->{$tag} = log ($TagCount->{$tag}/$totalcount);

    }
    $model->{'LogTagProbability'} = $LogTagProbability;
}




sub trainModel {
   my ($package, $modelname, $modeltype) = @_;  
   if ($modeltype eq 'nlu') {
      return trainModelNLU($package, $modelname);
   }
}



sub trainModelNLU {
   my ($package, $modelname) = @_;

   my $corpus = sprintf "%s/training_corpus.csv", $package;
   my $wordExclusionList = sprintf "%s/wordstoeliminate.csv", $package;
   my $classDefinitions = sprintf "%s/class_definitions.csv", $package;
   my $stemmerExceptions = sprintf "%s/stemmer_exceptions.csv", $package;
   my $modelParams = sprintf "%s/params.csv", $package;

   my $model = new ('NLU::NLU', 'nlu');
   readModelParams($model,$modelParams);
   readWordExclusions($model, $wordExclusionList);
   readClassDefinitions($model, $classDefinitions);
   readStemmerExceptions($model, $stemmerExceptions);
   computeWordAndTagCounts($model, $corpus);
   calculateTagProb($model);
   computeWordLikelihood($model);
   return saveModel($model, $modelname);
}


sub getIntent {
  my($sentence, $modelfile) = @_;
  my $model = readModel($modelfile);

  return getBest($model, $sentence);
}


sub getFuzzyMatch {  #grammarlist is list of match/tag valuesl upon return t it reordered from best to worse match
   my ($sentence, $package, $grammarlist) = @_;  

   my $wordExclusionList = sprintf "%s/wordstoeliminate.csv", $package;
   my $classDefinitions = sprintf "%s/class_definitions.csv", $package;
   my $stemmerExceptions = sprintf "%s/stemmer_exceptions.csv", $package;
   my $modelParams = sprintf "%s/params.csv", $package;

   my $model = new ('NLU::NLU', 'fuzzy');

   readModelParams($model,$modelParams);
   readWordExclusions($model, $wordExclusionList);
   readClassDefinitions($model, $classDefinitions);
   readStemmerExceptions($model, $stemmerExceptions);
   computeWordAndTagCountsFromGrammar($model, $grammarlist);
   calculateTagProb($model);
   computeWordLikelihoodFromGrammar($model);
   return getBest($model, $sentence);
 

}



sub saveModel() {
 my ($model, $modelfile) = @_;

  if (defined $modelfile) {
    store $model, $modelfile;
  }
  return $model;
}

sub readModel {
  my($modelFile) = @_;
  return( retrieve $modelFile);
}

sub readModelParams {
  my ($model, $paramsfile) = @_;
  my $excludedWords;
  open(PARAMS, $paramsfile) or die "Can't open $paramsfile";
  while (<PARAMS> ) {
    chomp;
    my($param, $value) = split(/,/, $_, 2);
    $model->{Params}->{$param} = $value;
  }
  close PARAMS;
}

sub readWordExclusions {
  my ($model, $wordExclusionList) = @_;
  my $excludedWords;
  open(Exclusions, $wordExclusionList) or die "Can't open $wordExclusionList";
  while (<Exclusions> ) {
    chomp;
    my($word) = split(/,/, $_, 2);
    $excludedWords->{$word}++;
  }
  close Exclusions;
  $model->{'excludedWords'} = $excludedWords;
}

sub readClassDefinitions {
  my ($model, $classDefinitions) = @_;
  my $ClassDefinitions;
  open(Classes, $classDefinitions);
  while (<Classes> ) {
    chomp;
    my($phrase, $class) = split(/,/, $_, 2);
    $ClassDefinitions->{$phrase}=$class;
  }
  close Classes;
  $model->{ClassDefinitions} = $ClassDefinitions;

}

sub readStemmerExceptions {
  my ($model, $stemmerExceptions) = @_;
  my $StemmerExceptions;
  open(Exceptions, $stemmerExceptions);
  while (<Exceptions> ) {
    chomp;
    my($word, $exception) = split(/,/, $_, 2);
    next if ! $word;
    $StemmerExceptions->{$word}=$exception;
  }
  close Exceptions;
  $model->{'StemmerExceptions'} =  $StemmerExceptions;
}

sub getStems {
   my($sentence, $model) = @_;
   $sentence = NLU::Normalization::normalizeSentence($sentence, $model);
   my $SentenceStems={};
   foreach my $word (split (/\s+/, $sentence)) {

      next if $model->{excludedWords}->{$word};

      if ($word =~ /{[^}]*}/){

           $SentenceStems->{$word}++;
           next;
      }

      my $stem = stem($word, $model);
      $SentenceStems->{stem($word, $model)}++;
   }

   return ($SentenceStems);
}

sub stem {
   my ($word, $model) = @_;
   if (defined $model->{StemmerExceptions}->{$word}) {
      return $model->{StemmerExceptions}->{$word} ;
   }
   return NLU::wordstemmer::porter($word);
}



1;


        