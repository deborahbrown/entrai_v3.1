package NLU::Normalization;
use Data::Dumper;


my $month='january|february|march|april|may|june|july|august|september|october|november|december';
my $number=<<ENDNUMBER;
ten|eleven|twelve|thirteen|fourteen|fifteen|sixteen|seventeen|eighteen|nineteen|third|one|two|three|four|five|six|seven|eight|nine|
ENDNUMBER

$number= $number . <<ENDNUMBER;
twenty|thirty|forty|fifty|sixty|seventy|eighty|ninety|hundred|thousand|million|
ENDNUMBER

$number= $number . <<ENDNUMBER;
first|second|third|fourth|fifth|sixth|seventh|eighth|ninth|tenth|eleven|twelfth|thirteenth|fourteenth|fifteenth|sixteenth|seventeenth|eighteenth|nineteenth|
ENDNUMBER

$number= $number . <<ENDNUMBER;
twentieth|thirtieth|fortieth|fiftieth|sixtieth|seventieth|eightieth|ninetieth|hundredth|thousandth|millionth|
ENDNUMBER

$number= $number . <<ENDNUMBER;
1st|2nd|3rd|4th|5th|6th|7th|8th|9th|10th|11th|12th|13th|14th|15th|16th|17th|18th|19th|[0-9]+
ENDNUMBER

$number =~ s/\s+//g;

sub normalizeSentence {
   my($sentence, $model) = @_;

   $sentence =~ tr/A-Z/a-z/;				#convert to lowercase
   #$sentence =~ s/[^a-z\s]//g;
   $sentence =~ s/($number)(\-)/$1 /g;			#change any hyphens between numbers to spaces
   $sentence =~ s/[_\-]+//g;				#remove any remaining hyphens or underscores
   #$sentence =~ s/{(.+)\^[^}]+/{$1/g;

   if ($model->{Params}->{labelDates} ) {
     $sentence = labelDates($sentence);
   }
   if ($model->{Params}->{labelDigits} ) {
    $sentence = labelDigits($sentence);
   }
   $sentence = labelClasses($sentence, $model);
    return $sentence;

}


sub labelDigits {
  my ($sentence) = @_;

  $sentence =~ s/((\b($number))+\b)/{DIGITS}/g;

  return $sentence;
}


sub labelDates {
  my ($sentence) = @_;

  $sentence =~ s/(\b$month)(\s+($number))+\b/{DATE}/g;
 
  return $sentence;
}

sub labelClasses {
  my ($sentence, $model) = @_;

  my $ClassDefinitions = $model->{ClassDefinitions};
  foreach my $phrase (keys %$ClassDefinitions) {
     my $class = $ClassDefinitions->{$phrase};
     $sentence =~ s/$phrase/{$class}/g;
  }
 
  return $sentence;
}

sub getSlots {
   my($sentence, $model) = @_;

   $sentence =~ tr/A-Z/a-z/;				#convert to lowercase
   $sentence =~ s/($number)(\-)/$1 /g;			#change any hyphens between numbers to spaces
   $sentence =~ s/[_\-]+//g;				#remove any remaining hyphens or underscores

   my $slots;
   if ($model->{Params}->{labelDates} ) {
     $slots = slotDates($sentence,$model,$slots);
   }
   if ($model->{Params}->{labelDigits} ) {
    $slots = slotDigits($sentence, $model, $slots);
   }
   $slots = slotClasses($sentence, $model, $slots);
   return $slots;

}


sub slotDigits {
  my ($sentence, $model, $slots) = @_;

  if ($sentence =~ /((\b($number))+\b)/) {
     $slots->{DIGITS} = $&;
  }
  return $slots;
}


sub slotDates {
  my ($sentence, $model, $slots) = @_;

  if ($sentence =~ /(\b$month)(\s+($number))+\b/ ) {;
     $slots->{DATE} = $&;
  }
  return $slots;
}

sub slotClasses {
  my ($sentence, $model, $slots) = @_;

  my $ClassDefinitions = $model->{ClassDefinitions};
  foreach my $phrase (keys %$ClassDefinitions) {
     if ($sentence =~ /$phrase/) {
       $slots->{$ClassDefinitions->{$phrase}} = $phrase;
     }
  }
  return $slots;
}
1;



