package Agent::Upstream;
use lib './lib';
use HTTP::Request;
use LWP::UserAgent;
use Data::Dumper;
use String::Random;
use JSON::Parse 'parse_json';
use JSON::Create 'create_json';
use Number::Format qw(:subs :vars);
use URI::Escape;
use Misc::Utilities;
use IO::Socket::SSL;

my %UserDestination;


sub transferToAgent {
   my ($self, $sessionid, $Conversation, $channeltype, $appname, $userid, $queueid) = @_;

   my @entries;
   foreach my $segment (@$Conversation) {
     my $segment_j = parse_json($segment); 
     my $sender = $segment_j->{who};
     my $text = $segment_j->{msg};

     my $entry = {
        sender => {
        type => $sender,
        },
        timestamp => time,
        message => {
        text => $text
        }
     };
     push (@entries, $entry);
    }

   #Compose message

    if ($channeltype eq 'CHAT') {
       $endpt = sprintf "tobrowser/%s/%s", $appname, $sessionid;
    }
    elsif ($channeltype eq 'SMS') {
       $endpt = sprintf "fromagent/%s/%s", $appname, $sessionid;
    }

    my $json = create_json(
      {
          "to" => $main::ServerParams->{SMSSvrPhnum},
          "from" => $queueid,
          "provider" => "chat",
          "transcript" => {
          "id" => $sessionid,
          "timestamp" => time,
          "entries" => \@entries
          },
         "endpoint" => "$main::ServerParams->{SSAgentServerEndpt}/$endpt"
      });
   my $uri = sprintf  "%s/startagent/%s/%s", $main::ServerParams->{UpstreamSvrAddr}, $main::ConfigParams->{UpstreamApplication}, $sessionid;

   my $req = HTTP::Request->new( 'POST', $uri );
   $req->header( 'Content-Type' => 'application/json' );
   $req->content( $json );
   my $ua = LWP::UserAgent->new;
   my $res = $ua->request( $req );
   if ($res->is_success) {
          return 1;

   } else {
          LogEvent(1,"HTTP-RQST", "upstreaminterface", $uri, "failure", $res->status_line);
         return 0;
         
   }

}

sub createAgentSession { #Send start session message to Upstream
   my ($self, $from, $json) = @_;

   my $uri = $main::ServerParams->{'createUpstreamAgentSessionURI'};

   my $req = HTTP::Request->new( 'POST', $uri );
   $req->header( 'Content-Type' => 'application/json' );
   $req->content( $json );
   my $ua = LWP::UserAgent->new;
   my $res = $ua->request( $req );
   if ($res->is_success) {
          LogEvent(3, "HTTP-RQST", "upstreaminterface", $uri, "success");
          my $perlobj = parse_json($res->decoded_content);
          return  $perlobj->{'endpoint'};

   } else {
         LogEvent(1,"HTTP-RQST", "upstreaminterface", $uri, "failed", $res->status_line);
         return 0;
         
   }
}

sub sendToAgent {

   my ($self, $from, $txt, $uri) = @_;
   $time_stamp = time;
   my $random = new String::Random;
   $random->{'A'} = ['A'..'Z', 'a'..'z', '0'..'9'];
   my $id = sprintf "%s-%s-%s", $from, $time_stamp, $random->randpattern("AAAAAAAAAAAAAAAAAAAA");
 
   $uri =~ s/\s+$//;
   my $to = $main::ServerParams->{SMSSvrPhnum};
   my $provider = 'chat';
   my $type = 'user';

   $txt= uri_unescape($txt);
   $txt =~ s/"/\\"/g;
   $txt =~ s/:/\\:/g;
   my $json = create_json (
    {
      "timestamp"  => $time_stamp,
       "message" => {
           "to" => $to,
           "from" => $from,
           "text" => $txt, 
           "id" => $id
       }
    });


   my $req = HTTP::Request->new( 'POST', $uri );
   $req->header( 'Content-Type' => 'application/json' );
   $req->content( $json );
   my $ua = LWP::UserAgent->new;
   my $res = $ua->request( $req );
   if ($res->is_success) {
          LogEvent(3, "HTTP-RQST", "upstreaminterface", $uri, "success");
         return 1;

   } else {
         LogEvent(1, "HTTP-RQST", "upstreaminterface", $uri, "failure", $res->status_line);
         return 0;
         
   }
}

sub sendSMS () {
     my($self, $msg, $sessionid, $prompt_replacements) = @_;

     $msg = doReplacements($msg, $prompt_replacements);
     if (!defined $msg)  { return 1; }
=head
     if( defined $prompt_replacements) {
       foreach my $prompt (keys %$prompt_replacements) {
         if ($msg =~ /$prompt/) {
              $msg = $prompt_replacements->{$prompt};
              last;
         }
       }
       if ($msg =~ /^\s*$/) {
         return 1;
       }
     }
=cut


     sendEntraiRqstFromUpstream('AGENTTOSMS', $sessionid, $msg);
}


sub sendToBrowser (){
     my($self, $sessionid, $agent_rqst, $prompt_replacements) = @_;

     my $perlobj = parse_json($agent_rqst);

     my $message = $perlobj->{'message'}->{'text'};
     $message =~ s/\s+/ /g;
     $message =~ s/\$\s*([0-9]+)/ ' $' . format_number($1)/ge;
     $message =~ s/\s+/ /g;

     $message = doReplacements($message, $prompt_replacements);
     if (!defined $message)  { return 1; }
=head
     if( defined $prompt_replacements) {
       foreach my $prompt (keys %$prompt_replacements) {
         if ($message =~ /$prompt/) {
              $message = $prompt_replacements->{$prompt};
              last;
         }
       }
       if ($message =~ /^\s*$/) {
         return 1;
       }
     }
=cut
     my $user = $perlobj->{'message'}->{'to'};


     sendEntraiRqstFromUpstream('BROWSER:PROMPT', $sessionid, $message);


}


#This should be customised to determine queue based on information from call
sub getQueue {
    my($reason, $queueid) = @_;
    if (!defined $queueid || $queueid eq '') {
      return $main::ConfigParams->{default_queue};
    }
    return $queueid;
}


sub sendEntraiRqstFromUpstream {
  my ($rqst, $sessionid, $msg)  = @_;

  my ($uri, $ua);
  if ($main::ServerParams->{Protocol} eq 'wss' ) {
    $uri =  sprintf "https://%s:%s", $main::ServerParams->{LocalAddr},
             $main::ServerParams->{port};
    $ua = LWP::UserAgent->new (
           ssl_opts => {verify_hostname => 0,
                        SSL_ca_file => $main::ServerParams->{SSL_cert_file},
                        protocols_allowed => ['https'],
     });
  }
  else {
    $uri =  sprintf "http://%s:%s", $main::ServerParams->{LocalAddr},
             $main::ServerParams->{port};
    $ua = LWP::UserAgent->new();

  }
  my $req = HTTP::Request->new(POST => $uri );
  $req->content_type('application/json');
  my $content = create_json({sender => 'agent', rqst => $rqst, sessionid => $sessionid, msg => $msg});
  $req->content ($content);
  my $response = $ua->request($req);
    if (!$response->is_success) {
        LogEvent(1, "RQQSTERR", $response->status_line);
    }
}


sub doReplacements {
    my ($origprompt, $replacements) = @_;

    return $origprompt if !defined $replacements;

    my @replacements = split (/\|/, $replacements);
    while (@replacements) {
       my $searchfor = shift @replacements;
       my $substitution = shift @replacements;
       if ($origprompt =~ /^$searchfor/  ) {
           if (!defined $substitution or $substitution =~ /^\s*$/ ) {
             return undef;
           }
           else {
             return $substitution;
           }
       }
    }
    return $origprompt;
}

1;

