package Agent::GenericAgent;

use Misc::Utilities;
use JSON::Create 'create_json';
use Data::Dumper;
use strict;
use warnings;





sub transferToAgent {
   my ($self, $agent, $Conversation, $endreason, $session) = @_;

   if ($agent eq "ECE") {
      return sendTransferRqst('ECE', $session->{id},$session->{ConfigParams}); 
   } 
}

sub sendTransferRqst {
  my ($agent, $sessionid, $params)  = @_;

  my ($uri, $ua);
  if ($main::ServerParams->{Protocol} eq 'wss' ) {
    $uri =  sprintf "https://%s:%s", $main::ServerParams->{LocalAddr},
             $main::ServerParams->{port};
    $ua = LWP::UserAgent->new (
           ssl_opts => {verify_hostname => 0,
                        SSL_ca_file => $main::ServerParams->{SSL_cert_file},
                        protocols_allowed => ['https'],
     });
  }
  else {
    $uri =  sprintf "http://%s:%s", $main::ServerParams->{LocalAddr},
             $main::ServerParams->{port};
    $ua = LWP::UserAgent->new();

  }
  my $req = HTTP::Request->new(POST => $uri );
  $req->content_type('application/json');
  my $content = $params ? $params : {} ;
  $content->{rqst} = 'TRANSFERTOAGENT';
  $content->{sessionid} = $sessionid;
  $content->{agent} = $agent;
  $req->content (create_json($content));
  my $response = $ua->request($req);
    if (!$response->is_success) {
        LogEvent(1, "RQQSTERR", $response->status_line);
    }
}

1;


